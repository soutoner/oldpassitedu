<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run(){
      Eloquent::unguard();
      //insertamos los usuarios
      //$this->call('UserTableSeeder');
      //mostramos el mensaje de que los usuarios se han insertado correctamente
      $this->command->info('User table seeded!');
      //insertamos los posts
      $this->call('CategoryTableSeeder');
      //mostramos el mensaje de que los posts se han insertado correctamente
      $this->command->info('Category table seeded!');
   }
 
}