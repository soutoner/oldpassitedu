<?php

//clase para insertar usuarios
class CategoryTableSeeder extends Seeder {
 
    public function run(){
      DB::table('categories')->insert(array(
              'title' => 'Mates',
              'user_id' => User::where('username','titoelbambino')->first()->id,
              'slug' => 'mates',
      ));
      DB::table('categories')->insert(array(
              'title' => 'Fisica',
              'user_id' => User::where('username','titoelbambino')->first()->id,
              'slug' => 'fisica',
      ));
      DB::table('categories')->insert(array(
              'title' => 'Matematicas Discretas',
              'user_id' => User::where('username','titoelbambino')->first()->id,
              'slug' => 'matematicas-discretas',
      ));
    }
}