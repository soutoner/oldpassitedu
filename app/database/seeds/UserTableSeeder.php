<?php

//clase para insertar usuarios
class UserTableSeeder extends Seeder {
 
    public function run(){
      DB::table('users')->insert(array(
              'username' => 'titoelbambino',
              'password' => Hash::make('123456'),
              'name' => 'Tito',
              'surname' => 'El Bambino',
              'birth' => '1994-11-11',
              'email' => 'elbambino@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'omega21',
              'password' => Hash::make('123456'),
              'name' => 'Omega',
              'surname' => 'El Fuerte',
              'birth' => '1994-11-11',
              'email' => 'omega@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'bachateamami',
              'password' => Hash::make('123456'),
              'name' => 'Romeo',
              'surname' => 'Santos',
              'birth' => '1994-11-11',
              'email' => 'romeo@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'elpapi',
              'password' => Hash::make('123456'),
              'name' => 'Daddy',
              'surname' => 'Yankee',
              'birth' => '1994-11-11',
              'email' => 'daddy@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'omar',
              'password' => Hash::make('123456'),
              'name' => 'Don',
              'surname' => 'Omar',
              'birth' => '1994-11-11',
              'email' => 'dile@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'prince',
              'password' => Hash::make('123456'),
              'name' => 'Prince',
              'surname' => 'Royce',
              'birth' => '1994-11-11',
              'email' => 'principe@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'sensato',
              'password' => Hash::make('123456'),
              'name' => 'Sensato',
              'surname' => 'Del Barrio',
              'birth' => '1994-11-11',
              'email' => 'sensato@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'situbioquitafuera',
              'password' => Hash::make('123456'),
              'name' => 'Pitbull',
              'surname' => 'Mr Worldwide',
              'birth' => '1994-11-11',
              'email' => 'dechocolate@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'osmani',
              'password' => Hash::make('123456'),
              'name' => 'Osmani',
              'surname' => 'Garcia',
              'birth' => '1994-11-11',
              'email' => 'taxi@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'donmiguelo13',
              'password' => Hash::make('123456'),
              'name' => 'Don',
              'surname' => 'Miguelo',
              'birth' => '1994-11-11',
              'email' => 'elmariodetumujer@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'daniel-santacruz',
              'password' => Hash::make('123456'),
              'name' => 'Daniel',
              'surname' => 'Santacruz',
              'birth' => '1994-11-11',
              'email' => 'lodicelagente@example.com',
      ));
      DB::table('users')->insert(array(
              'username' => 'cubaton-99',
              'password' => Hash::make('123456'),
              'name' => 'Gente',
              'surname' => 'De Zona',
              'birth' => '1994-11-11',
              'email' => 'bailando@example.com',
      ));
    }
}