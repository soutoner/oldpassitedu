<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFollowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_follows', function(Blueprint $table)
		{
			$table->create();
      $table->increments('id');
      $table->integer('user_id')->unsigned();
      $table->integer('follow_id')->unsigned();
      $table->unique( array('user_id','follow_id') );
      $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user_follows', function(Blueprint $table)
		{
			Schema::drop('user_follows');
		});
	}

}
