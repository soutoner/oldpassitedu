<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDefavsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_defavs', function($table)
		{
     $table->create();
      $table->increments('id');
      $table->integer('user_id')->unsigned();
      $table->integer('defaver_id')->unsigned();
      $table->unique( array('user_id','defaver_id') );
      $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_defavs');
	}

}
