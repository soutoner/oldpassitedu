<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsToUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('notesNotification',1)->default('Y');
			$table->string('followersNotification',1)->default('Y');
			$table->string('newsNotification',1)->default('Y');
			$table->string('commentsNotification',1)->default('Y');
			$table->string('suggestionNotesNotifications',1)->default('Y');
			$table->string('suggestionUsersNotifications',1)->default('Y');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->dropColumn('notesNotification');
			$table->dropColumn('followersNotification');
			$table->dropColumn('newsNotification');
			$table->dropColumn('commentsNotification');
			$table->dropColumn('suggestionNotesNotifications');
			$table->dropColumn('suggestionUserssNotifications');

		});
	}

}
