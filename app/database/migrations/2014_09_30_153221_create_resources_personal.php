<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesPersonal extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resources_personal', function($table)
		{
     $table->create();
      $table->increments('id');
      $table->integer('user_id')->unsigned();
      $table->integer('resource_id')->unsigned();
      $table->unique( array('user_id','resource_id') );
      $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resources_personal');
	}

}
