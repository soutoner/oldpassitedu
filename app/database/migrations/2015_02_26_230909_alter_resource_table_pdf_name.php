<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterResourceTablePdfName extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resources', function($table)
		{
			// we drop pdf_file boolean
			$table->dropColumn('pdf_file');
		  // we add the new column holding the pdf file name
		  $table->string('pdf_name')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('resources', function($table)
		{
			// we drop pdf_name field
			$table->dropColumn('pdf_name');
		  // we restore the old column
		  $table->boolean('pdf_file')->default(false);
		});
	}

}
