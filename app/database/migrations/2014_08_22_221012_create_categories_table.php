<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categories', function($table)
		{
     $table->create();
     $table->increments('id');
     $table->string('title', 50);
     $table->string('short_desc', 140)->default("Dinos algo sobre esta categoría.");
     $table->string('pencils')->default(0);
     $table->integer('user_id')->unsigned();
     $table->string('slug');
     $table->timestamps();
    }); 
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
