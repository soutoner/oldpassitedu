<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFavsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user_favs', function($table)
		{
     	$table->create();
	    $table->increments('id');
	    $table->integer('user_id')->unsigned();
	    $table->integer('faver_id')->unsigned();
	    $table->unique( array('user_id','faver_id') );
	    $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_favs');
	}

}
