<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('profiles', function($table)
    {
    	$table->create();
      $table->increments('id');
      $table->biginteger('uid')->unsigned();
      $table->string('access_token',400);
      $table->string('access_token_secret',400);
      $table->integer('user_id')->unsigned();
      $table->integer('type')->unsigned();
      $table->boolean('share')->default(true);
      $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
