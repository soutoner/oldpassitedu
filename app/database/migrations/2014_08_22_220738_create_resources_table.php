<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('resources', function($table)
		{
     $table->create();
     $table->increments('id');
     $table->string('title');
     $table->string('short_desc', 140)->default("Dinos algo sobre este recurso.");
     $table->boolean('pdf_file')->default(false);
     $table->string('youtube_url')->nullable();
     $table->integer('favs')->default(0);
     $table->integer('pencils')->default(0);
     $table->string('curso')->default('Otro');
     $table->string('keywords')->nullable();
     $table->string('visibility',2)->default('pu');
     $table->integer('user_id')->unsigned();
     $table->integer('category_id')->unsigned()->default(0);
     $table->string('slug');
     $table->timestamps();
    });     
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resources');
	}

}
