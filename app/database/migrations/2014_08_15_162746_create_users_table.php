<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function($table)
		{
     $table->create();
     $table->increments('id');
     $table->string('username', 15)->unique();
     $table->string('email', 50)->unique();
     $table->string('password');
     $table->string('name', 25);
     $table->string('surname', 50);
     $table->string('gender', 1);
     $table->string('studies', 50)->nullable();
     $table->string('short_desc', 140)->default('Dinos algo sobre ti ...');
     $table->string('birth', 10);
     $table->integer('favs')->default(0);
     $table->integer('quota')->default(0);
     $table->rememberToken();
     $table->timestamps();
    });     
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
