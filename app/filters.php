<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request) /*enforcing SSL*/
{
	
	// if (!App::environment('local'))
	// {
	// 	if( ! Request::secure())
	//       return Redirect::secure(Request::path());
	// }

});


App::after(function($request, $response)
{
	//
});


/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		return Redirect::back()
					->with('flash_message', 'Debes estar logueado para entrar en esta página!');
	}
});

// Filtro para rutas como editar un recurso o una categoria
Route::filter('loged_in', function($route)
{
	$username = $route->getParameter('username');
	if(Auth::check())
	{
		if(Auth::user()->username!=$username)
		{
			return Redirect::to('/usuarios/'.$username)
					->with('flash_message', 'No tienes permisos para entrar en esta pagina!');
		}
	}
	else
	{
		return Redirect::to('/')
					->with('flash_message', 'Debes estar logueado para entrar en esta página!');
	}
});

Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/usuarios/'.Auth::user()->username);
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/*
 *	404 error handler
 */

App::missing(function($exception)
{
    return Response::view('errors.404', array(), 404);
});

/*
 *	model not found exception handler
 */

App::error(function(Illuminate\Database\Eloquent\ModelNotFoundException $e)
{
    return Response::view('errors.404');
});
