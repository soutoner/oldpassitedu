<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// root
Route::get('/', 'StaticPagesController@welcome')->before('guest');

//header
Route::post('utils/search','UtilsController@search');
Route::get('utils/advancedSearch','UtilsController@advancedSearch');
Route::post('utils/advancedSearch','UtilsController@advancedSearch');
Route::get('utils/search','UtilsController@search');

//s busquedaUsuario
Route::post('utils/busquedaUsuario', function(){
	$buscador = Input::get('buscador');
	$found = ViewHelpers::userSearchEach($buscador);
	return View::make('utils.busquedaUsuario')-> with('found',User::whereIn('id',$found)->paginate(3))-> with ('buscador', $buscador);
});
// busquedaApuntes
Route::post('utils/busquedaApuntes', function(){
	$buscador = Input::get('buscador');
	$found = ViewHelpers::resourcesSearchEach($buscador);
	return View::make('utils.busquedaApuntes')-> with('found',Resource::whereIn('id',$found)->paginate(3))-> with ('buscador', $buscador);
});

// StaticPages
Route::get('condiciones', 'StaticPagesController@cond');
Route::get('politicas', 'StaticPagesController@policy');
Route::get('about', 'StaticPagesController@about');
Route::get('contacto', 'StaticPagesController@contact');
Route::get('faq', 'StaticPagesController@faq');

// Users
Route::get('usuarios', 'UserController@index');
Route::get('registrate', 'UserController@create');
Route::post('registrate', 'UserController@create'); // from welcome
Route::post('users', 'UserController@store'); // para que create haga post en store (registra usuario)
Route::get('usuarios/{username}', 'UserController@show')->where('username','[A-Za-z0-9]+');
Route::get('usuarios/{username}/followers', 'UserController@followers')->where('username','[A-Za-z0-9]+');
Route::get('usuarios/{username}/following', 'UserController@following')->where('username','[A-Za-z0-9]+');
Route::get('usuarios/{username}/sugerencias','UserController@sugerencias')->where('username','[A-Za-z0-9]+');

//Rankings
Route::get('rank','UserController@rankView');
Route::post('showResults','UserController@showResults');

// SESSIONS
Route::get('login', '')->before('auth');
Route::post('login', 'SessionController@login');
Route::get('perfil', 'SessionController@profile')->before('auth');
Route::get('notificaciones', 'SessionController@notificaciones')->before('auth');
Route::get('salir', 'SessionController@logout')->before('auth');
// follow & unfollow
Route::post('usuarios/{username}/follow', 'SessionController@follow');
Route::post('usuarios/{username}/unfollow', 'SessionController@unfollow');
// fav user
Route::post('usuarios/{username}/fav', 'SessionController@fav');
Route::post('usuarios/{username}/unfav', 'SessionController@unfav');
// defav user
Route::post('usuarios/{username}/defav', 'SessionController@defav');
Route::post('usuarios/{username}/undefav', 'SessionController@undefav');
// Session#update(profile)
Route::post('updateData', 'SessionController@updateData');
Route::post('updatePassword', 'SessionController@updatePassword');
Route::post('updateDesc', 'SessionController@updateDesc');
Route::get('deleteUser', 'SessionController@destroy')->before('auth');

// CATEGORIES
Route::post('postCategory', 'CategoryController@store');
Route::get('usuarios/{username}/categorias', 'CategoryController@index')->where('username','[A-Za-z0-9]+');
Route::get('usuarios/{username}/categorias/{idOrSlug}', 'CategoryController@show')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+');
Route::get('usuarios/{username}/categorias/{idOrSlug}/editar', 'CategoryController@edit')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('loged_in');
Route::get('usuarios/{username}/categorias/{idOrSlug}/eliminar', 'CategoryController@delete')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('loged_in');
Route::post('usuarios/{username}/categorias/{idOrSlug}/update', 'CategoryController@update');

// RESOURCES
Route::get('usuarios/{username}/new','ResourceController@create_or_edit')->where('username','[A-Za-z0-9]+')->before('auth');
Route::post('usuarios/{username}/new','ResourceController@create_or_edit')->where('username','[A-Za-z0-9]+');
Route::post('usuarios/{username}/crear','ResourceController@store')->where('username','[A-Za-z0-9]+');
Route::get('usuarios/{username}/recursos/{idOrSlug}','ResourceController@show')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+');
Route::get('usuarios/{username}/recursos/{idOrSlug}/editar', 'ResourceController@create_or_edit')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('loged_in');
Route::get('usuarios/{username}/recursos/{idOrSlug}/deletePDF', 'ResourceController@deletePDF')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('loged_in');
Route::get('usuarios/{username}/recursos/{idOrSlug}/eliminar', 'ResourceController@delete')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('loged_in');
Route::post('usuarios/{username}/recursos/{idOrSlug}/update', 'ResourceController@update');
Route::post('usuarios/{username}/recursos/{idOrSlug}/report','ResourceController@report')->where('username','[A-Za-z0-9]+')->where('idOrSlug','[a-z0-9\-]+')->before('auth');
// fav resource
Route::post('usuarios/{username}/recursos/{idOrSlug}/fav', 'ResourceController@favRes');
Route::post('usuarios/{username}/recursos/{idOrSlug}/defav', 'ResourceController@unfavRes');
// escritorio personal
Route::post('usuarios/{username}/recursos/{idOrSlug}/add', 'ResourceController@addRes');
Route::post('usuarios/{username}/recursos/{idOrSlug}/remove', 'ResourceController@removeRes');

// FACEBOOK LOGIN
Route::get('login/fb', 'FacebookController@login');
Route::get('login/fb/callback', 'FacebookController@callback');
Route::get('login/fb/{username}/delete', 'FacebookController@deleteLogin')->where('username','[A-Za-z0-9]+')->before('loged_in');
Route::post('login/fb/{username}/update', 'FacebookController@update')->where('username','[A-Za-z0-9]+')->before('loged_in');

// TWITTER LOGIN
Route::get('twitter/login', 'TwitterController@login');
Route::get('twitter/callback', 'TwitterController@callback');
Route::get('twitter/login/{username}/delete', 'TwitterController@deleteLogin')->where('username','[A-Za-z0-9]+')->before('loged_in');
Route::post('twitter/login/{username}/update', 'TwitterController@update')->where('username','[A-Za-z0-9]+')->before('loged_in');

//EMAIL NOTIFICATIONS
Route::get('notifications/{username}/enableAll','NotificationsController@enableAll')->where('username','[A-Za-z0-9]+')->before('loged_in');
Route::get('notifications/{username}/disableAll','NotificationsController@disableAll')->where('username','[A-Za-z0-9]+')->before('loged_in');
Route::post('notifications/{username}/enableOrDisableNotification','NotificationsController@enableOrDisableNotification')->where('username','[A-Za-z0-9]+')->before('loged_in');

Route::post('/notifications', function(){
	return View::make('layouts._notifications');
});
Route::post('/countnotifications', function(){
	return Auth::user()->notifications()->unread()->count();
});

Route::post('/popnotifications', function(){
	$userid = Input::get('userid');
	return View::make('layouts._popnotifications')->with("noti", ViewHelpers::popNotifications($userid));
});


// PASSWORDS
Route::controller('password', 'RemindersController');

// ACCESS (deprecated)
// Route::post('access/send', 'AccessController@sendAccess');
// Route::get('access/unete/{token}', 'AccessController@create')->before('guest');
// Route::post('users', 'AccessController@store');

// UTILS
Route::get('actualmente','UtilsController@now');
//Route::post('users/create', 'UserController@create'); // para que welcome haga post en create
//Route::post('usuarios/{username}/addCat','UtilsController@addCat')->where('username','[A-Za-z0-9]+')->before('auth');

Route::get('/caca', function () {
   
});
