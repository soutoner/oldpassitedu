<?php

class RemindersController extends BaseController {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		$this->layout->content = View::make('password.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		switch ($response = Password::remind(Input::only('email')))
		{
			case Password::INVALID_USER:
				return Redirect::back()->with('flash_message', 'No hay ningun usuario registrado con ese email!');

			case Password::REMINDER_SENT:
				return Redirect::back()->with('success_message', 'Enviado el correo de reactivacion!');
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);

		$this->layout->content = View::make('password.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);

		if(DB::table('password_reminders')->where('token',$credentials['token'])) // token correcta
		{
			$user = User::where('email',$credentials['email'])->first();
			if(empty($user)) // el usuario no existe
				return Redirect::back()->with('flash_message', 'No existe ningun usuario con ese e-mail');
			else // el usuario existe
			{
				if(preg_match('/^(?=.*[A-Z])(?=.*[0-9]).{6,}$/',$credentials['password'])) // seguridad de la pass
				{
					if(strcmp($credentials['password'],$credentials['password_confirmation'])==0) // contraseñas coinciden
					{
						$params = ['password' => Hash::make($credentials['password'])];
						if($user->update($params))
						{
							DB::table('password_reminders')->where('token',$credentials['token'])->delete(); // eliminamos el token
							Auth::login($user);
							return Redirect::to('/')->with('success_message', 'Contraseña reestablecida con exito');
						}
						else
							return Redirect::back()->with('flash_message', 'Ups! Ha habido algun problema');
					}
					else
						return Redirect::back()->with('flash_message', 'Las contraseña no coinciden');
				}
				else
					return Redirect::back()->with('flash_message', 'La contraseña no es lo bastante segura');
			}
		}
		else
			return Redirect::back()->with('flash_message', 'La sesion ha expirado');
	}

}
