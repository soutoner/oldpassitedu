<?php

class CategoryController extends BaseController {

	public function store()
	{
		$params=Input::only('title','short_desc');
		$user = Auth::user();

		// ----------> Creación de categoria <------------
		$category = new Category;
		$category->user_id = $user->id;
		$category->fill($params);
		$category->slug = Str::slug($params['title']);

		if ($category->save()) 
		{
			return Redirect::back()->with('success_message', 'La categoría ha sido creada con éxito.');
    } 
    else 
    {
    	return Redirect::back()->withErrors($category->errors());
    }
	}

	public function index($username)
	{
		$user = User::findByUsernameOrFail($username);
		$paginator = Category::where('user_id', $user->id)->orderBy('created_at', 'desc')->paginate(6);

		$this->layout->content = View::make('categories.index',['user' => $user, 'categories' => $paginator]);
	}

	public function show($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$category = Category::where('id',$idOrSlug)->orWhere('slug',$idOrSlug)->where('user_id',$user->id)->firstOrFail();

		$visibility = array('foo'); // default value so we dont get null
		if(!Auth::check()) // if you're not loged in you cant see registered and private ones
			array_push($visibility, Resource::Registrado, Resource::Privado);
		else{
			$viewer = Auth::user();
			if($viewer->username != $username) // you cant see private resources
				array_push($visibility, Resource::Privado);
		}

		$paginator = Resource::where('user_id', $user->id)->where('category_id', $category->id)->whereNotIn('visibility', $visibility)->orderBy('created_at', 'desc')->paginate(6);

		$this->layout->content = View::make('categories.show',[ 'user' => $user, 'category' => $category, 'id' => $category->id, 'resources' => $paginator]);
	}

	public function edit($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$category = Category::where('id',$idOrSlug)->orWhere('slug',$idOrSlug)->where('user_id',$user->id)->firstOrFail();
		$paginator = Resource::where('user_id', $user->id)->where('category_id', $category->id)->orderBy('created_at', 'desc')->paginate(6);

		$this->layout->content = View::make('categories.edit',[ 'user' => $user, 'category' => $category, 'id' => $category->id, 'resources' => $paginator]);
	}

	public function update($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$category = Category::where('id',$idOrSlug)->orWhere('slug',$idOrSlug)->where('user_id',$user->id)->firstOrFail();

		$params = Input::only('title', 'short_desc');
		$params = array_add($params, 'slug', Str::slug($params['title']));

		if($category->update($params))
		{
			return Redirect::to('/usuarios/'.$username.'/categorias/'.$idOrSlug)->with('success_message', 'La categoria ha sido actualizada con exito');
		}
		else
		{
			return Redirect::back()->withErrors($category->errors());
		}
	}

	public function delete($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$category = Category::where('id',$idOrSlug)->orWhere('slug',$idOrSlug)->where('user_id',$user->id)->firstOrFail();
		ControllerHelpers::rrmdir(public_path().'/files/usuarios/'.$username.'/'.$category->id);
		foreach($category->resources as $resource){
			ControllerHelpers::remove_resource_rows($resource);
		}

	  $category->delete();

	  return Redirect::to('/usuarios/'.$username.'/categorias')
	            ->with('success_message', 'La categoria ha sido eliminada con exito.');
	}

}
