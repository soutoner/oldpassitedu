<?php

class StaticPagesController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function welcome()
	{
		$users = User::orderBy('created_at', 'DESC')->limit(5)->get();
		$resources = Resource::orderBy('created_at', 'DESC')->limit(5)->get();
		$this->layout->content = View::make('static_pages.welcome', ['lastestUsers' => $users, 'lastestResources' => $resources]);
	}

	public function about()
	{
		$this->layout->content = View::make('static_pages.about');
	}

	public function contact()
	{
		$this->layout->content = View::make('static_pages.contact');
	}

	public function policy()
	{
		$this->layout->content = View::make('static_pages.policy');
	}

	public function cond()
	{
		$this->layout->content = View::make('static_pages.cond');
	}
	public function faq()
	{
		$this->layout->content = View::make('static_pages.faq');
	}
}
