<?php

class TwitterController extends BaseController {

	public function login(){
		// your SIGN IN WITH TWITTER  button should point to this route
        $sign_in_twitter = TRUE;
        $force_login = FALSE;
        $callback_url = 'http://' . $_SERVER['HTTP_HOST'] . '/twitter/callback';
        // Make sure we make this request w/o tokens, overwrite the default values in case of login.
        Twitter::set_new_config(array('token' => '', 'secret' => ''));
        $token = Twitter::getRequestToken($callback_url);
        if( isset( $token['oauth_token_secret'] ) ) {
            $url = Twitter::getAuthorizeURL($token, $sign_in_twitter, $force_login);

            Session::put('oauth_state', 'start');
            Session::put('oauth_request_token', $token['oauth_token']);
            Session::put('oauth_request_token_secret', $token['oauth_token_secret']);

            return Redirect::to($url);
        }
        return Redirect::back()->with('flash_message', 'Ups ha habido algun problema al inciiar sesion con Twitter');
	}

	public function callback(){
		// You should set this route on your Twitter Application settings as the callback
        // https://apps.twitter.com/app/YOUR-APP-ID/settings
        if(Session::has('oauth_request_token')) {
            $request_token = array(
                'token' => Session::get('oauth_request_token'),
                'secret' => Session::get('oauth_request_token_secret'),
            );

            Twitter::set_new_config($request_token);

            $oauth_verifier = FALSE;
            if(Input::has('oauth_verifier')) {
                $oauth_verifier = Input::get('oauth_verifier');
            }

            // getAccessToken() will reset the token for you
            $token = Twitter::getAccessToken( $oauth_verifier );
            if( !isset( $token['oauth_token_secret'] ) ) {
                return Redirect::to('/')->with('flash_message', 'No hemos podido iniciar sesion con Twitter.');
            }

            $credentials = Twitter::query('account/verify_credentials');
            if( is_object( $credentials ) && !isset( $credentials->error ) ) {
                // $credentials contains the Twitter user object with all the info about the user.
                // Add here your own user logic, store profiles, create new users on your tables...you name it!
                // Typically you'll want to store at least, user id, name and access tokens
                // if you want to be able to call the API on behalf of your users.

                // This is also the moment to log in your users if you're using Laravel's Auth class
                // Auth::login($user) should do the trick.
            	$uid = $credentials->id;
            	$profile = Profile::whereUid($uid)->first();

            	if (empty($profile)) { // no existe usuario con ese uid, lo creamos
    				  		$user = Auth::user();
    				      if(empty($user))
    				      {
    				      	return Redirect::to('/')->with('flash_message', 'Todavia no tenemos abierto el registro, sentimos las molestias. Si ya estas registrado, recuerda conectarte con Twitter en tu perfil');
    				      }
    				      else 
    				      {
                            $cipher = new Cipher();
    				      	$profile = $this->createProfile($user, $uid);
                            $profile->access_token = $cipher->encrypt($token['oauth_token']);
                            $profile->access_token_secret = $cipher->encrypt($token['oauth_token_secret']);
                            $profile->save();
    				      }
    				  }

        		  $user = $profile->user;

        		  Auth::login($user);

              return Redirect::to('/perfil')->with('success_message', "Has iniciado sesion con Twitter!");
            }
            return Redirect::to('/')->with('flash_message', 'Ha habido algun error mientras iniciabas sesion!');
        }
	}


	public function createProfile($user, $uid){
		$profile = new Profile();
        $profile->uid = $uid;
        $profile->type = Profile::Twitter;
        $profile = $user->profiles()->save($profile);

        return $profile;
	}

    public function deleteLogin($username){
        $user = Auth::user();
        $profile = Profile::where('user_id',$user->id)->where('type',Profile::Twitter)->first();
        $profile->delete();

        return Redirect::back()->with('success_message', 'Has sido desconectado de Twitter');
    }

    public function update(){
        $user = Auth::user();
        $profile = Profile::where('user_id',$user->id)->where('type',Profile::Twitter)->first();
        $profile->share = Input::get('share');

        if($profile->save())
            return Redirect::back()->with('success_message', 'Tu informacion ha sido actualizada con exito');
        else
            return Redirect::back()->with('flash_message', 'Ups ha habido algun problema actualizando tu informacion');
    }

}
