<?php

class FacebookController extends BaseController {

	public function login() {
		$facebook = new Facebook(Config::get('facebook'));
	  $params = array(
	      'redirect_uri' => url('/login/fb/callback'),
	      'scope' => 'email,publish_actions',
	  );
	  return Redirect::to($facebook->getLoginUrl($params));
	}

	public function callback(){
		$code = Input::get('code');
	  if (strlen($code) == 0) return Redirect::to('/')->with('flash_message', 'Ha habido un error en la comunicacion con facebook');

	  $facebook = new Facebook(Config::get('facebook'));
	  $uid = $facebook->getUser();

	  if ($uid == 0) return Redirect::to('/')->with('flash_message', 'Ha habido un error');

	  $me = $facebook->api('/me');

	  $profile = Profile::whereUid($uid)->first();
	  if (empty($profile)) { // no existe usuario con ese uid, lo creamos
	  		$user = Auth::user();
	      if(empty($user)) //registro de usuario
	      {
	      	return Redirect::action('UserController@create', ['facebook' => $me]);
	      }
	      else 
	      {
	      	$cipher = new Cipher();
	      	$profile = $this->createProfile($user, $uid);
	      	$profile->access_token = $cipher->encrypt($facebook->getAccessToken());
	  			$profile->save();
	      }
	  }	  

	  $user = $profile->user;

	  Auth::login($user);

	  return Redirect::to('/usuarios/'.$user->username)->with('success_message', 'Has iniciado sesion con Facebook');
	}

	public static function createProfile($user, $uid){
		$profile = new Profile();
    $profile->uid = $uid;
    $profile->type = Profile::Facebook;
    $profile = $user->profiles()->save($profile);

    return $profile;
	}

	public function deleteLogin($username){
		$user = Auth::user();
		$facebook = new Facebook(Config::get('facebook'));
		$profile = Profile::where('user_id',$user->id)->where('type',Profile::Facebook)->first();
		$facebook->api($profile->uid.'/permissions/','delete');
		$profile->delete();

		return Redirect::to('/usuarios/'.$user->username)->with('success_message', 'Has sido desconectado de Facebook');
	}

	// public function update(){
	// 	$user = Auth::user();
	// 	$profile = Profile::where('user_id',$user->id)->where('type',Profile::Facebook)->first();
	// 	$profile->share = Input::get('share');

	// 	if($profile->save())
	// 		return Redirect::back()->with('success_message', 'Tu informacion ha sido actualizada con exito');
	// 	else
	// 		return Redirect::back()->with('flash_message', 'Ups ha habido algun problema actualizando tu informacion');
	// }

}
