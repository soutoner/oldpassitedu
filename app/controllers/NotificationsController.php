<?php
class NotificationsController extends BaseController {

	public function enableAll(){
		$user = Auth::user();
		DB::table('users')
            ->where('id', $user->id)
            ->update(array('notesNotification' => 'Y',
            			   'followersNotification' => 'Y',
            			   'newsNotification' => 'Y',
            			   'commentsNotification' => 'Y',
            			   'suggestionNotesNotifications' => 'Y',
            			   'suggestionUsersNotifications' => 'Y'));
            return Redirect::back()->with('success_message', 'Tus preferencias han sido actualizadas');
	}
	public function disableAll(){
		$user = Auth::user();
		DB::table('users')
            ->where('id', $user->id)
            ->update(array('notesNotification' => 'N',
            			   'followersNotification' => 'N',
            			   'newsNotification' => 'N',
            			   'commentsNotification' => 'N',
            			   'suggestionNotesNotifications' => 'N',
            			   'suggestionUsersNotifications' => 'N'));
            return Redirect::back()->with('success_message', 'Tus preferencias han sido actualizadas');
	}
	public function enableorDisableNotification(){
		$notification = $_POST['notification'];
		$user = Auth::user();
		$value = DB::table('users')->where('id', $user->id)->pluck($notification);
		if(strcmp($value, "Y") == 0){
			DB::table('users')
            ->where('id', $user->id)
            ->update(array($notification => 'N'));
		}else{
			DB::table('users')
            ->where('id', $user->id)
            ->update(array($notification => 'Y'));	
		}
		return Redirect::back()->with('success_message', 'Tus preferencias han sido actualizadas');
	}
	
}