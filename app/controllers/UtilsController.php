
<?php
class UtilsController extends BaseController {


	public function search(){
		if(!Input::has('page')){
		/*PREGUNTA PARA VER SI VENGO DE LA BUSQUEDA AVANZADA*/
	 		if(Input::has('buscador') && Input::has('bool')){
	 			$buscador = Input::get('buscador');
	 			$seleccion = 2;
	 		}else{
	 			$seleccion = Input::get('cond');
	 			$buscador = Input::get('buscador');
	 		}
 		/*TAMAÑO DEL INPUT MINIMO DE 3 LETRAS*/
 		if (isset($seleccion)){
 			//COGEMOS EL RADIO INSERTADO
 			if($seleccion == 1) {
 				if(strlen(Input::get('buscador')) == 0){
	 				$this->layout->content = View::make('utils.search')->with('found',User::where('id','>','0')->orderBy('favs','desc')->paginate(10));
	 				return;
	 			}else if(strlen(Input::get('buscador')) < 3){
		 				return Redirect::to('/')->with('flash_message','No has escrito en el buscador o has puesto una palabra menor de 3 letras');
		 		}
 				//SELECCION: BUSCAR USUARIOS
 				$found = array();
 				//LLAMAMOS A LA FUNCION QUE BUSCA USUARIOS
 				$found = ViewHelpers::userSearch($buscador);
 				if(count($found) == 0){
 					Session::flash('flash_message','El usuario que has escrito no existe, ni tampoco se parece a otro existente');
 					$this->layout->content = View::make('utils.search')->with('found',$found);
 				}else{
 					Session::put('found',$found);
 					$this->layout->content = View::make('utils.search')->with('found',User::whereIn('id',$found)->paginate(10)); 
 				}
 			}else{
				if(strlen(Input::get('buscador')) == 0){
 					$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::where('id','>','0')->orderBy('favs','desc')->paginate(10));
 					return;
	 			}else if(strlen(Input::get('buscador')) < 3){
		 			return Redirect::to('/')->with('flash_message','No has escrito en el buscador o has puesto una palabra menor de 3 letras');
		 		}
				//BUSCAMOS APUNTES POR PALABRAS CLAVE(FUNCION EN HELPERS.PHP)
				$found = array();
				//LLAMAMOS A LA FUNCION QUE BUSCA USUARIOS
				$found = ViewHelpers::noteSearch($buscador);
				if(count($found) == 0){
					Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
					$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
				}else{
					Session::put('found',$found); 
					$auxiliar = Resource::where('user_id',array(Auth::id()))->lists('id');
					if(empty($auxiliar))
						$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->whereNotIn('id',array(0))->orderBy('favs','desc')->paginate(10));
					else			
						$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->whereNotIn('id',$auxiliar)->orderBy('favs','desc')->paginate(10));
				}
			}
		}
		}else{
			$found = Session::get('found');

			if(Input::has('n')){
				if(empty($found)){
					$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::where('id','>','0')->orderBy('favs','desc')->paginate(10));
	 				return;
				}else{
					$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
				}
			}else{
				if(empty($found)){
					$this->layout->content = View::make('utils.search')->with('found',User::where('id','>','0')->orderBy('favs','desc')->paginate(10));
				}else{
					$this->layout->content = View::make('utils.search')->with('found',User::whereIn('id',$found)->paginate(10)); 
				}
			}
	}
	}
	public function advancedSearch(){
		
		if(!Input::has('page')){
			$buscador = Input::get('buscador');
			$palabras = explode(" ",$buscador);
			$curso = Input::get('curso');
			$usuarios = Input::get('usuarios');
			$todos = "todos";
			$found = array();
			/*BUSQUEDA NORMAL , NO SE HAN ELEGIDO FILTROS*/
			if(strcmp($curso[0],"todos") == 0 && strcmp($usuarios[0], "todos") == 0){
				return $this->search();
			}else if(strcmp($usuarios[0],"otros") == 0 && strcmp($curso[0],"todos") != 0){
				/*OTROS USUARIOS, CURSO ESPECIFICO,*/
				if(Auth::check()){
					$followID = DB::table('user_follows')->where('user_id','=',Auth::id())->lists('follow_id');
					$found = Resource::whereNotIn('user_id',$followID)->where('curso','=',$curso[0])
																   ->where(function($query) use ($buscador){
											 				 $query->where('title','=',$buscador)
											 			 	       ->orWhere('title', 'like',"%$buscador%");
																 })->lists('id');
					$user = Auth::user();
					$auxiliar = Resource::where('user_id',array(Auth::id()))->lists('id');
					$pagFollowing = User::whereNotIn('id', function($query) use ($user){
						$query->select('follow_id')
						      ->from('user_follows')
						      ->where('user_id', $user->id);
					})->whereNotIn( 'id', [$user->id])->get();
					//BUSCAMOS APUNTES POR PALABRAS CLAVE Y SUS FILTROS(FUNCION EN HELPERS.PHP)
					$found = ViewHelpers::keywordAdvancedSearcher($palabras,$found,$curso[0],$pagFollowing,$usuarios[0]);
					//FOUND CONTIENE LOS RECURSOS ENCONTRADOS POR PALABRAS CLAVE
				}else{
					//NO SE HA INICIADO SESION
					Session::flash('flash_message','No has iniciado sesion, inicia sesion para buscar por usuarios');
					return Redirect::to('/');
				}	
				if(empty($found)){
					//NO EXISTEN ESTOS APUNTES EN NUESTRA DB
					Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
					$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
				}else{
					$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
				}
			}else if(strcmp($curso[0],"todos") != 0  && strcmp($usuarios[0], "todos") == 0){
				//BUSCAMOS APUNTES POR PALABRAS CLAVE Y SUS FILTROS(FUNCION EN HELPERS.PHP)
					$found = ViewHelpers::keywordAdvancedSearcher($palabras,$found,$curso[0],array(),$usuarios[0]);
				//FOUND CONTIENE LOS RECURSOS ENCONTRADOS POR PALABRAS CLAVE

				/*BUSCAR EN TODOS LOS USUARIOS, POR CURSO ESPECIFICO*/
				if(Auth::check()){
					$auxiliar = Resource::where('user_id',array(Auth::id()))->lists('id');
					if(empty($auxiliar)){
					$resources = Resource::where(function($query) use ($buscador){
												 		$query->where('title','=',$buscador)
												 			  ->orWhere('title', 'like',"%$buscador%");
													})->where('curso','=',$curso[0])->whereNotIn('id',array(0))->orderBy('favs','desc')->lists('id');
					}else{
						$resources = Resource::where(function($query) use ($buscador){
												 		$query->where('title','=',$buscador)
												 			  ->orWhere('title', 'like',"%$buscador%");
													})->where('curso','=',$curso[0])->whereNotIn('id',$auxiliar)->orderBy('favs','desc')->lists('id');
					}
				}else{
					$resources = Resource::where(function($query) use ($buscador){
												 		$query->where('title','=',$buscador)
												 			  ->orWhere('title', 'like',"%$buscador%");
													})->where('curso','=',$curso[0])->orderBy('favs','desc')->lists('id');
				}
				foreach ($resources as $resource) {
					if(!in_array ( $resource , $found )){
						array_push($found, $resource);
					}
				}
				if(empty($found)){
					Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
					$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
				}else{
					$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
				}
				
			}else if(strcmp($curso[0],"todos") != 0  && strcmp($usuarios[0], "mios") == 0){
				/*BUSCAR EN MIS USUARIOS, POR CURSO ESPECIFICO*/
				if(Auth::check()){
					$followID = DB::table('user_follows')->where('user_id','=',Auth::id())->lists('follow_id');
					$found = Resource::whereIn('user_id',$followID)->where('curso','=',$curso[0])
																   ->where(function($query) use ($buscador){
											 				 $query->where('title','=',$buscador)
											 			 	       ->orWhere('title', 'like',"%$buscador%");
																 })->lists('id');
					$user = Auth::user();
					$pagFollowing = User::whereIn('id', function($query) use ($user){
						$query->select('follow_id')
						      ->from('user_follows')
						      ->where('user_id', $user->id);
					})->get();
					//BUSCAMOS APUNTES POR PALABRAS CLAVE Y SUS FILTROS(FUNCION EN HELPERS.PHP)
					$found = ViewHelpers::keywordAdvancedSearcher($palabras,$found,$curso[0],$pagFollowing,$usuarios[0]);
					//FOUND CONTIENE LOS RECURSOS ENCONTRADOS POR PALABRAS CLAVE
					
					if(empty($found)){
						Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
						$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
					}else{
						$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
					}
				}else{
					//NO SE HA INICIADO SESION
					Session::flash('flash_message','No has iniciado sesion, inicia sesion para buscar por usuarios');
					return Redirect::to('/');
				}
			}else if(strcmp($curso[0],"todos") == 0  && strcmp($usuarios[0], "otros") == 0){
				/*BUSCAR EN OTROS USUARIOS SIN CURSO ESPECIFICO*/
				if(Auth::check()){
					$followID = DB::table('user_follows')->where('user_id','=',Auth::id())->lists('follow_id');
					$found = Resource::whereNotIn('user_id',$followID)->where(function($query) use ($buscador){
											 				 $query->where('title','=',$buscador)
											 			 	       ->orWhere('title', 'like',"%$buscador%");
																 })->lists('id');
					$user = Auth::user();
					$auxiliar = Resource::where('user_id',array(Auth::id()))->lists('id');
					$pagFollowing = User::whereNotIn('id', function($query) use ($user){
						$query->select('follow_id')
						      ->from('user_follows')
						      ->where('user_id', $user->id);
					})->whereNotIn( 'id', [$user->id])->get();
					//BUSCAMOS APUNTES POR PALABRAS CLAVE Y SUS FILTROS(FUNCION EN HELPERS.PHP)
					$found = ViewHelpers::keywordAdvancedSearcher($palabras,$found,$curso[0],$pagFollowing,$usuarios[0]);
					//FOUND CONTIENE LOS RECURSOS ENCONTRADOS POR PALABRAS CLAVE
					if(empty($found)){
					//NO EXISTEN ESTOS APUNTES EN NUESTRA DB
					Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
					$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
					}else{
						$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
					}
				}else{
					//NO SE HA INICIADO SESION
					Session::flash('flash_message','No has iniciado sesion, inicia sesion para buscar por usuarios');
					return Redirect::to('/');
				}
			}else if(strcmp($curso[0],"todos") == 0 && strcmp($usuarios[0],"mios") == 0){
				/*BUSCAR EN MIS USUARIOS SIN CURSO ESPECIFICO*/
				if(Auth::check()){
					$followID = DB::table('user_follows')->where('user_id','=',Auth::id())->lists('follow_id');
					$found = Resource::whereIn('user_id',$followID)->where(function($query) use ($buscador){
											 				 $query->where('title','=',$buscador)
											 			 	       ->orWhere('title', 'like',"%$buscador%");
																 })->lists('id');
					$user = Auth::user();
					$pagFollowing = User::whereIn('id', function($query) use ($user){
						$query->select('follow_id')
						      ->from('user_follows')
						      ->where('user_id', $user->id);
					})->get();
					//BUSCAMOS APUNTES POR PALABRAS CLAVE Y SUS FILTROS(FUNCION EN HELPERS.PHP)
					$found = ViewHelpers::keywordAdvancedSearcher($palabras,$found,$curso[0],$pagFollowing,$usuarios[0]);
					//FOUND CONTIENE LOS RECURSOS ENCONTRADOS POR PALABRAS CLAVE
					if(empty($found)){
					//NO EXISTEN ESTOS APUNTES EN NUESTRA DB
						Session::flash('flash_message','No se han encontrado coincidencias con estos filtros');
						$this->layout->content = View::make('utils.searchNotes')->with('found',$found);
					}else{
						$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
					}
					
				}else{
					//NO SE HA INICIADO SESION
					Session::flash('flash_message','No has iniciado sesion, inicia sesion para buscar por usuarios');
					return Redirect::to('/');
				}
				
			}
		}else{
			$found = Session::get('found');
			$this->layout->content = View::make('utils.searchNotes')->with('found',Resource::whereIn('id',$found)->paginate(10));
		}
	}

	public function now(){
		$this->layout->content = View::make('utils.actualmente');
	}


}?>