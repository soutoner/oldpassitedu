<?php

class ResourceController extends BaseController {

	public function store($username)
	{
		$user = User::findByUsernameOrFail($username);
		$params=Input::only('title','curso','category_id','short_desc','editor','youtube_url','keywords','visibility');
	
		// ----------> Creación del recurso <------------
		$resource = new Resource;
		$resource->fill($params); // se completan los parametros fillable
		$resource->user_id = $user->id;
		$resource->slug = Str::slug($params['title']);

		// we check if the user will have space to host that resource
		if(Input::hasFile('pdf_file'))
			$aprox_size = 4+(Input::file('pdf_file')->getSize()/1000); // in KBytes
		else
			$aprox_size = 4;

		if($aprox_size>User::Quota)
			return Redirect::to('/usuarios/'.$user->username.'/new')->with('flash_message','No tienes suficiente espacio para almacenar este recurso');

		if(strlen($params['editor'])<=0)
			return Redirect::to('/usuarios/'.$user->username.'/new')->with('flash_message','No puedes dejar el archivo de texto vacio.');

		if ($resource->save()) 
		{
			// ----------> Creacion del directorio del usuario <-----------------
			$path = public_path().'/files/usuarios/'.$user->username.'/'.$params['category_id'].'/'.$resource->id;
			if (!is_dir($path))
			{
	        //make new directory with unique id
	   			mkdir($path,0777,true); 
			}

			// TEXT FILE
			ControllerHelpers::save_text_file($path, $params['editor']);
			// END TEXT FILE

			// PDF FILE
			if (Input::hasFile('pdf_file'))
			{
				$pdf = Input::file('pdf_file');
				// PDF VALIDATION
		    $validator = Validator::make(
		      array('pdf_file' => $pdf),
		      array('pdf_file' => 'max:1000000|mimes:pdf'),
		      array(
		        'max'    => 'El :attribute debe pesar como max. :max kilobytes.',
		        'mimes'    => 'El :attribute debe tener formato: :values.'
		      )
		    );
		    if($validator->fails())
		    {
		    	// rollback all we did (save resource to DB and create dir)
		    	$resource->delete();
		    	ControllerHelpers::rrmdir($path, $pdf);
		      return Redirect::to('/usuarios/'.$user->username.'/new')->withErrors($validator);
		    }
		    else
		    {
		    	ControllerHelpers::save_pdf_file($path, $pdf);
		    }
			}	
			// END PDF FILE		

			// UPDATE QUOTA
			ControllerHelpers::user_quota($user);
			// END UPDATE QUOTA

			// Compartimos donde haya configurado el usuario (comprobacion dentro de las funciones)
			//ControllerHelpers::share_resource_fb($user,$resource);
			ControllerHelpers::share_resource_tw($user,$resource);

      return Redirect::to('/usuarios/'.$username.'/recursos/'.$resource->slug)->with('success_message', 'El recurso ha sido creado con éxito!');
   	} 
  	else 
  	{
  		return Redirect::to('/usuarios/'.$user->username.'/new')->withErrors($resource->errors());
  	}
	}

	public function create_or_edit($username="", $idOrSlug="")
	{
		if(Request::segment(3)=='new')
		{
			$user = User::findByUsernameOrFail($username);
			$this->layout->content = View::make('resources.create', ['user' => $user]);
		}
		else
		{
			$user = User::findByUsernameOrFail($username);
			$resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();
			$category = $resource->category;
			$this->layout->content = View::make('resources.create', ['user' => $user, 'category' => $category , 'resource' => $resource]);
		}

	}

	public function show($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();
		$category = $resource->category;

		// VISIBILITY
		if($resource->visibility==Resource::Privado)
		{
			$user = Auth::user();
			if(empty($user) || ($user->username!=$username) )
				return Redirect::back()->with('flash_message', 'Debido a la configuracion de privacidad no puedes ver este recurso');
		}
		else if($resource->visibility==Resource::Registrado)
		{
			if(!Auth::check())
				return Redirect::back()->with('flash_message', 'Debido a la configuracion de privacidad de este recurso debes haber inciado sesion para poder verlo');
		}
		// END VISIBILITY

		$text = $resource->text;
		$expiresAt = Carbon::now()->addMinutes(5);
		Cache::put('text'.$resource->id, $text, $expiresAt);

		$this->layout->content = View::make('resources.show', ['user' => $user, 'category' => $category , 'resource' => $resource]);
	}

	public function update($username, $idOrSlug)
	{
		$user = User::findByUsernameOrFail($username);
		$resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();

		$params=Input::only('title','curso','short_desc','editor','youtube_url','category_id','keywords','visibility');
		$old_category = $resource->category_id;
		$params = array_add($params, 'slug', Str::slug($params['title']));

		// we check if the user will have space to host that resource
		if(Input::hasFile('pdf_file'))
			$aprox_size = 4+(Input::file('pdf_file')->getSize()/1000); // in KBytes
		else
			$aprox_size = 4;

		if($aprox_size>User::Quota)
			return Redirect::back()->with('flash_message','No tienes suficiente espacio para almacenar este recurso');

		if(strlen($params['editor'])<=0)
			return Redirect::back()->with('flash_message','No puedes dejar el archivo de texto vacio.');

		if($resource->update($params))
		{
			// ----------> Creacion del directorio del usuario <-----------------
			$oldpath = public_path().'/files/usuarios/'.$user->username.'/'.$old_category.'/'.$resource->id;
			$path = public_path().'/files/usuarios/'.$user->username.'/'.$params['category_id'].'/'.$resource->id;
			if (!is_dir($path)) // el usuario ha cambiado de categoria, creamos la nueva carpeta, y borramos la anterior
			{
				ControllerHelpers::rcopy($oldpath, $path); // copiar todo el directorio a la nueva carpeta
				ControllerHelpers::rrmdir($oldpath); // eliminamos la carpeta anterior y su contenido
			}

			// TEXT FILE
			ControllerHelpers::save_text_file($path, $params['editor']);
			// END TEXT FILE

			// PDF FILE
			if (Input::hasFile('pdf_file'))
			{
				$pdf = Input::file('pdf_file');
				// PDF VALIDATION
				$validator = Validator::make(
					array('pdf_file' => $pdf),
				  array('pdf_file' => 'max:10000|mimes:pdf'),
					array(
				    'max'    => 'El :attribute debe pesar como max. :max kilobytes.',
				    'mimes'    => 'El :attribute debe tener formato: :values.'
					)
				);
				if($validator->fails())
				{
					return Redirect::back()->withErrors($validator);
				}
		    else
		    {
		    	ControllerHelpers::save_pdf_file($path, $pdf);
		    }
			}
			// END PDF FILE

			// UPDATE QUOTA
			ControllerHelpers::user_quota($user);
			// END UPDATE QUOTA

			return Redirect::to('/usuarios/'.$username.'/recursos/'.$resource->slug)->with('success_message', 'El recurso ha sido editado con éxito!');
		}
		else
		{
			return Redirect::back()->withErrors($resource->errors());
		}
	}

	public function deletePDF($username, $idOrSlug)
	{
		$user = Auth::user();
		$resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();

	  $path = public_path().'/files/usuarios/'.$username.'/'.$resource->category->id.'/'.$resource->id;
		ControllerHelpers::remove_pdf_file($path);

		// UPDATE QUOTA
		ControllerHelpers::user_quota($user);
		// END UPDATE QUOTA

	  return Redirect::back()->with('success_message', 'El PDF ha sido eliminado con exito.');
	}

	public function delete($username, $idOrSlug)
	{
	  $user = Auth::user();
		$resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();
	  $path = public_path().'/files/usuarios/'.$username.'/'.$resource->category->id.'/'.$resource->id;

	  ControllerHelpers::remove_single_resource($path,$resource);

	  // UPDATE QUOTA
		ControllerHelpers::user_quota($user);
		// END UPDATE QUOTA

	  return Redirect::to('/usuarios/'.$username)
	            ->with('success_message', 'El recurso ha sido eliminado con exito.');
	}

	public function favRes($username, $idOrSlug)
  {
  		$user = User::findByUsernameOrFail($username);
      $resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();         
      $follower = Auth::user();
      $resource->favers()->save($follower);
      DB::table('resources')->where('id', $resource->id)->increment('favs');
      $resOwner = $resource->user->username;

      $user->newNotification()
        ->withSubject($follower->username.' ha añadido a favoritos el recurso '.$resource->title.'.')
        ->fromUser($follower)
        ->regarding($resource)
        ->deliver();

      return Redirect::back();
  }

  public function unfavRes($username, $idOrSlug)
  {
      $user = User::findByUsernameOrFail($username);
      $resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();         
      $follower = Auth::user();
      $resource->favers()->detach($follower);
      DB::table('resources')->where('id', $resource->id)->decrement('favs');
      
      return Redirect::back();
  }

  public function addRes($username, $idOrSlug)
  {
      $user = User::findByUsernameOrFail($username);
      $resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();         
      $follower = Auth::user();
      $resource->adders()->save($follower);
      
      return Redirect::back();
  }

  public function removeRes($username, $idOrSlug)
  {
      $user = User::findByUsernameOrFail($username);
      $resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();
      $follower = Auth::user();
      $resource->adders()->detach($follower);
      
      return Redirect::back();
  }
  public function report($username,$idOrSlug){
  	$user = User::findByUsernameOrFail($username);
      $resource = Resource::where('id', $idOrSlug)->orWhere('slug', $idOrSlug)->where('user_id',$user->id)->firstOrFail();
  	$data = Input::only('texto','pdf','link','inapropiado');
  	array_push($data, $username);
  	array_push($data, $resource->id);
  		Mail::queue('emails.admin.reports', $data, function($message)
			{
			  $message->to('passitedu@gmail.com', 'Passitedu')
          ->subject('Recurso reportado');
			});

  	// $user->newNotification()
   //      ->withSubject('Tu recurso '.$resource->title.' ha sido reportado. Recuerda las condiciones.')
   //      ->fromUser($follower)
   //      ->regarding($resource)
   //      ->deliver();

  	return Redirect::to('/usuarios/'.$username)->with('flash_message','Tu aviso ha llegado a nuestro soporte, muchas gracias');
  }
  
}
