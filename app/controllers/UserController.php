<?php

class UserController extends BaseController {

	public function index()
	{
		$this->layout->content = View::make('users.index')->with('users', User::paginate(10));
	}

	public function create()
	{
		if(Input::get('facebook')){
			$me = Input::get('facebook');
			if($me['gender'] == 'male') $me['gender'] = 'H';
			else $me['gender'] = 'M';

			$facebook = new Facebook(Config::get('facebook'));
			$me = array_add($me, 'accessToken', $facebook->getAccessToken());

		} else 
			$me = [];

		$this->layout->content = View::make('users.create', ['fbUser' => $me]);
	}

	public function store()
	{
		// obetenemos los valores del form
		$params=Input::only('username', 'email', 'name', 'surname', 'gender', 'studies', 'birth', 'password', 'password_confirmation');

		// ----------> Creación de usuario <------------
		$user = new User;
		$user->fill($params); // se completan los parametros fillable
		if(Input::get('short_desc')!=null) $user->short_desc = Input::get('short_desc');

		if ($user->save() && strcmp($params['password'], $params['password_confirmation'])==0) 
		{
			// logueamos
			Auth::login($user);

			if(Input::get('uid')!=0){ // creamos el profile de facebook
				$uid = Input::get('uid');
				$accessToken = Input::get('accessToken');

				$cipher = new Cipher();
      	$profile = FacebookController::createProfile($user, $uid);
      	$profile->access_token = $cipher->encrypt($accessToken);
  			$profile->save();
			}

	    return Redirect::to('/usuarios/'.$user->username)->with('success_message', 'Bienvenido a PassItEDU, gracias por registrarte!');
	  } 
	  else 
	  {
	  	if(strcmp($params['password'], $params['password_confirmation'])!=0)
	  		$user->errors()->add('password_confirmation','La contraseña y la confirmación no coinciden.');
	  	return Redirect::back()->withErrors($user->errors());
	  }
	}

	public function show($username)
	{

		$user = User::findByUsernameOrFail($username);
		$feed = Resource::whereIn('user_id', function($query) use ($user)
		{
		  $query->select('follow_id')
		        ->from('user_follows')
		        ->where('user_id', $user->id);
		})->orderBy('created_at', 'desc')->limit(30)->paginate(7);
		$activity = Resource::where('user_id', $user->id)->orderBy('created_at', 'desc')->limit(30)->paginate(7);
		$personal = Resource::whereIn('id', function($query) use ($user)
		{
		  $query->select('resource_id')
		        ->from('resources_personal')
		        ->where('user_id', $user->id);
		})->orderBy('created_at', 'desc')->paginate(7);
		$this->layout->content = View::make('users.show')->with(['user' => $user, 'pagFeed' => $feed, 'activity' => $activity, 'resources' => $personal]);
	}

	public function followers($username)
	{
    $user = User::findByUsernameOrFail($username);

  	$pagination = User::whereIn('id', function($query) use ($user)
		{
		  $query->select('user_id')
		        ->from('user_follows')
		        ->where('follow_id', $user->id);
		})->paginate(8);

		$this->layout->content = View::make('users.social', ['user' => $user, 'usuarios' => $pagination]);
	}

	public function following($username)
	{
    $user = User::findByUsernameOrFail($username);

    $pagination = User::whereIn('id', function($query) use ($user)
		{
		  $query->select('follow_id')
		        ->from('user_follows')
		        ->where('user_id', $user->id);
		})->paginate(8);

		$this->layout->content = View::make('users.social', ['user' => $user, 'usuarios' => $pagination]);
	}
	//Como me siguen pues aparecen
	public function sugerencias($username){
		$user = User::findByUsernameOrFail($username);
		$pagination = User::whereIn('id', function($query) use ($user){
				    			$query->select('u2.follow_id')
							    ->from('user_follows AS u2')
							    ->whereIn('u2.user_id', function($query2) use($user){
								    	$query2->select('follow_id')
								    	->from('user_follows')
								    	->where('user_id','=',$user->id);
				    					});
							})
							    ->where('id','NOT LIKE',$user->id)
							    ->whereNotIn('id',function($query3)use ($user){
							    	 $query3->select('follow_id')
		        					->from('user_follows')
		        					->where('user_id', $user->id);
							    })
							    ->orWhereIn('id',function($query4)use($user){
							    	$query4->select('id')
							    	->from('users')
							    	->where(Str::lower('studies'),'LIKE',Str::lower($user->studies))
							    	->whereNotIn('id',function($query5)use($user){
							    		$query5->select('follow_id')
		        						->from('user_follows')
		        						->where('user_id', $user->id);
		        				 	})
		        				 	->where('id','NOT LIKE',$user->id);
							    })
							->orderBy('favs','desc')
							->paginate(10);
		
		$this->layout->content = View::make('users.social', ['user' => $user, 'usuarios' => $pagination]);
	}
	public function rankView(){
		
		$this->layout->content = View::make('users.rank')->with('users', User::orderBy('created_at','asc')->paginate(100));
	}
	public function showResults(){
		$rank = Input::get('usuarios');
		$users = array();
			switch ($rank[0]) {
	    case "0":
			$this->layout->content = View::make('users.rank')->with('users', User::orderBy('created_at','asc')->paginate(100));
			break;
	    case "1":
	    	$query = User::select('users.id','username','email','password','name','surname','gender','studies','short_desc','birth','favs','quota','remember_token','users.created_at','users.updated_at','min','pts',DB::raw('((min+favs)*0.75)+(pts*0.25) as res'))->Join('media','id','=','user_id')->Join('rank','id','=','rank.user_id')->groupBy('id','min')->orderByRaw('res desc')->get(100);

	        $this->layout->content = View::make('users.rank')->with('users', $query)->with('link',0);
	        break;
	    case "2":
	        $this->layout->content = View::make('users.rank')->with('users', User::orderBy('favs','desc')->paginate(100));
	        break;
	    case "3":
	    //Select *,count(*) from users join user_follows on users.id = user_follows.follow_id group by users.id order by count(*) desc
	    	$moreFollowers = User::select('users.id','username','email','password','name','surname','gender','studies','short_desc','birth','favs','quota','remember_token','users.created_at','users.updated_at')->Join('user_follows', 'users.id', '=', 'user_follows.follow_id')->groupBy('users.id')->orderByRaw('count(*) desc')->paginate(100);	    	
	    	$this->layout->content = View::make('users.rank')->with('users',$moreFollowers);
	    	break;
	    case "4":
	    	$moreFollowing = User::select('users.id','username','email','password','name','surname','gender','studies','short_desc','birth','favs','quota','remember_token','users.created_at','users.updated_at')->Join('user_follows', 'users.id', '=', 'user_follows.user_id')->groupBy('users.id')->orderByRaw('count(*) desc')->paginate(100);	    	
	    	$this->layout->content = View::make('users.rank')->with('users',$moreFollowing);
	    	break;
	    
		}
	}
}
