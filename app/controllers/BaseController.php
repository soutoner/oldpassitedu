<?php

class BaseController extends Controller {

	/* definiendo layout */
	protected $layout = 'layouts.master'; // layout en /app/layouts/master.blade.php

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
