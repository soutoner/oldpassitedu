<?php

class SessionController extends BaseController {

    public function login()
    {
        //coprobamos si se loguea con un mail o con un username
        if(preg_match('/\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i',Input::get('login'))) // email
        {
            $login = array(
            'email' => Input::get('login'),
            'password' => Input::get('pass')
            );
        } // username
        else 
        {
            $login = array(
            'username' => Input::get('login'),
            'password' => Input::get('pass')
            );
        }

        if (Auth::attempt($login)) {
            return Redirect::to('/usuarios/'.Auth::user()->username)
                ->with('success_message', 'Bienvenido de nuevo '.Auth::user()->name.' !');
        }

        // authentication failure! lets go back to the login page
        return Redirect::back()
            ->with('flash_message', 'El username/email o contraseña que has introducido no son correctos.');
    }

    public function logout() 
    {
        $name = Auth::user()->name;
        Auth::logout();
        Session::flush();

        return Redirect::to('/')
                ->with('success_message', 'Hasta pronto '.$name.' !');
    }

    public function profile() 
    {
        $user = Auth::user();

        $fbProfile = Profile::where('user_id',$user->id)->where('type',Profile::Facebook)->first();
        $twProfile = Profile::where('user_id',$user->id)->where('type',Profile::Twitter)->first();

        $this->layout->content = View::make('sessions.profile')->with(['user' => $user, 'fbProfile' => $fbProfile, 'twProfile' => $twProfile]);
    }

    public function notificaciones() 
    {
        $user = Auth::user();

        $notifications = Notification::where('user_id',$user->id)->orderBy('created_at', 'desc')->limit(100)->paginate(15);

        $this->layout->content = View::make('sessions.notifications')->with(['user' => $user, 'notifications' => $notifications]);
    }

    public function updateData()
    {
        $user = Auth::user();
        $params=Input::only('name', 'surname', 'studies');

        if($user->update($params))
            return Redirect::back()->with('success_message', 'Información actualizada con exito.');
        else
            return Redirect::back()->withErrors($user->errors());
    }

    public function updatePassword() // TODO check password strength
    {
        $user = Auth::user();
        $params = Input::only('old_password', 'password', 'password_confirmation');
        if(Hash::check($params['old_password'],$user->password))
        { // correct password
            if(strcmp($params['password'],$params['password_confirmation'])==0)
            {
                $new=['password' => Hash::make($params['password'])];
                if($user->update($new))
                    // TODO: mail informando sobre el cambio de contraseña
                    return Redirect::back()->with('success_message', 'Contraseña actualizada con exito.');
                else 
                    return Redirect::back()->withErrors($user->errors());             
            }
            else // confirmación incorrecta
                return Redirect::back()->with('flash_message', 'La contraseña nueva y la confirmación no coinciden.');
        }
        else // incorrect password
            return Redirect::back()->with('flash_message', 'La contraseña antigua que has introducido no es correcta.');

    }

    public function updateDesc()
    {
        $user = Auth::user();
        $params=Input::only('short_desc');

        if($user->update($params)) // validation
            return Redirect::back()->with('success_message', 'La descripcion ha sido actualizada con exito.');
        else
            return Redirect::back()->withErrors($user->errors());

    }

    public function destroy()
    {
        $user = Auth::user();

        //Categories and resources
        foreach($user->categories as $category)
            App::make('CategoryController')->delete($user->username, $category->id);
        //Database
        ControllerHelpers::delete_user_rows($user);
        //Social
        if(in_array(Profile::Facebook, $user->profiles->lists('type')))
            App::make('FacebookController')->deleteLogin($user->username);

        $user->delete();

        return Redirect::to('/')->with('success_message', 'Hasta pronto! Esperamos verte de vuelta!');
    }

    // FOLLOW & UNFOLLOW

    public function follow($username)
    {
        $user = User::findByUsernameOrFail($username); 
        $follower = Auth::user();
        $follower->follow()->save($user);

        $user->newNotification()
        ->withSubject('Ahora '.$follower->username.' te esta siguiendo.')
        ->fromUser($follower)
        ->regarding($user)
        ->deliver();

        return Redirect::to('/usuarios/'.$username);
    }

    public function unfollow($username)
    {
        $user = User::findByUsernameOrFail($username);
        $follower = Auth::user();
        $follower->follow()->detach($user->id);

        // $user->newNotification()
        // ->withSubject('Ahora '.$follower->username.' te ha dejado de seguir.')
        // ->fromUser($follower)
        // ->regarding($user)
        // ->deliver();

        return Redirect::to('/usuarios/'.$username);
    }

    // FAV & UNFAV

    public function fav($username)
    {
        $user = User::findByUsernameOrFail($username); 
        $follower = Auth::user();
        $user->favers()->save($follower);
        DB::table('users')->where('id', $user->id)->increment('favs');

        $user->newNotification()
        ->withSubject('Ahora '.$follower->username.' te ha votado con +1.')
        ->fromUser($follower)
        ->regarding($user)
        ->deliver();

        return Redirect::to('/usuarios/'.$username);
    }

    public function unfav($username)
    {
        $user = User::findByUsernameOrFail($username); 
        $follower = Auth::user();
        $user->favers()->detach($follower);
        DB::table('users')->where('id', $user->id)->decrement('favs');

        return Redirect::to('/usuarios/'.$username);
    }

    // DEFAV & UNDEFAV

    public function defav($username)
    {
        $user = User::findByUsernameOrFail($username); 
        $follower = Auth::user();
        $user->defavers()->save($follower);
        DB::table('users')->where('id', $user->id)->decrement('favs');

        $user->newNotification()
        ->withSubject('Ahora '.$follower->username.' te ha votado con -1.')
        ->fromUser($follower)
        ->regarding($user)
        ->deliver();

        return Redirect::to('/usuarios/'.$username);
    }

    public function undefav($username)
    {
        $user = User::findByUsernameOrFail($username); 
        $follower = Auth::user();
        $user->defavers()->detach($follower);
        DB::table('users')->where('id', $user->id)->increment('favs');
        return Redirect::to('/usuarios/'.$username);
    }


}
