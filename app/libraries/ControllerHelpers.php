<?php

class ControllerHelpers {

  // --------> DIRECTORIES <--------------

  // Function to calculate quota of an user       
  public static function user_quota($user) {
      $f = public_path().'/files/usuarios/'.$user->username;
      $io = popen ( '/usr/bin/du -sk ' . $f, 'r' );
      $size = fgets ( $io, 4096);
      $size = substr ( $size, 0, strpos ( $size, "\t" ) );
      pclose ( $io );
      $data = ['quota' => $size];
      $user->update($data);
  }

	// Function to Copy folders and files       
  public static function rcopy($src, $dst) {
      if (file_exists ( $dst ))
          rrmdir ( $dst );
      if (is_dir ( $src )) {
          mkdir ( $dst, 0777, true );
          $files = scandir ( $src );
          foreach ( $files as $file )
              if ($file != "." && $file != "..")
                  ControllerHelpers::rcopy ( "$src/$file", "$dst/$file" );
      } else if (file_exists ( $src ))
          copy ( $src, $dst );
  }

	// Function to remove folders and files 
  public static function rrmdir($dir) {
      if (is_dir($dir)) {
          $files = scandir($dir);
          foreach ($files as $file)
              if ($file != "." && $file != "..") ControllerHelpers::rrmdir("$dir/$file");
          rmdir($dir);
      }
      else if (file_exists($dir)) unlink($dir);
  }

  // --------> RESOURCES <--------------

  // REMOVE: delete all the rows in the database for that resource
  public static function remove_resource_rows($resource){
    $resource->delete();
    DB::table('resources_favs')->where('resource_id', $resource->id)->delete();
    DB::table('resources_personal')->where('resource_id', $resource->id)->delete();
  }

  // REMOVE: delete all the rows in the database for that resource and his folders
  public static function remove_single_resource($path, $resource){
    ControllerHelpers::rrmdir($path);
    ControllerHelpers::remove_resource_rows($resource);
  }

  // REMOVE: delete a PDF file and his info for a resource
  public static function remove_pdf_file($path){
    $resource_id = basename($path);
    $resource = Resource::find($resource_id);
    if(is_file($path.'/'.$resource->pdf_name)) // si habia ya un PDF
    {
      unlink($path.'/'.$resource->pdf_name);
    }
    DB::table('resources')->where('id', $resource_id)->update(array('pdf_name' => ''));
  }

  // SAVE: save text file of a resource
  public static function save_text_file($path, $text){
    $file = fopen( $path.'/text', "w+" );
    if( $file == false )
    {
       echo ( "Error in opening file" );
       exit();
    }
    $filesize = filesize( $path.'/text' );
    // we eliminate any link other than an internal one
    $text = preg_replace('/href=[\\"\\\'][^#\\"\\\']*[\\"\\\']/','',$text);
    // eliminamos cualquier ' y lo cambiamos por " que nos la puede liar con el iframe
    $text = str_replace("'", "\"", $text);
    fwrite( $file, $text );
    fclose( $file );
  }

  // SAVE: save PDF file of a resource
  public static function save_pdf_file($path, $pdf){
    $resource_id = basename($path);
    $pdf_name = $pdf->getClientOriginalName();
    $pdf->move($path, $pdf_name);

    DB::table('resources')->where('id', $resource_id)->update(array('pdf_name' => $pdf_name));
  }

  // --------> USER <--------------

  public static function delete_user_rows($user){
    DB::table('user_favs')->where('user_id', $user->id)->delete();
    DB::table('user_defavs')->where('user_id', $user->id)->delete();
    DB::table('user_follows')->where('user_id', $user->id)->delete();
    DB::table('notifications')->where('from_user_id', $user->id)->delete();
  }

  // public static function share_resource_fb($user, $resource){
  //   $cipher = new Cipher();
  //   $profile = Profile::where('user_id',$user->id)->where('type',Profile::Facebook)->first();
    
  //   if(empty($profile) || !$profile->share)
  //     return ;

  //   $bitly = new Bitly();
  //   $urlmin = $bitly->shorten('https://www.passitedu.com/usuarios/'.$user->username.'/recursos/'.$resource->slug);
    
  //   $attachment = array
  //   (
  //   'access_token'=>$cipher->decrypt($profile->access_token),
  //   'message' => 'Acabo de subir un nuevo recurso a PassItEDU! '.$urlmin,
  //   'name' => $resource->title,
  //   'caption' => $user->username.'@'.$resource->category->title,
  //   'link' => 'http://www.passitedu.com/usuarios/'.$user->username.'/recursos/'.$resource->slug,
  //   'actions' => array('name'=>'PassItEDU','link'=>'http://www.passitedu.com'), 
  //   'description' => $resource->short_desc,
  //   'picture' => 'https://www.passitedu.com/images/mascotacolor.png'
  //   );
  //   $facebook = new Facebook(Config::get('facebook'));
  //   $result = $facebook->api($profile->uid.'/feed/','post',$attachment);
  // }

  public static function share_resource_tw($user, $resource){
    $cipher = new Cipher();
    $profile = Profile::where('user_id',$user->id)->where('type',Profile::Twitter)->first();
    
    if(empty($profile) || !$profile->share)
      return ;

    $bitly = new Bitly();
    $urlmin = $bitly->shorten('https://www.passitedu.es/usuarios/'.$user->username.'/recursos/'.$resource->slug);

    Twitter::set_new_config(array('token' => $cipher->decrypt($profile->access_token) , 'secret' => $cipher->decrypt($profile->access_token_secret)));
    Twitter::postTweet(array('status' => 'He subido un recurso a @PassItEDU - '.$resource->title.'@'.$resource->category->title.' '.$urlmin, 'format' => 'json'));
  }
}