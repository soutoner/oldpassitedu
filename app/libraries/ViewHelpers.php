<?php

	class ViewHelpers{
		
		// My common functions
		public static function loged_in($username)
		{
		    return Auth::check() && Auth::user()->username==$username;
		}

		// My common functions
		public static function not_loged_in($username)
		{
		    return !Auth::check() || Auth::user()->username!=$username;
		}

		// Tiempo que hace que se ha creado algo
		public static function time_ago($created_at)
		{
			return LocalizedCarbon::instance($created_at)->diffForHumans();
		}

		// given a category it will return all the resources that satisfy visibility terms of the user
		public static function category_resources($category, $slice = 0){
			$user = $category->user;

			$visibility = array('foo'); // default value so we dont get null
			if(!Auth::check()) // if you're not loged in you cant see registered and private ones
				array_push($visibility, Resource::Registrado, Resource::Privado);
			else{
				$viewer = Auth::user();
				if($viewer->username != $user->username) // you cant see private resources
					array_push($visibility, Resource::Privado);
			}

			if($slice>0) 	
				return Resource::where('user_id', $user->id)->where('category_id', $category->id)->whereNotIn('visibility', $visibility)->orderBy('created_at', 'desc')->take($slice)->get();
			else 	
				return Resource::where('user_id', $user->id)->where('category_id', $category->id)->whereNotIn('visibility', $visibility)->orderBy('created_at', 'desc')->get();

		}

		// given a resource it returns the href to the resource
		public static function resource_link($resource){
			return '/usuarios/'.$resource->user->username.'/recursos/'.$resource->slug;
		}

		// given an user calculates his percentage of quota usage
		public static function resource_mini($resource){
				return "<div class='res-placehold'>".$resource->mini."</div>";
		}

		// given an user calculates his percentage of quota usage
		public static function quota_percentage($user, $round = 0){
			return round(($user->quota*100)/User::Quota,$round);
		}

		// given a resource it returns the href to the resource
		public static function category_link($category){
			return '/usuarios/'.$category->user->username.'/categorias/'.$category->slug;
		}


		public static function userSearch($buscador){
			$palabras = explode(" ",$buscador);
			$found = array();
			$size = count($palabras);
			for($i = 0;$i < $size;$i++){
				//NO HAY USUARIO CON EL NOMBRE SEPARADO
				if(strlen($palabras[$i]) < 3){
					return Redirect::to('/')->with('flash_message','Has escrito alguna palabra menor de 3 letras');
				}
				else{
					//ENCONTRAR USUARIOS SIMILARES AL BUSCADO
					$start =$palabras[$i][0].$palabras[$i][1].$palabras[$i][2];
					$cont = strlen($palabras[$i]);
					$finish = $palabras[$i][$cont-3].$palabras[$i][$cont-2].$palabras[$i][$cont-1];
					$users = User::where('username', '=' , $palabras[$i])->orWhere('username', 'like',"$start%")->orWhere('username', 'like',"%$finish")->lists('id');

					if(count($users)==0){
						//NO EXISTE TAL USUARIO					
					 	return $users;
					}else{
						//HA ENCONTRADO AL MENOS 1 TODO ELEGIR EL QUE ES IGUAL AL NOMBRE BUSCADO Y PONERLO EL PRIMERO
						foreach ($users as $user){	
							/*if(!in_array ( $user , $found )){
								$aux = User::where('id','=',$user)->lists('username');
								if(strcmp($aux[0],$palabras[$i])== 0){
									array_unshift($found, $user);
								}else{*/
									array_push($found,$user);
								/*}
							}*/
						}
					}
				}					
			}
			//DEVUELVE ARRAY DE IDS
			return $found;
		}

		/**
		*  Busqueda sin restricciones
		*  por nombre -> %name% y por apellidos -> %surname%
		*  (se usa en router para 'utils/busquedaUsuario')
		**/
		public static function userSearchEach($buscador){
	      	$buscadorPorPalabras=explode(" ",$buscador);
	      	/*
	      	*  [0] -> nombre
	      	*/
			$nombre = $buscadorPorPalabras[0];
			/*
			*  [1+] -> apellido/s
			*/
			$apellidos = '';
			for ($i=1; $i < count($buscadorPorPalabras) ; $i++) { 
			    $apellidos .= $buscadorPorPalabras[$i].' ';
			}
			$apellidos = trim($apellidos);
        	return User::where('name', 'like' , "%$nombre%")->where('surname', 'like' , "%$apellidos%")->lists('id');
		}
		/**
		*  Busqueda sin restricciones
		*  por titulo -> %title%
		*  (se usa en router para 'utils/busquedaApuntes')
		**/
		public static function resourcesSearchEach($buscador){
        	return Resource::where('title', 'like' , "%$buscador%")->lists('id');
		}
		/**
		*  LeftJoin de notificaciones y users
		*  return tupla + el gravatar del usuario (from_user_id)
		**/
		public static function popNotifications($userId){
			$notification =	DB::table('users')
		        ->join('notifications', function($join) use($userId){
		            $join->on('users.id', '=', 'notifications.from_user_id')
		                 ->where('notifications.user_id', '=', $userId);
				})
				->orderBy('notifications.created_at', 'desc')
				->first();
				$gravatar = User::find($notification->from_user_id)->gravatar();
        	return array_add(array_dot($notification), 'gravatar', $gravatar);
		}

		public static function noteSearch($buscador){
			$found = array();
			$found = ViewHelpers::keywordSearcher($buscador,$found);
			//BUSCAMOS APUNTES
			$aux = Resource::where('user_id',array(Auth::id()))->lists('id');
			if(empty($aux))
			$resources = Resource::where('title','=',$buscador)->orWhere('title', 'like','%'.$buscador.'%')->whereNotIn('id',array(0))->orderBy('favs','desc')->lists('id');
			else{
				$resources = Resource::where('title','=',$buscador)->orWhere('title', 'like','%'.$buscador.'%')->whereNotIn('id',$aux)->orderBy('favs','desc')->lists('id');
			}
			//Elimina repetidos
			foreach ($resources as $resource) {
				if(!in_array($resource, $found)){
					array_unshift($found, $resource);
				}
			}
			if(empty($found)){
				Session::flash('flash_message','Los apuntes que estas buscando no existen, ni tampoco se parece a otro existente');
			}
			return $found;

		}
		public static function keywordSearcher($buscador,$found){	
			$ids = Resource::lists('keywords','id');
			$palabras = explode(" ",$buscador);
			$aux = array_keys($ids);
			foreach ($palabras as $palabra) {
				for ($i=0; $i < count($ids); $i++) { 
					$etiquetas =explode(",",$ids[$aux[$i]]);
					if(in_array($palabra, $etiquetas)){
						 $res = Resource::where('id','=',$aux[$i])->lists('id');
						 //Elimina repetidos
						 	foreach ($res as $row) {
						 		if(!in_array ( $row , $found )){
									array_push($found, $row);			
								}
						 	}
					}
				}
			}
			return $found;
		}
		public static function keywordAdvancedSearcher($palabras,$found,$curso,$users,$usuarios){
			$ids = Resource::lists('keywords','id');	
			$aux = array_keys($ids);
			$auxiliar = Resource::where('user_id',array(Auth::id()))->lists('id');
			foreach ($palabras as $palabra) {
				for ($i=0; $i < count($aux); $i++) {
					$etiquetas =explode(",",$ids[$aux[$i]]);
					if(in_array($palabra, $etiquetas)){
						//BUSQUEDA EN MIS U OTROS USUARIOS
						if(strcmp($usuarios, "todos") != 0){
							foreach ($users as $user) {
								if(strcmp($curso, "todos")!= 0){
									if(empty($auxiliar))
										$res = Resource::where('id','=',$aux[$i])->where('user_id', '=', $user->id)->whereNotIn('id',array(0))->where('curso','=',$curso)->lists('id');
									else
						 				$res = Resource::where('id','=',$aux[$i])->where('user_id', '=', $user->id)->whereNotIn('id',$auxiliar)->where('curso','=',$curso)->lists('id');
						 		}else{
						 			if(empty($auxiliar))
						 				$res = Resource::where('id','=',$aux[$i])->where('user_id', '=', $user->id)->whereNotIn('id',array(0))->lists('id');
						 			else
						 				$res = Resource::where('id','=',$aux[$i])->where('user_id', '=', $user->id)->whereNotIn('id',$auxiliar)->lists('id');
						 			
						 		}
						 		foreach ($res as $row) {
						 			if(!in_array ( $row , $found )){
										array_push($found, $row);
									}
						 		}
							}
						//BUSQUEDA EN TODOS LOS USUARIOS
						}else {
							if(empty($auxiliar))
								$res = Resource::where('id','=',$aux[$i])->where('curso','=',$curso)->whereNotIn('id',array(0))->lists('id');
							else
							$res = Resource::where('id','=',$aux[$i])->where('curso','=',$curso)->whereNotIn('id',Resource::where('user_id',array(Auth::id()))->lists('id'))->lists('id');
							foreach ($res as $row) {
						 		if(!in_array ( $row , $found )){
									array_push($found, $row);
								}
						 	}
						}			
					}
				}
			}
			return $found;
		}
	}
