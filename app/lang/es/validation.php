<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| válidoation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the válidoator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => "El :attribute debe ser aceptado.",
	"active_url"           => "El :attribute no es una URL válida.",
	"after"                => "El :attribute debe ser una fecha posterior a :date.",
	"alpha"                => "El :attribute debe contener solo letras.",
	"alpha_dash"           => "El :attribute debe contener solo letras, números y guiones.",
	"alpha_num"            => "El :attribute debe contener solo letras y números.",
	"array"                => "El :attribute debe ser un array.",
	"before"               => "El :attribute debe ser una fecha anterior a :date.",
	"between"              => array(
		"numeric" => "El :attribute debe estar entre :min y :max.",
		"file"    => "El :attribute debe estar entre :min y :max kilobytes.",
		"string"  => "El :attribute debe estar entre :min y :max caracteres.",
		"array"   => "El :attribute debe tener entre :min y :max elementos.",
	),
	"boolean"              => "El :attribute campo debe ser true o false.",
	"confirmed"            => "El :attribute la confirmación no coincide.",
	"date"                 => "El :attribute no es una fecha válida.",
	"date_format"          => "El :attribute no coincide con el formato :format.",
	"different"            => "El :attribute y :other deben ser diferentes.",
	"digits"               => "El :attribute debe tener :digits dígitos.",
	"digits_between"       => "El :attribute debe estar entre :min y :max dígitos.",
	"email"                => "El :attribute debe ser una dirección de correo válida.",
	"exists"               => "El :attribute seleccionado no es válido.",
	"image"                => "El :attribute debe ser una imagen.",
	"in"                   => "El :attribute seleccionado no es válido.",
	"integer"              => "El :attribute debe ser un entero.",
	"ip"                   => "El :attribute debe ser una dirección IP válida.",
	"max"                  => array(
		"numeric" => "El :attribute no debe ser mayor de :max.",
		"file"    => "El :attribute no debe ser mayor de :max kilobytes.",
		"string"  => "El :attribute no debe ser mayor de :max caracteres.",
		"array"   => "El :attribute no debe tener mas de :max elementos.",
	),
	"mimes"                => "El :attribute debe ser un fichero de tipo: :values.",
	"min"                  => array(
		"numeric" => "El :attribute debe tener al menos :min.",
		"file"    => "El :attribute debe tener al menos :min kilobytes.",
		"string"  => "El :attribute debe tener al menos :min caracteres.",
		"array"   => "El :attribute debe tener al menos :min elementos.",
	),
	"not_in"               => "El :attribute seleccionado no es válido.",
	"numeric"              => "El :attribute debe ser númerico.",
	"regex"                => "El formato del :attribute no es válido.",
	"required"             => "El campo :attribute es obligatorio.",
	"required_if"          => "El campo :attribute es obligatorio cuando :other es :value.",
	"required_with"        => "El campo :attribute es obligatorio cuando :values esta presente.",
	"required_with_all"    => "El campo :attribute es obligatorio cuando :values esta presente.",
	"required_without"     => "El campo :attribute es obligatorio cuando :values no esta presente.",
	"required_without_all" => "El campo :attribute es obligatorio cuando ninguno de :values estan presentes.",
	"same"                 => "El :attribute y :other deben coincidir.",
	"size"                 => array(
		"numeric" => "El :attribute debe ser de tamaño :size.",
		"file"    => "El :attribute debe ser de tamaño :size kilobytes.",
		"string"  => "El :attribute debe ser de tamaño :size caracteres.",
		"array"   => "El :attribute debe contener :size elementos.",
	),
	"unique"               => "El :attribute ya esta en uso.",
	"url"                  => "El formato de :attribute no es válido.",

	/*
	|--------------------------------------------------------------------------
	| Custom válidoation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom válidoation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom válidoation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
