<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "La contraseña tiene que tener al menos 6 carácteres, una mayuscula y un número.",

	"user" => "No podemos encontrar a un usuario con esa dirección de e-mail.",

	"token" => "Este token para reestablecer la contraseña no es válido.",

	"sent" => "Password reminder enviado!",

);
