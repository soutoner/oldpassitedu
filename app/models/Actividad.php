<?php
// DEPRECATED ***********
// // ardent validation
// use LaravelBook\Ardent\Ardent;

// class Actividad extends Ardent {

// 	/* ENUM TYPE */
// 	const Follow = 0;
//   const AddCategory = 1;
//   const AddResource = 2;
//   const Fav = 3;
//   const Defav = 4;

// 	/**
// 	 * The database table used by the model.
// 	 *
// 	 * @var string
// 	 */
// 	protected $table = 'actividades';

// 	/*Mass asignment*/
// 	protected $fillable = array('content', 'type', 'user_id');
// 	/*No Mass assignment*/
// 	protected $guarded = array('id');

// 	/**
// 	 * Actividad parent user
// 	 */
// 	public function resources()
// 	{
// 	  return $this->belongsTo('User', 'user_id');
// 	}

}