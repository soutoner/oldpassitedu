<?php

	class Profile extends Model {
		/* ENUM TYPE */
		const Facebook = 0;
	  const Twitter = 1;

	  public function beforeSave() {
	    return true;
	    //or don't return nothing, since only a boolean false will halt the operation
		}	
		
		public function beforeUpdate() {
	    return $this->beforeSave();
		}	
		

		// User that owns this profile
		public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
}