<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use \Illuminate\Database\Eloquent\ModelNotFoundException;

	Validator::extend('olderThan', function($attribute, $value, $parameters)
	{
	$minAge = ( ! empty($parameters)) ? (int) $parameters[0] : 13;
	return (new DateTime)->diff(new DateTime($value))->y >= $minAge;

	//validación de la edad, por defecto 13
	});

	class User extends Model implements UserInterface, RemindableInterface {
		use UserTrait, RemindableTrait;

		/* ENUM TYPE */
		const Quota = 512000;

		public function beforeUpdate() {
	  	// el nombre de usuario y email en letras minusculas
	    $this->email = strtolower($this->email);
	    $this->username = strtolower($this->username);

	    // el nombre y apellidos bien formateado
	    $this->name = strtolower($this->name);
	    $this->name = ucwords($this->name);
	    $this->surname = strtolower($this->surname);
	    $this->surname = ucwords($this->surname);

	    return true;
		}	

		public function beforeSave() {
	    //password
	    $this->password = Hash::make($this->password);
	    $this->beforeUpdate();
	   
	    return true;
	    //or don't return nothing, since only a boolean false will halt the operation
	  }	

		/*Nombre por defecto de la tabla 'users'*/

		/*Mass asignment*/
		protected $fillable = array('username', 'email', 'name', 'surname', 'gender', 'studies', 'birth', 'password', 'short_desc','quota');
		/*No Mass assignment*/
		protected $guarded = array('id','favs', 'remember_token');
		protected $hidden = array('password', 'remember_token');

		/*Validation rules*/
		public static $rules = array(
	    'username'    => array('required','between:3,15','regex:/\A[a-zA-Z0-9]+\Z/','unique:users'),
	    'email'       => array('required','max:50','regex:/\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i','unique:users'),
	    'name'   	    => array('required','between:2,25','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'surname'     => array('required','between:2,50','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'gender'      => array('required','max:1','regex:/\A[HM]+\Z/'),
	    'password'		=> array('required','regex:/^(?=.*[A-Z])(?=.*[0-9]).{6,}$/'),
	    'studies' 	  => array('max:50','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'short_desc' 	=> array('max:140'),
	    'birth' 			=> array('required','max:10','olderThan:16','after:1920-01-01','regex:/\A[0-9\-]+\Z/')
	  );
	  public static $updateRules = array(
	    'username'    => array('required','between:3,15','regex:/\A[a-zA-Z0-9]+\Z/'),
	    'email'       => array('required','max:50','regex:/\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i'),
	    'name'   	    => array('required','between:2,25','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'surname'     => array('required','between:2,50','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'gender'      => array('required','max:1','regex:/\A[HM]+\Z/'),
	    'password'		=> array('required','regex:/^(?=.*[A-Z])(?=.*[0-9]).{6,}$/'),
	    'studies' 	  => array('max:50','regex:/\A[a-zA-Záéíóú\ ]+\Z/'),
	    'short_desc' 	=> array('max:140'),
	    'birth' 			=> array('required','max:10','olderThan:16','after:1920-01-01','regex:/\A[0-9\-]+\Z/')
	  );

		/* Custom error messages TODO*/
	  public static $messages = array(
	    'required' => 'El campo :attribute no puede estar vacío.',
	    'unique' => 'El campo :attribute que has introducido esta actualmente en uso.',
	    'between' => 'El campo :attribute debe estar entre :min y :max.',
	    'regex' => 'El campo :attribute no tiene un formato válido.',
	    'max' => 'El campo :attribute no puede tener más de :max carácteres.',
	    'min' => 'El campo :attribute no puede tener menos de :min carácteres.',
	    'older_than' => 'Debes tener al menos 16 años.',
	    'after' => 'La fecha de nacimiento que has introducido es demasiado antigua.',
	    'confirmed' => 'La contraseña y la confirmación no coinciden.'
	  );

	  /**
     * Find by username, or throw an exception.
     *
     * @param string $username The username.
     * @param mixed $columns The columns to return.
     *
     * @throws ModelNotFoundException if no matching User exists.
     *
     * @return User
     */
    public static function findByUsernameOrFail( $username, $columns = array('*') ) {
        if ( ! is_null($user = static::whereUsername($username)->first($columns))) {
            return $user;
        }

        throw new ModelNotFoundException;
    }

    /**
     * Find by username, or throw an exception.
     *
     * @param string $username The username.
     * @param mixed $columns The columns to return.
     *
     * @throws ModelNotFoundException if no matching User exists.
     *
     * @return User
     */
    public static function findByEmailOrFail( $email, $columns = array('*') ) {
        if ( ! is_null($user = static::whereEmail($email)->first($columns))) {
            return $user;
        }

        throw new ModelNotFoundException;
    }

    // ----------> RELATIONSHIPS <-----------
	  /**
		 * User following relationship
		 */
		public function follow()
		{
		  return $this->belongsToMany('User', 'user_follows', 'user_id', 'follow_id');
		}
		/**
		 * User followers relationship
		 */
		public function followers()
		{
		  return $this->belongsToMany('User', 'user_follows', 'follow_id', 'user_id');
		}
		/**
		 * User faver relationship
		 */
		public function favers()
		{
		  return $this->belongsToMany('User', 'user_favs', 'user_id', 'faver_id');
		}
		/**
		 * User faver relationship
		 */
		public function defavers()
		{
		  return $this->belongsToMany('User', 'user_defavs', 'user_id', 'defaver_id');
		}
		/**
		 * User faved resources relationship
		 */
		public function favedResources()
		{
		  return $this->belongsToMany('Resource', 'resources_favs', 'user_id', 'resource_id');
		}
		/**
		 * User faved resources relationship
		 */
		public function personalResources()
		{
		  return $this->belongsToMany('Resource', 'resources_personal', 'user_id', 'resource_id');
		}
		/**
		 * User categories relationship
		 */
		public function categories()
		{
		  return $this->hasMany('Category', 'user_id');
		}
		/**
		 * User resources relationship
		 */
		public function resources()
		{
		  return $this->hasMany('Resource', 'user_id');
		}
		public function profiles()
    {
      return $this->hasMany('Profile', 'user_id');
    }
    // In your User model - 1 User has Many Notifications
		public function notifications()
		{
		    return $this->hasMany('Notification');
		}

		// ----------> BOOLEANS <-----------
		// to know if someone is followed by somebody
		public function followed_by($id)
		{
			return $this->followers->contains($id);
		}
		public function faved_by($id)
		{
			return $this->favers->contains($id);
		}
		public function defaved_by($id)
		{
			return $this->defavers->contains($id);
		}

		// FUNCIONES AUXILIARES
		public function gravatar($size=250)
		{
			$hash = md5(strtolower(trim($this->attributes['email'])));
			return "https://secure.gravatar.com/avatar/$hash.png?s=$size";
		}
		public function colorFavs()
		{
			if($this->attributes['favs'] > 0)
				return 'style="color:green;background:#D8D8D8;"';
			else
				return 'style="color:green;background:#D8D8D8;"';
		}
		public function getCompleteNameAttribute()
		{
			return $this->attributes['name'].' '.$this->attributes['surname'];
		}
		public function newNotification()
		{
	    $notification = new Notification;
	    $notification->user()->associate($this);
	 
	    return $notification;
		}
}