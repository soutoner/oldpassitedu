<?php

	Validator::resolver(function($translator, $data, $rules, $messages)
	{
	  return new CustomValidator($translator, $data, $rules, $messages);
	});

	Validator::extend('validYoutubeURL', function($attribute, $value, $parameters)
	{
		$parts = parse_url($value);
		return !empty($parts['host']) && ($parts['host'] == 'www.youtube.com' || $parts['host'] == 'www.youtube.be') && !empty($parts['query']);
	});

	class Resource extends Model {

		/* ENUM TYPE */
		const Publico = 'pu';
	  const Privado = 'pr';
	  const Registrado = 'r';

	  const Secundaria = 'Secundaria';
	  const Bachillerato = 'Bachillerato';
	  const Universidad = 'Universidad';
	  const Otro = 'Otro';

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'resources';

		public function beforeSave() {
		    //$this->title = strtolower($this->title);
		    //$this->title = ucfirst($this->title);

		    $this->keywords = strtolower($this->keywords);
		    $this->keywords = CustomValidator::stripAccents($this->keywords);
		    
		    return true;
		    //or don't return nothing, since only a boolean false will halt the operation
		}
		public function beforeUpdate() {
	    return $this->beforeSave();
		}	

		/*Mass asignment*/
		protected $fillable = array('title', 'short_desc','youtube_url', 'curso', 'keywords', 'category_id', 'visibility','slug');
		/*No Mass assignment*/
		protected $guarded = array('id', 'favs');

		/*Validation rules*/
		public static $rules = array(
	    'title'    => array('required','between:3,50','resourceTitleNotRepeated:user_id'),
	    'short_desc' 	=> array('required','max:140'),
	    'youtube_url' => array('between:3,100','validYoutubeURL'),
	    'keywords'    => array('regex:/\A[a-zA-Záéíóú\,]+\Z/')
	  );
	  public static $updateRules = array(
	    'title'    => array('required','between:3,50','resourceTitleNotRepeated:user_id,id'),
	    'short_desc' 	=> array('required','max:140'),
	    'youtube_url' => array('between:3,100','validYoutubeURL'),
	    'keywords'    => array('regex:/\A[a-zA-Záéíóú\,]+\Z/')
	  );

		/* Custom error messages TODO*/
	  public static $messages = array(
	    'required' => 'El campo :attribute no puede estar vacío.',
	    'between' => 'El :attribute debe estar entre :min y :max.',
	    'max' => 'El :attribute no puede tener más de :max carácteres.',
	    'resource_title_not_repeated' => 'Ya tienes un recurso con este titulo.',
	    'regex' => 'El :attribute no tiene un formato válido.',
	    'valid_youtube_ur_l' => 'La URL de Youtube no tiene el formato correcto.'
	  );

	  /**
		 * Resource parent category
		 */
		public function category()
		{
		  return $this->belongsTo('Category', 'category_id');
		}
		 /**
		 * Resource parent user
		 */
		public function user()
		{
		  return $this->belongsTo('User', 'user_id');
		}
		/**
		 * Resource faver relationship
		 */
		public function favers()
		{
		  return $this->belongsToMany('User', 'resources_favs', 'resource_id', 'user_id');
		}
		/**
		 * Resource adders relationship
		 */
		public function adders()
		{
		  return $this->belongsToMany('User', 'resources_personal', 'resource_id', 'user_id');
		}

		// ----------> BOOLEANS <-----------
		// to know if the resource is faved by somebody
		public function faved_by($id)
		{
			return $this->favers->contains($id);
		}
		// to know if the resource is added by somebody
		public function added_by($id)
		{
			return $this->adders->contains($id);
		}

		// ----------> FUNCIONES AUXILIARES <-----------
		public function getTextAttribute()
		{
			$path = public_path().'/files/usuarios/'.$this->user->username.'/'.$this->category->id.'/'.$this->attributes['id'];
			$file = fopen( $path.'/text', "r" );
			if( $file == false )
			{
			   echo ( "Error in opening file" );
			   exit();
			}
			$filesize = filesize( $path.'/text' );
			$filetext = fread( $file, $filesize );

			fclose( $file );
			return $filetext;
		}
		public function getMiniAttribute()
		{
			return strip_tags(Str::limit($this->text,400));
		}
		public function getYoutubeIDAttribute()
		{
			$youtube_url = $this->attributes['youtube_url'];
			// We get an array of index-value
			parse_str( parse_url( $youtube_url, PHP_URL_QUERY ), $url_vars );
			// We return only the id
			// compatibilidad con base de datos donde solo se encuentre el ID
			return empty($url_vars['v']) ? $youtube_url : $url_vars['v'];
		}
		public function colorFavs()
		{
			if($this->attributes['favs'] > 0)
				return 'style="color:green;background:#D8D8D8;"';
			else
				return 'style="color:green;background:#D8D8D8;"';
		}
}