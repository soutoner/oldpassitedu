<?php

class Bitly {
	var $path;
	var $user; 
	var $key;
	function Bitly () {
		$this->path = "http://api.bit.ly/v3/";
		$this->user = Config::get('bitly.user');
		$this->key = Config::get('bitly.api-key');
	}
	function shorten($url) {
		$temp = $this->path."shorten?login=".$this->user."&apiKey=".$this->key."&uri=".$url."&format=txt";
		$data = file_get_contents($temp);
		return $data;
	}
	function expand($url) {
		$temp = $this->path."expand?login=".$this->user."&apiKey=".$this->key."&shortUrl=".$url."&format=txt";
		$data = file_get_contents($temp);
		return $data;
	}	
}