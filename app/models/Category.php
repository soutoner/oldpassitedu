<?php

	Validator::resolver(function($translator, $data, $rules, $messages)
	{
	  return new CustomValidator($translator, $data, $rules, $messages);
	});

	class Category extends Model {

		public function beforeSave() {
	    // el nombre de usuario y email en letras minusculas
			//$this->title = strtolower($this->title);
	    //$this->title = ucfirst($this->title);

	    return true;
	    //or don't return nothing, since only a boolean false will halt the operation
		}	

		public function beforeUpdate() {
	    return $this->beforeSave();
		}	

		/**
		 * The database table used by the model.
		 *
		 * @var string
		 */
		protected $table = 'categories';

		/*Mass asignment*/
		protected $fillable = array('title', 'short_desc','slug');
		/*No Mass assignment*/
		protected $guarded = array('id');

		/*Validation rules*/
		public static $rules = array(
	    'title'    => array('required','between:3,50', 'categoryTitleNotRepeated:user_id'),
	    'short_desc' 	=> array('required','max:140')
	  );
	  public static $updateRules = array(
	    'title'    => array('required','between:3,50', 'categoryTitleNotRepeated:user_id,id'),
	    'short_desc' 	=> array('required','max:140')
	  );

		/* Custom error messages TODO*/
	  public static $messages = array(
	    'required' => 'El campo :attribute no puede estar vacío.',
	    'between' => 'El :attribute debe estar entre :min y :max.',
	    'max' => 'El :attribute no puede tener más de :max carácteres.',
	    'category_title_not_repeated' => 'Ya tienes una categoria con este titulo.'
	  );


		/**
		 * Category resources relationship
		 */
		public function resources()
		{
		  return $this->hasMany('Resource', 'category_id');
		}

		/**
		 * Category user parent
		 */
		public function user()
		{
		  return $this->belongsTo('User', 'user_id');
		}

}