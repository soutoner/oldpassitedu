<?php

class Notification extends Eloquent
{
    protected $fillable   = ['user_id', 'from_user_id', 'type', 'subject', 'object_id', 'object_type'];
 
    public function getDates()
    {
      return ['created_at', 'updated_at'];
    }
 
    public function user()
    {
      return $this->belongsTo('User');
    }
    public function from()
    {
      return $this->belongsTo('User', 'from_user_id');
    }
		public function scopeUnread($query)
		{
		  return $query->where('is_read', '=', 0)->orderBy('created_at', 'desc')->limit(20);
		}

    // Setters and getters
    public function withSubject($subject)
		{
	    $this->subject = $subject;
	 
	    return $this;
		}

    public function fromUser($user)
    {
      $this->from_user_id = $user->id;
   
      return $this;
    }
		 
		public function regarding($object)
		{
	    if(is_object($object))
	    {
	        $this->object_id   = $object->id;
	        $this->object_type = get_class($object);
	    }
	 
	    return $this;
		}
		public function deliver()
		{
			//$this->sent_at = new Carbon;
			$this->save();

			return $this;
		}
    public function readed()
    {
      DB::table('notifications')->where('id', $this->id)->update(['is_read' => 1]);
    }
    public function uri()
    {
      if($this->hasValidObject()){
        if($this->object_type == "Resource") 
          return '/usuarios/'.$this->user->username.'/recursos/'.Resource::find($this->getObject()->id)->slug;
        else if($this->object_type == "User") 
          return '/usuarios/'.$this->from->username;
      } else {
        return '';
      }
    }
		public function hasValidObject()
    {
      try
      {
          $object = call_user_func_array($this->object_type . '::findOrFail', [$this->object_id]);
      }
      catch(\Exception $e)
      {
          return false;
      }

      $this->relatedObject = $object;

      return true;
    }
 
    public function getObject()
    {
      if($this->relatedObject)
      {
          $hasObject = $this->hasValidObject();

          if(!$hasObject)
          {
              throw new \Exception(sprintf("No valid object (%s with ID %s) associated with this notification.", $this->object_type, $this->object_id));
          }
      }

      return $this->relatedObject;
    }
}