<?php

  class CustomValidator extends Illuminate\Validation\Validator {

    public function validateCategoryTitleNotRepeated($attribute, $value, $parameters)
    {
      $user_id = $this->getValue($parameters[0]);

      if(sizeof($parameters)>1)
      {
        $id =[$this->getValue($parameters[1])];
        $titles = array_map('strtolower', Category::where('user_id',$user_id)->whereNotIn('id',$id)->lists('title'));
      } else {
        $titles = array_map('strtolower', Category::where('user_id',$user_id)->lists('title'));
      }

  		return !in_array(strtolower($this->getValue($attribute)),$titles);
    }

    public function validateResourceTitleNotRepeated($attribute, $value, $parameters)
    {
      $user_id = $this->getValue($parameters[0]);

      if(sizeof($parameters)>1)
      {
        $id = [$this->getValue($parameters[1])];
        $titles = array_map('strtolower', Resource::where('user_id',$user_id)->whereNotIn('id',$id)->lists('title'));
      }
      else
      {
        $titles = array_map('strtolower', Resource::where('user_id',$user_id)->lists('title'));
      }

  		return !in_array(strtolower($this->getValue($attribute)),$titles);
    }

    public static function stripAccents($string){
      return strtr($string,'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
    'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

}