<?php
class Cipher {
    private $securekey, $iv;
    function __construct() {
        $this->securekey = Config::get('app.key');
        $this->iv = mcrypt_create_iv(32);
    }
    function encrypt($input) {
        return base64_encode(mcrypt_encrypt(Config::get('app.cipher'), $this->securekey, $input, MCRYPT_MODE_ECB, $this->iv));
    }
    function decrypt($input) {
        return trim(mcrypt_decrypt(Config::get('app.cipher'), $this->securekey, base64_decode($input), MCRYPT_MODE_ECB, $this->iv));
    }
}