<?php

// Bitly app Config 
return array(
        'user' => $_ENV['BITLY-USER'],
        'api-key' => $_ENV['BITLY-API-KEY']
    );