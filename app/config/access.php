<?php

return array(

	'reminder' => array(

		'email' => 'emails.access.welcome',

		'table' => 'access_reminders',

		'expire' => 60,

	),

);
