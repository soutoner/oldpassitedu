<?php

class StaticPagesTest extends TestCase {

	protected $params = array(
        'name'       => 'Pepito',
        'surname'    => 'Mengano',
        'studies'    => 'Master en recoger caracoles',
        'gender'     => 'H',
        'username'   => 'pepitoGuapo',
        'email'      => 'email@ejemplo.es',
        'password'   => 'foobar6Y',
        'short_desc' => 'Hola soy Pepito Mengano y aqui manda mi **s#@**',
        'birth'      => '11-11-1994'
  );

	public function testGetWelcome()
	{
		$crawler = $this->client->request('GET', '/');

		$this->assertCount(1, $crawler->filter('h2:contains("Bienvenidos")'));
	}

	public function testRootPageUserLogedIn(){
		Route::enableFilters();
		// user loged in -> profile
		$user = new User($this->params);
		$user->save();
		Auth::login($user);
		$crawler = $this->client->request('GET', '/');
		$this->assertCount(1, $crawler->filter('title:contains('.$user->username.')'));
	}

	public function testRootPageUserGuest(){
		Route::enableFilters();
		// guest -> welcome
		$crawler = $this->client->request('GET', '/');
		$this->assertCount(1, $crawler->filter('h2:contains("Bienvenidos")'));
	}

	public function testGetAbout()
	{
		$crawler = $this->client->request('GET', '/about');

		$this->assertCount(1, $crawler->filter('h1:contains("Sobre Nosotros")'));
	}

	public function testGetContact()
	{
		$crawler = $this->client->request('GET', '/contacto');

		$this->assertCount(1, $crawler->filter('h1:contains("Contacto")'));
	}

	public function testGetPolicy()
	{
		$crawler = $this->client->request('GET', '/politicas');

		$this->assertCount(1, $crawler->filter('h1:contains("Políticas de Privacidad")'));
	}

	public function testGetCond()
	{
		$crawler = $this->client->request('GET', '/condiciones');

		$this->assertCount(1, $crawler->filter('h1:contains("Términos de Uso")'));
	}
	public function testGetFaq()
	{
		$crawler = $this->client->request('GET', '/faq');

		$this->assertCount(1, $crawler->filter('h1:contains("FAQ - Preguntas Frecuentes")'));
	}

}
