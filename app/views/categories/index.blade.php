@section('title', 'Categorías de '.$user->username)

@section('header')
@parent
<div class="container-fluid bodyheader">
	<!-- avatar !-->
	<div class="col-md-1" style="margin:0px;padding:0px;">
		<img src="{{$user->gravatar(100)}}"/>
	</div>
	<!-- FIN avatar !-->
	<!-- info !-->
	<div class="col-md-8" style="margin:0px;">
		<h4>{{$user->completeName}} - <small><a href="/usuarios/{{$user->username}}">@{{{$user->username}}}</a></small></h4>
		<div class="well well-sm">{{$user->short_desc}}</div>
		<a><small>Reportar usuario</small></a>
	</div>
	<!-- FIN info !-->
	<!-- estadisticas !-->
	<div class="col-md-3">
		<ul class="list-group">
			<li class="list-group-item">Categorias: <span class="badge">{{$user->categories->count()}}</span></li>
			<li class="list-group-item">Recursos: <span class="badge">{{$user->resources->count()}}</span></li>
		</ul>
		<small class="pull-right">Ultima actualizacion: @if($user->resources->last()) {{ViewHelpers::time_ago($user->resources->last()->created_at)}} @else Nunca @endif</small>
	</div>
	<!-- FIN estadisticas !-->
</div>
@endsection

@section('content')
<!-- BODY !-->
<div class="row col-centered bodypanel">
	@include('publi/160x600left')
	<!-- panel central !-->
	<div class="col-md-8">
		<h4>Categorias de {{$user->username}}</h4> <br>
			<!-- accordeon !-->
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  @forelse($categories as $cat)
				  <div class="panel panel-default">
				    <div class="panel-heading" role="tab" id="heading{{$cat->id}}">
				      <h4 class="panel-title">
				        <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{$cat->id}}" aria-expanded="true" aria-controls="collapse{{$cat->id}}">
				          {{$cat->title}} <span class="pull-right badge">{{$cat->resources->count()}}</span>
				        </a>
				      </h4>
				    </div>
				    <div id="collapse{{$cat->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$cat->id}}">
				      <!-- recursos de categoria !-->
				      <div class="panel-body">
				      	<p>Descripcion:</p>
				      	<p style="margin-left:10px;">{{$cat->short_desc}}</p>
				      	<p>Ultimos recursos:</p>
								@forelse(ViewHelpers::category_resources($cat,5) as $res)
									<a class="list-group-item" href="{{ViewHelpers::resource_link($res)}}">
										{{$res->title}} <small class="pull-right">{{ViewHelpers::time_ago($res->created_at)}}</small>
									</a>
								@empty
									<p>El usuario no ha añadido ningun recurso...</p>
								@endforelse
				      </div>
				      <!-- FIN recursos de categoria !-->
				    </div>
				  </div>
			  @empty
			  	<p>El usuario no ha añadido categorias...</p>
			  @endforelse
			</div>
		<!-- FIN accordeon !-->
		<center>{{$categories->links()}}</center>
	</div>
	<!-- FIN panel central !-->
	@include('publi/160x600right')
</div>
<!-- FIN BODY !-->
@endsection