@section('title', 'Editar '.$category->title)

@section('header')
@parent
<!-- HEADER !-->
<form method="post" action="update" id="category">
<div class="panel panel-default">
  <div class="panel-body">
    <div class="row"  style="padding:0 15px 0 15px;">
      <div class="media">
  		  <a class="pull-left col-md-1">
  		    <img class="media-object" src='{{$user->gravatar(100)}}'>
  		  </a>
  		  <div class="media-body col-md-7" >
  		    <h3 class="media-heading">
            <div class="col-md-6"><input type="text" value="{{ null!==Input::old('title') ? Input::old('title') : $category->title }}" class="form-control" name="title"></input></div> - <small><a href="/usuarios/{{$user->username}}">{{ '@'.$user->username }}</a></small>
          </h3>
          <br>
  		    <textarea name="short_desc" class='form-control input-lg' 
            form="category" placeholder="Añade una descripción...">{{ null!==Input::old('short_desc') ? Input::old('short_desc') : $category->short_desc }}</textarea>
  		  </div>
  		  <div class="col-md-3 pull-right">
  		  	<ul class="list-group">
  				  <li class="list-group-item">Recursos <span class="badge">{{ $category->resources->count() }}</span> </li>
  				</ul>
          <div class="pull-right">
            <button class="btn btn-primary" type="submit">Guardar</button>
             <a href="eliminar"><button type="button" class="btn btn-danger" onClick="if(confirm('¿Estas seguro de borrar la categoria? Todos los recursos pertenecientes a esta categoria desapareceran.'))
            ;
            else return false;">Eliminar</button></a>
            <a href="../{{$category->slug}}"><button class="btn btn-default" type="button">Cancelar</button></a>      
          </div>
  		  </div>
  		</div>
    </div>
  </div>
</div>
</form>
<!-- FIN HEADER !-->
@endsection

@section('content')
<!-- BODY !-->
<div class="panel panel-default"> 
	<!-- RECURSOS Y PUBLI !-->
	<div class="row" style="margin:0px;padding-top:20px;">
    <!-- PUBLI HERE !-->
    <div class="col-md-2">
      @include('publi/160x600left')
    </div>
    <!-- FIN PUBLI !-->
    <!-- RECURSOS HERE !-->
    <div class="col-md-8">
      <div class="row"  style="margin:0px;padding:0px;">
        @forelse($resources as $res)
    		<div class="col-md-4">
    				<div class="thumbnail">
    					<center><div style="background-color:grey;height:250px;width:90%;"><h3>{{$res->mini}}</h3></div></center>
                <div class="row" style="margin:0px;">
            			<div class="caption">
                    <hr style="margin:0px;">
      				  		<h4><a href='../recursos/{{$res->slug}}'>{{ $res->title }}</a> <span class="badge pull-right"><a data-container="body" style="text-decoration:none;color:white;" data-toggle="popover" data-placement="top" 
            data-content="Cuantos usuarios han hecho Fav en este recurso.">{{$res->favs}}</a></span></h4>
                    <p>{{ $res->short_desc }}</p>
                    @if(Auth::check())
                    <div class="row" style="margin:0px;">
                      @if(!ViewHelpers::loged_in($user->username))
                      <div class="col-md-4">
                        @if($res->faved_by(Auth::user()->id))
                        <form action="../recursos/{{$res->id}}/defav" method="post">
                            <button class="btn btn-success" type="submit" name="defav"><img src='/images/fav.png'> Fav</button>
                        </form>
                        @else
                        <form action="../recursos/{{$res->id}}/fav" method="post">
                            <button class="btn btn-primary" type="submit" name="fav"><img src='/images/fav.png'> Fav</button>
                        </form>
                        @endif
                      </div>
                      @endif
                      <div class="col-md-7"><button class="btn btn-default" role="button">Añadir a..</button></div>
                      <a href="/usuarios/{{$user->username}}/recursos/{{$res->id}}/eliminar"><button class="btn btn-danger btn-sm" onClick="if(confirm('¿Estas seguro de borrar el recurso? El recurso no se podra recuperar.'))
                      ;
                      else return false;">Eliminar</button></a>
                    </div>
                    @endif
                    <hr style="margin:10px;">
                    <div class="pull-right"><small>{{ViewHelpers::time_ago($res->updated_at)}}</small></div>
      				  	</div>
                </div>
    				</div>
    		</div>
        @empty
        Todavía no has añadido recursos
        @endforelse
      </div>
    </div>
    <!-- FIN RECURSOS HERE !-->
    <!-- PUBLI HERE !-->
    <div class="col-md-2">
      @include('publi/160x600right')
    </div>
    <!-- FIN PUBLI !-->
	</div>
  <!-- FIN RECURSOS Y PUBLI !-->
  <center>{{$resources->links()}}</center>
</div>
<!-- FIN BODY !-->
@endsection
