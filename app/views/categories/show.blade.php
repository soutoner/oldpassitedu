@section('title', $user->username.'@'.$category->title)

@section('header')
@parent
<div class="container-fluid bodyheader">
  <!-- avatar !-->
  <div class="col-md-1" style="margin:0px;padding:0px;">
    <img src="{{$user->gravatar(100)}}"/>
  </div>
  <!-- FIN avatar !-->
  <!-- info !-->
  <div class="col-md-8" style="margin:0px;">
    <h4>{{$category->title}} - <small><a href="/usuarios/{{$user->username}}">@{{{$user->username}}}</a></small></h4>
    <div class="well well-sm">{{$category->short_desc}}</div>
    <a><small>Reportar usuario</small></a>
  </div>
  <!-- FIN info !-->
  <!-- estadisticas !-->
  <div class="col-md-3">
    <ul class="list-group">
      <li class="list-group-item">Recursos: <span class="badge">{{$category->resources->count()}}</span></li>
      <li class="list-group-item">
        <div class="row">
          <div class="fb-share-button col-md-6" data-href="{{Request::url()}}" data-layout="button_count"></div>
          <div><a href="https://twitter.com/share" class="twitter-share-button col-md-6" data-via="PassItEDU" data-lang="es">Twittear</a></div>
        </div>
      </li>
    </ul>
    @if(ViewHelpers::loged_in($user->username))
    <div class="pull-right">
      <a href="{{$category->slug}}/editar"><button class="btn btn-info">Editar</button></a>
    </div>
    @else
    <small class="pull-right">Ultima actualizacion: @if($category->resources->last()) {{ViewHelpers::time_ago($category->resources->last()->created_at)}} @else Nunca @endif</small>
    @endif
  </div>
  <!-- FIN estadisticas !-->
</div>
@endsection

@section('content')
<!-- BODY !-->
<div class="row col-centered bodypanel"> 
  @include('publi/160x600left')
  <!-- RECURSOS HERE !-->
  <div class="col-md-8" >
    @include('partials/_showrecurso')

    <center>{{$resources->links()}}</center>
  </div>
  <!-- FIN RECURSOS HERE !-->
  @include('publi/160x600right')
</div>
<!-- FIN BODY !-->
@endsection
