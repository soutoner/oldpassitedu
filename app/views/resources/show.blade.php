@section('title', $resource->title)

@section('header')
@parent
<div class="container-fluid bodyheader">
  <!-- avatar !-->
  <div class="col-md-1" style="margin:0px;padding:0px;">
    <img src="{{$user->gravatar(100)}}"/>
  </div>
  <!-- FIN avatar !-->
  <!-- info !-->
  <div class="col-md-8" style="margin:0px;">
    <h4>{{$resource->title}} / <a href="{{ViewHelpers::category_link($resource->category)}}">{{$resource->category->title}}</a> - <small><a href="/usuarios/{{$user->username}}">@{{{$user->username}}}</a></small></h4>
    <div class="well well-sm">{{$resource->short_desc}}</div>
    <a><small>Reportar recurso</small></a>
  </div>
  <!-- FIN info !-->
  <!-- estadisticas !-->
  <div class="col-md-3">
    <ul class="list-group">
      <li class="list-group-item">Añadido a favoritos: <span class="badge">{{$resource->favs}}</span></li>
      <li class="list-group-item">
        <div class="row">
          <div class="fb-share-button col-md-6" data-href="{{Request::url()}}" data-layout="button_count"></div>
          <div><a href="https://twitter.com/share" class="twitter-share-button col-md-6" data-via="PassItEDU" data-lang="es">Twittear</a></div>
        </div>
      </li>
    </ul>
    @if(ViewHelpers::loged_in($user->username))
    <div class="pull-right">
      <a href="{{$resource->slug}}/editar"><button class="btn btn-info">Editar</button></a>
    </div>
    @else
    <div class="pull-right row">
      @if(Auth::check())
      <div class="col-md-8"><small>Ultima actualizacion: {{ViewHelpers::time_ago($resource->updated_at)}}</small></div>
      <div class="col-md-4">
        @if($resource->faved_by(Auth::user()->id))
          <form action="{{$resource->id}}/defav" method="post">
              <button class="btn btn-success" type="submit" name="defav"><img src='/images/fav.png'> Fav</button>
          </form>
        @else
          <form action="{{$resource->id}}/fav" method="post">
              <button class="btn btn-primary" type="submit" name="fav"><img src='/images/fav.png'> Fav</button>
          </form>
        @endif
      </div>
      @else
        <small>Ultima actualizacion: {{ViewHelpers::time_ago($resource->updated_at)}}</small>
      @endif
    </div>
    @endif
  <!-- FIN estadisticas !-->
</div>
@endsection

@section('content')
<!-- Nav tabs -->
<ul class="nav nav-tabs dark" role="tablist" style="margin-top:20px;">
  <li class="active"><a href="#text" role="tab" data-toggle="tab">Texto</a></li>
  @if($resource->pdf_name)<li><a href="#pdf" role="tab" data-toggle="tab">PDF</a></li>@endif
  @if($resource->youtube_url)<li><a href="#youtube" role="tab" data-toggle="tab">YouTube</a></li>@endif
</ul>

<!-- Tab panes -->
<div class="tab-content" style="margin-bottom:20px;">
	<!-- TEXTO !-->
  <div class="tab-pane active" id="text">
  	<div class="panel panel-default" style="padding:20px;overflow:auto;">
      {{ Cache::get('text'.$resource->id, $resource->text) }}
  	</div>
  </div>
  <!-- FIN TEXTO !-->
  <!-- PDF !-->
  <div class="tab-pane" id="pdf">
  	<div class="panel panel-default" style="padding:20px;">
        <center>
          <iframe src="/files/usuarios/{{$user->username.'/'.$category->id.'/'.$resource->id.'/'.$resource->pdf_name}}" 
          style="width:800px; height:700px;" frameborder="0"></iframe>
        </center>
  	</div>
  </div>
  <!-- FIN PDF !-->
  <!-- YOUTUBE !-->
  <div class="tab-pane" id="youtube">
  	<div class="panel panel-default" style="padding:20px;">
        <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="//www.youtube.com/embed/{{$resource->youtubeID}}"></iframe>
        </div>
  	</div>
  </div>
  <!-- FIN YOUTUBE !-->
  <!-- COMENTARIOS !-->
  <center>
  <div style="margin:20px;" class="fb-comments panel" data-href="{{Request::url()}}" data-numposts="5" data-colorscheme="light"></div>
  </center>
  <!-- FIN COMENTARIOS !-->
</div>
@endsection
