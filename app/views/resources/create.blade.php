@if(Request::segment(3) == 'new')
  @section('title', 'Nuevo recurso')
@else
  @section('title', 'Editar '.$resource->title)
@endif

@section('head')
<script>
$(window).bind('beforeunload', function(){
  return '¿Seguro que quieres salir de esta pagina?';
});
</script>
@endsection

@section('content')
<form method="post" action="@if(Request::segment(3)=='new') crear @else update @endif" id="resource" enctype="multipart/form-data">
<div class="row" style="padding:20px 20px 0 20px;">
  <!-- ACCORDION !-->
	<div class="panel-group" id="accordion">
    <!-- PANEL TITULO DESCRIPCION !-->
    <div class="panel panel-default">
  	  <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Informacion basica
          </a>
        </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse in">
    	  <div class="panel-body">
          <div class="form-group col-md-5">
            <input class="form-control" type="text" placeholder="Nombre del recurso" 
            name="title" value="{{ null!==Input::old('title') ? Input::old('title') : (Request::segment(3)=='new' ? Input::get('title') : $resource->title) }}">
          </div>
          <div class="form-group col-md-3">
            <select class="form-control" name="category_id" form="resource" style="width:80%;float:left;margin-right:10px;">
              @forelse($user->categories as $category)
              <option value='{{$category->id}}' 
                @if(Request::segment(3)=='new')
                  @if(Input::get('category')==$category->id || Input::old('category')==$category->id) selected="selected" @endif>{{$category->title}}</option>
                @else
                  @if($resource->category->id==$category->id) selected="selected" @endif>{{$category->title}}</option>
                @endif
              @empty
              <option>Añade una categoría</option>
              @endforelse
            </select>
            <button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="bottom" data-html="true" data-content="@include('partials/_addcategoryform')">+</button>
          </div>
          <div class="form-group col-md-2">
            <select class="form-control" name="curso" form="resource">
              @if(Request::segment(3)=='new')
              <option value="{{Resource::Secundaria}}" @if(Input::get('curso')==Resource::Secundaria) selected @endif>Secundaria</option>
              <option value="{{Resource::Bachillerato}}" @if(Input::get('curso')==Resource::Bachillerato) selected @endif>Bachillerato</option>
              <option value="{{Resource::Universidad}}" @if(Input::get('curso')=='Universidad') selected @endif>Universidad</option>
              <option value="{{Resource::Otro}}" @if(Input::get('curso')=='Otro') selected @endif>Otro</option>
              @else
              <option value="{{Resource::Secundaria}}" @if($resource->curso==Resource::Secundaria) selected @endif>Secundaria</option>
              <option value="{{Resource::Bachillerato}}" @if($resource->curso==Resource::Bachillerato) selected @endif>Bachillerato</option>
              <option value="{{Resource::Universidad}}" @if($resource->curso==Resource::Universidad) selected @endif>Universidad</option>
              <option value="{{Resource::Otro}}" @if($resource->curso==Resource::Otro) selected @endif>Otro</option>
              @endif
            </select>
          </div>
          <div class="form-group col-md-2">
            <select class="form-control" name="visibility" form="resource">
              @if(Request::segment(3)=='new')
              <option value="{{Resource::Publico}}">Publico</option>
              <option value="{{Resource::Registrado}}">Registrado</option>
              <option value="{{Resource::Privado}}">Privado</option>
              @else
              <option value="{{Resource::Publico}}" @if($resource->visibility==Resource::Publico) selected @endif>Publico</option>
              <option value="{{Resource::Registrado}}" @if($resource->visibility==Resource::Registrado) selected @endif>Registrado</option>
              <option value="{{Resource::Privado}}" @if($resource->visibility==Resource::Privado) selected @endif>Privado</option>
              @endif
            </select>
          </div>
          <div class="form-group col-md-10">
            <textarea name="short_desc" class='form-control input-lg' 
            form="resource" placeholder="Añade una descripción...">{{ null!==Input::old('short_desc') ? Input::old('short_desc') : (Request::segment(3)=='new' ? Input::get('short_desc') : $resource->short_desc) }}</textarea>
            <br>
            <input type="text" class="form-control" name="keywords" placeholder="Inserta las palabras claves para favorecer la busqueda de este recurso: Ej: matematicas,ecuaciones,calculo,integrales" 
            value="{{ null!==Input::old('keywords') ? Input::old('keywords') : (Request::segment(3)=='new' ? Input::old('keywords') : $resource->keywords) }}">
          </div>
          <div class="form-group col-md-2">
            <button type="submit" class="btn btn-primary btn-lg" formmethod="post" onClick="if(confirm('¿Estas seguro de que has terminado de editar el recurso?'))
            ;
            else return false;">Enviar</button>
            @if(Request::segment(3) != 'new')
            <a href="eliminar"><button type="button" class="btn btn-danger btn-sm" onClick="if(confirm('¿Estas seguro de borrar el recurso? El recurso no se podra recuperar.'))
            ;
            else return false;">Eliminar</button></a>
            <a href="../{{$resource->slug}}"><button type="button" class="btn btn-default btn-sm">Cancelar</button></a></center>
            @endif
          </div>
    	  </div>
      </div>
  	</div>
  
  <!-- FIN PANEL TITULO DESCRIPCION !-->

	<!-- PANEL CONTENIDO !-->
	<div class="panel panel-default">
	  <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        Contenido
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
  	  <div class="panel-body">
  			<!-- EDITOR DE TEXTO !-->
        <h4>Texto - <small><a href="#">¿Cómo uso el editor?</a></small></h4>
        <div style="padding-right:5px;">
        <textarea name="editor" id="editor1" form="resource">
          {{ null!==Input::old('editor') ? Input::old('editor') : (Request::segment(3)=='new' ? 'Escribe aquí tu apunte, o una pequeña descripción.' : $resource->text) }}
        </textarea>
        </div>
        <script>
          // Replace the <textarea id="editor1"> with a CKEditor
          // instance, using default configuration.
          CKEDITOR.replace( 'editor1', {
            customConfig: '/ckeditor/custom/config.js'
          });
        </script>
        <!-- FIN EDITOR DE TEXTO !-->
  	  	<hr>
  	  	<div class="form-group">
  	  		<h4>URL de un vídeo de YouTube </h4>
          <input class="form-control" type="text" placeholder="Ejemplo: http://www.youtube.com/watch?v=OwVWlsbjMd0" 
          name="youtube_url" value="{{null!==Input::old('youtube_url') ? Input::old('youtube_url') : (Request::segment(3)=='new' ? Input::old('youtube_url')  : $resource->youtube_url)}}">
        </div>
        <hr>
        <div class="form-group">
  	  		<h4>@if (Request::segment(3)!='new')
                @if($resource->pdf_name) Cambiar el PDF @else Sube un PDF @endif
              @else
                Sube un PDF
              @endif
            <small>
              <a data-container="body" data-toggle="popover" data-placement="right" 
              data-content="El PDF debe estar en el formato correcto y tener como max. 10 MB">(?)</a>
            </small>
          </h4> 
          @if (Request::segment(3)!='new')
            @if($resource->pdf_name) 
              <p>Ya tienes un archivo PDF alojado. ¿Quieres eliminarlo? <a href="deletePDF"><button type="button" class="btn btn-danger btn-sm">Si</button></a></p>
            @else
              {{ Form::file('pdf_file','',array('id'=>'','class'=>'')) }}
            @endif 
          @else
            {{ Form::file('pdf_file','',array('id'=>'','class'=>'')) }}
          @endif
        </div>
  	  </div>
    </div>
	</div>
  <!-- FIN PANEL CONTENIDO !-->
  </div>
  <!-- ACCORDION !-->
</div>
</form>
@endsection
