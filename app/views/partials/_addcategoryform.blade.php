<form method='post' action='/postCategory'>
  <div class='form-group'>
    <input class='form-control' type='text' placeholder='Nueva categoria' 
    name='title'>
  </div>
  <div class='form-group'>
    <input class='form-control' type='text' placeholder='Descripcion' 
    name='short_desc'>
  </div>
  <div class='form-group'>
    <input class='form-control' type='submit' class='btn btn-hidden' 
    value='Agregar'>
  </div>
</form>