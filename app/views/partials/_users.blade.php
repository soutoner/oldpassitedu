<div class="panel panel-heading row" style="margin:0 0 10px 0;">
  <div class="col-md-2"><?php if(isset($cont))echo $cont;?><img style="margin-left:10px;"src="{{$usr->gravatar(80)}}" align="middle"/></div>
  <div class="col-md-10">
	  	<div class="row">
	  		<div class="col-md-9">
				<h4>{{$usr->name.' '.$usr->surname}} - <small><a href="/usuarios/{{$usr->username}}">{{'@'.$usr->username}}</a></small></h4>
		    <p>{{$usr->short_desc}}</p>
		    <?php $res = Resource::where('user_id','=',$usr->id)->orderBy('updated_at','desc')->take(1)->get();
		 			foreach($res as $rs){
		 				echo"<tr><td style='padding: 0 5px 0 0;'>Ultimo recurso ".ViewHelpers::time_ago($rs->updated_at)." :</td> <td><a href='/usuarios/$usr->username/recursos/$rs->slug'>$rs->title</a></td></tr>";
		 			}
		 	?>
		 		</div>
		 		<div class="col-md-3 pull-right">
		 		<table>
		 		@if(isset($usr->res))<tr><td style="padding: 0 5px 0 0;">Score:</td> <td>{{round($usr->res,2)}}</td></tr>@endif
		 		</table>
		 		<br><br>

		 		@if(Auth::check())
		 			@if(!ViewHelpers::loged_in($usr->username))
			 			@if($usr->followed_by(Auth::user()->id))
	      					<input class="follow btn btn-success" type="submit" name="{{$usr->username}}" value="&#10008; Seguido" />
	   					@else
	          				<input class="follow btn btn-primary" type="submit" name="{{$usr->username}}" value="&#10004; Seguir" />
	    				@endif
    				@endif
    			@endif
    		</div>
    	</div>
  </div>
</div>