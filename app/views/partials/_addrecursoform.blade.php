<!-- AÑADIR RECURSO FORM !-->
@if(ViewHelpers::loged_in($user->username))
<div class="panel-group" id="accordion" style="margin:0px;">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        <img src="/images/plus.png"> <u>Añadir recurso</u>
        </a>
      </h4>
    </div>
  <div id="collapseOne" class="panel-collapse collapse">
    <div class="panel-body">
    @if($user->categories->count()>0)
      <form method="post" action="@if(Request::segment(3) == 'categorias') ../new @else {{$user->username}}/new @endif" id="resource">
          <div class="form-group col-md-4" >
            <input class="form-control" type="text" placeholder="Nombre del recurso" 
            name="title" value="{{Input::old('title')}}" >
          </div>
          <div class="form-group col-md-3" >
            <select class="form-control" name="category" form="resource">
              @if(Request::segment(3) == 'categorias')
                @forelse($user->categories as $cate)
                  <option value="{{$cate->id}}" @if($id==$cate->id || Input::old('category')==$category->id) selected="selected" @endif>{{$cate->title}}</option>
                @empty
                  <option>Añade una categoría</option>
                @endforelse
              @else
                @forelse($user->categories as $category)
                  <option value="{{$category->id}}">{{$category->title}}</option>

                  @empty
                  <option>Añade una categoría</option>
                @endforelse
              @endif
            </select>
          </div>
          <div class="form-group col-md-1">
            <button type="button" class="btn btn-default"  data-toggle="popover" data-placement="bottom" data-html="true" data-content="@include('partials/_addcategoryform')">+</button>
          </div>
          <div class="form-group col-md-3">
            <select class="form-control" name="curso" form="resource">
              <option value="Secundaria">Secundaria</option>
              <option value="Bachillerato">Bachillerato</option>
              <option value="Universidad">Universidad</option>
              <option value="Otro">Otro</option>
            </select>
          </div>
          <div class="form-group col-md-10">
            <textarea name="short_desc" class='form-control input-lg' 
            form="resource">Añade una descripción...</textarea>
          </div>
          <div class="form-group col-md-2">
            <input type="submit" class="btn btn-primary" formmethod="post" value="Agregar">
          </div>
        </form>
    @else
    <p>{{$user->name}} debes añadir primero alguna categoría.</p>
      @include('partials/_addcategoryform')
    @endif
    </div>
    </div>
  </div>
</div>
@endif
<!-- FIN AÑADIR RECURSO FORM !-->