<div class="@if(Request::segment(3)==null) panel panel-heading  @endif row">
  @forelse($resources as $res)
	<div class="col-md-4">
			<div class="thumbnail">
				<center>{{ViewHelpers::resource_mini($res)}}</center>
          <div class="row" style="margin:0px;" >
      			<div class="caption" style="height:240px;">
              <hr style="margin:0px;">
				  		<h4><a href='../recursos/{{$res->slug}}'>{{ $res->title }}</a> <span class="badge pull-right"><a data-container="body" style="text-decoration:none;color:white;" data-toggle="popover" data-placement="top" 
      data-content="Cuantos usuarios han hecho Fav en este recurso.">{{$res->favs}}</a></span></h4>
              <p>{{ $res->short_desc }}</p>
              @if(Auth::check())
              <div class="row" style="margin:0px;">
              	@if(Request::segment(3)==null)
            		<div class="row" style="margin:0px;">
                  <form method="post" action="{{$user->username}}/recursos/{{$res->id}}/remove">
                    <div class="col-md-5"><button class="btn btn-primary" role="button">Eliminar</button></div>
                  </from>
                </div>
                @else
	                @if(!ViewHelpers::loged_in($user->username))
		                <div class="col-md-5">
		                  @if($res->faved_by(Auth::user()->id))
		                  <form action="../recursos/{{$res->id}}/defav" method="post">
		                      <button class="btn btn-success" type="submit" name="defav"><img src='/images/fav.png'> Fav</button>
		                  </form>
		                  @else
		                  <form action="../recursos/{{$res->id}}/fav" method="post">
		                      <button class="btn btn-primary" type="submit" name="fav"><img src='/images/fav.png'> Fav</button>
		                  </form>
		                  @endif
		                </div>
		                @endif
		                @if($res->added_by(Auth::user()->id))
		                  <form method="post" action="../recursos/{{$res->id}}/remove">
		                    <div class="col-md-5"><button class="btn btn-primary" role="button">Añadir a..</button></div>
		                  </from>
		                @else
		                  <form method="post" action="../recursos/{{$res->id}}/add">
		                    <div class="col-md-5"><button class="btn btn-default" role="button">Añadir a..</button></div>
		                  </from>
		                @endif	                
					@endif
              </div>
              @endif
              <hr style="margin:10px;">
              @if($res->pdf_name)
                <img src="/images/pdf-icon.png" style="height:32px;width:32px;">
              @endif
              @if($res->youtube_url)
                <img src="/images/youtube.png" style="height:35px;width:35px;">
              @endif
              <div class="pull-right"><small>{{ViewHelpers::time_ago($res->updated_at)}}</small></div>
				  	</div>
          </div>
			</div>
	</div>
  @empty
  El usuario todavía no ha añadido recursos
  @endforelse
</div>