@section('title', 'Condiciones de Uso')

@section('content')
<div class="row">
<div class="panel panel-default"> 
  <div class="panel-body" style="padding:30px;">
  	<div class="page-header" style="margin-top:-10px">
  		<h1>Términos de Uso</h1>
		</div>
    
      Gracias por visitar www.passitedu.es (en adelante la Web). Esta web es propiedad de Adrián González Leiva y Francisco Navarro Aguilar (en adelante PassItEDU) con localización en Málaga. Recordarle a usted (en adelante el Usuario) que nos reservamos el derecho de modificar estas condiciones de uso en cualquier momento, con la respectiva notificación a los Usuarios sobre las novedades.
      <br></br>
      Al ser un proyecto, estaremos constantemente añadiendo funcionalidades y cambios a la web, por lo que estas Condiciones de Uso cambiarán bastante de aquí al lanzamiento de la plataforma, por lo que recomendamos al Usuario que consulte este texto con cierta asiduidad para conocer todas las novedades y cambios que le conciernen.

    <h3>¿Qué es PassItEDU?</h3>

      PassItEDU es un portal de enseñanza con un canal de Youtube con multitud de vídeos sobre matemáticas en distintos niveles de enseñanza (ESO, Bachiller, Universidad, etc.) entre otras asignaturas. principalmente enfocado a Ingenierías a nivel Universitario, pero con mucho contenido básico de Matemáticas. Ofrece también un foro de dudas donde poder preguntar, responder o compartir información entre alumnos.

    <h3>Entonces, ¿cuál es el cambio?</h3>

      Ahora también la Web ofrecerá un portal de enseñanza online, donde a forma de red social, cada usuario podrá crear su perfil para subir sus apuntes, ya sea en formato de texto, PDF, o como video de YouTube y así poder compartirlos con el resto de usuarios de la comunidad. Cada usuario a parte de tener el rol de profesor, también podrán actuar como alumno, leyendo los apuntes de otros usuarios y siguiendo a determinados profesores para poder enterarse de todos los apuntes que suben al portal. Todos los apuntes, sea cual sea el curso, o los estudios que estas cursando tendrán lugar en la Web.

    <h3>Registro en la plataforma</h3>

      Al estar todavía en una etapa muy pronto del desarrollo, el único formulario que el Usuario tiene a su disposición es el de registro en la plataforma. Este formulario simplementa pedirá al Usuario nombre y apellidos, el email (para poder notificar de todas las novedades) y datos estadísticos como la fecha de nacimiento y los estudios que esta cursando para conocer mejor al Usuario y poder ofrecer una experiencia más personalizada.
      <br></br>
      Es importante que el Usuario lea las Políticas de Privacidad además de este texto, porque para darse de alta en nuestra plataforma, tendrá que estar totalmente conforme con estos textos.
      <br></br>
      Otro detalle importante para el Usuario y que debe conocer, es que la edad mínima de acceso a esta plataforma (aunque todavía este en beta) es de 16 años. En el caso de encontrarte por debajo de esa edad, lo sentimos pero todavía tendrás que esperar un par de años para poder acceder. En el caso de que seas mayor de 16 años, pero seas todavía menor de edad, tendrás que informar y consultar con tus padres o tutores legales el acceso a la plataforma, y que estos te den pleno consentimiento para registrarte, en otro caso, PassItEDU estará en pleno derecho de borrar tu usuario de nuestra base de datos.

    <h3>Recursos y propiedad intelectual</h3>

      Al ofrecer cualquier tipo de recurso (ya sea en formato de texto o PDF) en nuestra web damos por hecho que el contenido divulgado es exclusivamente de su propiedad y que ha sido elaborado integramente por el usuario.
      <br></br>
      No aceptamos en ningun caso la subida de recursos que no pertenezcan al usuario y/o que hayan sido subidos sin el permiso del autor original, eximiendo de cualquier responsabilidad a PassitEDU. Si esto ocurre con algun usuario la responsabildiad sera exclusivamente suya, y nos reservamos el derecho de eliminar el recurso y/o suspender la cuenta del usuario.

    <h3>Uso de cookies</h3>

      En el caso de esta web usaremos siempre dos o tres tipos de cookies que mantendran informacion sobre su sesion en nuestra plataforma, con la unica de intencion de reconocer si usted incia sesion o no. Si quieres más información acerca de ellas pásate por la Política de Privacidad o ve al apartado de contacto.

    <h3>Contacto</h3>

      Hasta que la plataforma no esté mas avanzada, no podremos actualizar ni definir más estas Condiciones de Uso, de mientras, si tenéis alguna duda, queja o sugerencia por favor, escríbenos a passitedu[at]gmail.com 
	</div>
</div>
</div>
@endsection