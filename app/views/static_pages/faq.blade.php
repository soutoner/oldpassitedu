@section('title', 'Condiciones de Uso')

@section('content')
<div class="row">
<div class="panel panel-default"> 
  <div class="panel-body" style="padding:30px;">
  	<div class="page-header" style="margin-top:-10px">
  		<h1>FAQ - Preguntas Frecuentes</h1>
	</div>
		 <h3>¿Cómo subo mis apuntes?</h3>
		 	<p>Una vez creada una <a href="https://www.passitedu.com/usuarios/soutoner/recursos/como-crear-tu-primera-categoria">categoría</a>, podrás añadir todos los <a>recursos</a> que quieras siempre que respetes el límite de almacenamiento.</p>
		 	<p><b>¿Por qué limite de almacenamiento?</b> Como bien podéis observar, PassItEDU es un servicio gratuito, pero para que siga así, de momento solo podemos garantizar un espacio limitado para cada usuario. Hasta que en un futuro no ampliemos servidores, no podremos ofrecer más espacio.</p>
		 <h3>¿Cómo descargo apuntes?</h3>
		 	<p>De momento solo se pueden descargar los PDF si eres un usuario registrado. Los apuntes de texto y los enlaces a YouTube están disponibles online, pero debido a limitaciones del servidor no podemos ofrecerlos en otros formatos.</p>
		 <h3>¿Para qué sirve los favs?</h3>
		 	<p>Los favs es un sistema de votos que hemos implementado para que si un recurso te ha gustado, puedas puntuarlo positivamente, así todos los demás usuarios sabrán la calidad de los apuntes. Te animamos a que votes todos los recursos que puedas y que sean de tu agrado, así nos ayudaremos a tener más información sobre los recursos. 
		 	Además, de estos favs, dependerá tu posición en el <a href="/rank" target="_new">ranking de mejores usuarios de PassitEDU</a>, por lo que es importante hacer que los usuarios den fav a tu recurso.</p>
		 <h3>¿Qué es eso de +1 y -1?</h3>	
		 	<p>Es otro sistema de votos tal y como el de los recursos, pero en este caso es más completo puesto que no solo podremos votar positivamente a un usuario sino que también podremos votarlo negativamente si el usuario no respeta las normas o hace un mal uso de la plataforma.</p>    
     <h3>Escritorio Personal ¿para qué sirve?</h3>
      <p>El escritorio personal es un espacio donde los usuarios podrán guardar los recursos que han encontrado interesantes mientras navegaban por la web, para poder acceder fácilmente a ellos. Todavía es una función que esta muy verde, pero que ya esta operativa para añadir recursos por separados.</p>
      <p><a>¿Cómo añadir un recurso al escritorio personal?</a></p>
      <br></br>
	</div>
</div>
</div>
@endsection
