@section('title', 'Sobre Nosotros')

@section('content')
<div class="row">
<div class="panel panel-default"> 
  <div class="panel-body" style="padding:30px;">
  	<div class="page-header" style="margin-top:-10px">
  		<h1>Sobre Nosotros</h1>
		</div>

		<center>
		<div class="embed-responsive embed-responsive-16by9" style="margin:20px;">
			<iframe class="embed-responsive-item" src="http://www.youtube.com/embed/OwVWlsbjMd0?feature=player_detailpage" frameborder="0"  allowfullscreen></iframe>
		</div>
		<p> Buenas, somos <b>Adrián</b> y <b>Fran</b> y nosotros formamos <b>Pass-It</b>. Somos <b>dos estudiantes</b> de Ingeniería Informática en la Universidad de Málaga y por supuesto no somos matemáticos ni mucho menos. Nuestra <b>idea</b> es crear una comunidad sencilla en la que todos nos podamos <b>ayudar</b> a afrontar las dificultades que nos encontramos en nuestros estudios día a día. No solo intentamos ayudar con nuestros vídeos (que distan mucho de ser algo profesionales) sino que <b>intentamos</b> por todos los medios <b>crear una comunidad</b> donde todos nos ayudemos a todos. Porque nadie es experto en lo que sabe, empezando por <b>nosotros</b>, pero estamos seguro de que todos siempre podemos aportar un granito de arena. Espero que sea una buena experiencia, que os ayude, y que consigamos llegar lejos <b>todos juntos</b>. 
		<br></br>
		Un cordial saludo de PassItEDU, de parte de Fran y Adrián.
		</center>
    
	</div>
</div>
</div>
@endsection
