@section('title', 'Bienvenidos')

@section('content')
<!-- Bienvenidos !-->
<div class="row dark" style="padding:50px 100px 50px 100px;text-align:center;">
  <div class="dark col-md-5 col-centered"> <!-- left !-->
  	<h3>Bienvenidos a PassItEDU</h3>
    <br>
    <p style="font-size:15px;"><b>¿Qué es PassItEDU?</b> Es una nueva plataforma en la que podrás compartir
      tus apuntes para que otros estudiantes como tu puedan aprender.</p>
    <img src="/images/gomalapiz.png" height="90" width="90" alt="Goma y lápiz"><br><br>
  </div>
</div>
<!-- FIN Bienvenidos !-->

<!-- Features !-->
<div class="row" style="background-color:white;padding:50px 100px 50px 100px;text-align:center;">
  <h3>¿Qué puedes hacer?</h3>
  <br>
  <div class="col-md-3">
    <img src="/images/ckeditor.png" height="115" width="240" style="margin-top:10px;" alt="Editor de texto de PassitEDU"/><br><br>
    <p>Podrás subir tus apuntes en formato de <b>texto</b> con nuestro intuitivo editor, como archivo <b>PDF</b>, o como <b>video de YouTube.</b></p>
  </div>
  <div class="col-md-3">
    <img src="/images/seguir.png" height="135" width="240" alt="Botón de seguir de PassitEDU"/><br><br>
    <p>Podrás <b>seguir</b> y <b>ser seguido</b> por otros usuarios para enterarte de todas las novedades. También podrás añadir sus recursos
      a <b>tu escritorio personal</b> para tenerlos siempre a mano.</p>
  </div>
  <div class="col-md-3">
    <img src="/images/comentarios.png" height="70" width="240" style="margin-top:20px; alt=" alt="Comentarios de PassitEDU"/><br><br>
    <p>Podrás <b>escribir comentarios</b> en los apuntes de otros usuarios a modo de <b>foro</b> para preguntar todas las dudas que te surjan mientras estas estudiando. 
      <br><small>(Próximamente)</small></p>
  </div>
  <div class="col-md-3">
    <img src="/images/notificaciones.png" height="45" width="240" style="margin-top:30px;" alt="Header de PassitEDU"/><br><br>
    <p>Tendrás a tu alcance un <b>sistema de notificaciones</b> para poder estar siempre a la última con otros compañeros que estudien lo mismo que tú.</p>
  </div>
</div>
<!-- FIN Features !-->

<!-- Regístrate !-->
<div class="dark row" style="padding:50px 100px 50px 100px;">
  <div class="col-md-6 dark" style="margin:80px 0 0 0;">
    <center><h2>¿Todavía no estás registrado?</h2></center>
  </div>

  <div class="col-md-6 dark"> <!-- right !-->
    <h3>Únete</h3>      
    <form method="post" action="/registrate">
      <div class="form-group">
        <input type="text" name="username" placeholder='Elige un nombre de usuario' 
         class='form-control' style='width:70%;' value="{{ Input::old('username') }}">
      </div>
      <div class="form-group">
        <input type="email" name="email" placeholder='pepito@ejemplo.com'
         class='form-control' style='width:70%;' value="{{ Input::old('email') }}">
        <p class="help-block" style="color:#dfdfdf;">Recuerda: tienes que usar un email válido.</p>
      </div>
      <div class="form-group">
        <input type="password" name="password" placeholder='Elige tu contraseña'
         class='form-control' style='width:70%;' value="{{ Input::old('password') }}">
        <p class="help-block" style="color:#dfdfdf;">Mínimo 6 caracteres, al menos una mayúscula y un número.</p>
      </div>
     <button class="btn btn-large btn-primary" >Enviar</button>
     <div class="pull-right"><a href="/login/fb"><img style="height:35px;width:35px;" src="/images/facebook.gif" alt="Icono de Facebook"></a> Registrarte con Facebook</div>
    </form>
  </div>
</div>
<!-- FIN Registrate !-->

<!-- Si necesitas ayuda !-->
<div class="row" style="background-color:white;padding:50px 100px 50px 100px;">
  <center>
    <h3>Y recuerda <small>si necesitas ayuda...</small></h3>
    <br>
    <p style="font-size:15px;">Solo tendras que visitar el perfil de Edu (<a href="usuarios/yosoyedu">@yosoyedu</a>) para poder ver todos los tutoriales sobre como funciona la web.</p>
    <img height="264" width="160" src="/images/mascotacolor.png" style="margin:20px 0 0 0;"/>
  </center>
</div>
<!-- FIN Si necesitas ayuda !-->

<!-- Ultimamente en PassitEDU !-->
<div class="row dark" style="padding:50px 100px 50px 100px;">
    <h3>Ultimamente en PassItEDU</h3>
      <br>

    <div class="col-md-6"> <!-- lastest users !-->
      <p>Ultimos usuarios</p>
      <ol>
        @foreach($lastestUsers as $usr)
          <li><img src="{{$usr->gravatar(50)}}"/> {{$usr->completeName}} - <small><a href="usuarios/{{$usr->username}}">@{{{$usr->username}}}</a></small> <small class="pull-right">{{ViewHelpers::time_ago($usr->created_at)}}</small></li>
          <hr style="margin:10px;">
        @endforeach
      </ol>
    </div>
    <br>
    <div class="col-md-6"> <!-- lastest categories !-->
      <p>Ultimos recursos</p>
      <ol>
        @foreach($lastestResources as $rsc)
          <li> <a href="{{ViewHelpers::resource_link($rsc)}}">{{$rsc->title}}</a> @ <a href="{{ViewHelpers::category_link($rsc->category)}}">{{{$rsc->category->title}}}</a> - <small>by <a href="usuarios/{{$rsc->user->username}}">{{$rsc->user->username}}</a></small> <small class="pull-right">{{ViewHelpers::time_ago($rsc->created_at)}}</small></li>
          <hr>
        @endforeach
      </ol>
    </div>
</div>
<!-- FIN Ultimamente en PassitEDU !-->
@endsection


@section('footer')
    @parent
    <div class="pull-right" style="margin-right:20px;"><a href="https://www.positivessl.com" style="font-family: arial; font-size: 10px; color: #212121; text-decoration: none;"><img src="https://www.positivessl.com/images-new/PositiveSSL_tl_trans2.png" alt="SSL Certificate" title="SSL Certificate" border="0" /></a></div>
    <p class="pull-right dark" style="margin:15px 10px 0 0;">Esta web tiene certificado SSL, <br>asegurate siempre de entrar <br>mediante la direccion https://<br> asi tu informacion se mantendra segura.</p>
@stop 


