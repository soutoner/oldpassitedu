@section('title', 'Políticas de Privacidad')

@section('content')
<div class="row">
<div class="panel panel-default"> 
  <div class="panel-body" style="padding:30px;">
  	<div class="page-header" style="margin-top:-10px">
  		<h1>Políticas de Privacidad</h1>
		</div>
    
  		Gracias por visitar www.passitedu.es (en adelante, la Web), la cual es gestionada por Adrián González Leiva y Francisco Navarro Aguilar (en delante, PassItEDU). Esta política de privacidad recoge, en el caso de que usted (en adelante el Usuario) nos ofrezca información o sea recogida por la Web, cómo y de qué manera se trataran sus datos y para qué será utilizada durante su experiencia en la Web.
       <br></br>
      Desde PassItEDU le garantizamos el cumplimiento de la Ley Orgánica 15/1999, de 13 de Diciembre, de Protección de Datos de Carácter Personal (en adelante LOPD) con un fichero inscrito a titulo de Adrian Gonzalez Leiva (77237685M).
      Ya sea usuario registrado o no, al navegar por la Web esta usted aceptando nuestra Política de Privacidad, y las Condiciones de Uso que estas conllevan, por lo que es de vital importancia que las lea y comprenda. Si no quiere que algo de lo aquí mencionado tenga efecto, por favor no use la Web. 
      
    <h3>¿Qué datos recogemos?</h3>

      A través de la Web, los únicos datos personales que obtengamos serán aquellos que explícitamente (y en forma de formulario) pidamos al Usuario. Estos datos por supuesto se verán perfectamente reflejado en el fichero que tenemos inscrito en el Registro General de Protección de Datos.
      <br></br>
      Con la intención de ofrecer una grata experiencia al Usuario durante su paso por la Web, pedimos que los datos que nos ofrezcan sean correctos, veraces y exactos, y que en cualquiera de los casos, sean actuales.
      <br></br>
      PassItEDU le recuerda que para inscribirte en nuestra web, tienes que tener como mínimo 16 años, y que siendo en ese caso menor de edad, tendrás que informar y pedir consentimiento a tus padres o tutores legales sobre todo lo referente al uso de la Web y por supuesto al registro en ella. Nos reservamos el derecho en cualquier momento de pedir el D.N.I. u cualquier otro documento acreditativo de la edad del Usuario e incluso fotocopiado para verificar la edad de éste.
      
    <h3>¿Cómo son almacenados esos datos?</h3>

      Los datos que el usuario nos facilite serán guardados en una base de datos MySQL propiedad de PassItEDU. Tenga en cuenta que las cookies (también pueden ser datos sensibles) serán almacenadas en su navegador, pero este tema es tratado más adelante.
      <br></br>
      El acceso a la base de datos (y en definitiva a los datos) esta restringido exclusivamente a PassitEDU, y nadie más, ajeno a ellos podrán acceder a estos datos por lo tanto aseguramos su privacidad y seguridad de éstos.

    <h3>¿Qué hacemos con los datos?</h3>

      Los datos que el Usuario nos facilitan nos sirven para ofrecer una experiencia más completa al Usuario en su paso por la Web. Datos como la fecha de nacimiento, sexo, o estudios que ha cursado serán datos púramente estadísticos para poder ofrecer una experiencia más personalizada, en ningún momento se hará un uso ilegítimo de ellos.
      <br></br>
      El Usuario debe ser consciente de que, al formar parte de la Web, nos reservamos el derecho de usar sus datos para fines publicitarios o "sociales" tales como ser mencionados en nuestras respectivas páginas en servicios como Facebook, Youtube o Twitter, pero en ningún caso, con datos sensibles o con carácter ofensivo.
      <br></br>
      También usaremos datos como el email para ofrecerte novedades, cambios importantes o actualizaciones en la Web para asi poder mantenerte informado de las novedades. En cualquier momento esta subscripción puede ser cancelada por el Usuario.

    <h3>¿Qué son las cookies?</h3>

      Las cookies son pequeños archivos de texto que se alojan en su navegador y que son generados por nuestra Web (en el caso de que navege por nuestra Web) para poder recordar datos suyos tales como, que el Usuario ha iniciado sesión en nuestro portal, o qué color prefería el Usuario a la hora de ver el diseño de nuestra Web.
      <br></br>
      En cualquier caso, las cookies tienen caducidad, y cada cierto tiempo pasan a ser inservibles, por lo tanto son automaticamente borradas de nuestro servidor (y seguramente de su navegador). Siempre el usuario tiene la posibilidad de ir a su navegador preferido y borrar manualmente las cookies. Al navegar por nuestro sitio Web, el usuario esta aceptando implícitamente que está de acuerdo con el uso de cookies para mejorar su experiencia en la Web.
      <br></br>
      Si la preocupación del Usuario es que datos sensibles (e.j. la edad, contraseñas) sean convertidos en cookies y alojados en el navegador o en nuestro servidor, cabe destacar que los datos que se convierten en cookies son, la mayoría de las veces, cadenas de carácteres incomprensibles, incluso para PassItEDU como webmasters, y que solo son de información para el servidor de la Web (e.j. conocer que usted ha iniciado sesión).

    <h3>¿Estan seguros mis datos?</h3>

      En PassItEDU siempre tratamos de estar atentos a la seguridad hasta en el último detalle, pero es importante mencionar que ninguna técnica de seguridad es infalible o perfecta, y que cada día los métodos para recabar información de forma ilegitima son más rápido y mejores.
      <br></br>
      Por nuestra parte podemos asegurarte que sus datos sensibles (contraseñas y números claves) serán debidamente encriptados en nuestro servidor de forma que, aunque alguien externo a PassItEDU puediese acceder a la base de datos, sería imposible que pudiese leer los datos.
      <br></br>
      También mencionar que trataremos de activar todos los mecanismos para que nadie pueda suplantar su identidad por medio de cookies.
      <br></br>
      PassItEDU tiene certificados SSL para poder ofrecer una conexión segura (HTTPS) como los de páginas como Facebook o Twitter, para que sus datos sean casi invisibles a cualquier intruso, ya que iran cifrados durante su navegacion por la red. Aunque como ya mencionamos, esto es casi imposible.

    <h3>Datos de terceros</h3>

      Es importante que el Usuario tenga en cuenta, que si en algún momento, por medio de nuestra Web, accede a un enlace (url) o a una publicidad ajena a PassitEDU, y ofrece sus datos o simplemente navega por ella, queda totalmente al descubierto en cuanto a términos de privacidad se refiere. PassItEDU no garantiza que sus políticas se cumplan en sitios ajenos.
      <br></br>
      También cabe mencionar que nunca un administrador de la Web le pedirá sus datos sensibles (como contraseña) a menos que sea para iniciar sesión en nuestro portal. Por lo tanto tenga cuidado con quien comparte el Usuario sus datos sensibles. 

    <h3>Cambios y actualizaciones</h3>

      PassItEDU se guarda el derecho de actualizar o modificar estas Políticas de Privacidad en cualquier momento, con su respectivo aviso a los Usuarios de la Web cada vez que esto ocurra, y específicando claramente los cambios. 

    <h3>Derechos de usuario</h3>

      El Usuario a parte de tener muchos deberes, tiene como derechos principales: pedir que en cualquier momento que sus datos o perfil sean borrados del sitio.
      Editar, modificar o actualizar sus datos en nuestra Web. Cancelar su subscripción a boletines de información y actualizaciones sobre la Web. Para otra consulta de cualquier tipo vaya al apartado de Contacto.

    <h3>Contacto</h3>

      Si tiene alguna duda, sugerencia o queja por favor haganosla saber a la siguiente dirección de correo: passitedu[at]gmail.com 
	</div>
</div>
</div>
@endsection