@section('title', 'Contacto')

@section('content')
<div class="row">
<div class="panel panel-default"> 
  <div class="panel-body" style="padding:30px;">
  	<div class="page-header" style="margin-top:-10px">
  		<h1 >Contacto</h1>
  	</div>
  		<div class="col-md-4" style="height:370px;margin:15px auto 25px auto;border-right:solid #D8D8D8;">
  			<h2 style="color:red;margin-bottom:30px;">Youtube</h2>
  			<script src="https://apis.google.com/js/platform.js"></script>
				<div class="g-ytsubscribe" data-channel="Passitedu" data-layout="full" data-count="undefined">
				</div>
				<br><br>
				<p>Este es nuestro canal, puedes preguntarnos dudas de algún video directamente en la zona de comentarios, también puedes mandarnos alguna petición a través de un mensaje directo al canal y la contestaremos rápidamente.</p>
  		</div>
  		<div class="col-md-4" height="250" style="height:370px;margin:15px auto 25px auto;border-right: solid #D8D8D8">
  			<h2 style="margin-bottom:10px;color:#33ccff;">Twitter</h2>
	  		<a class="twitter-timeline" width="300" height="250" href="https://twitter.com/PassItEDU" data-widget-id="489422785647607808">Tweets por @PassItEDU</a>
				<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
				</script>
				<p>Puedes contactarnos por Twitter si quieres compartir algo con el resto de seguidores.</p>
  		</div>
  		<div class="col-md-4" style="height:370px;margin:15px auto 25px auto;">
  			<h2 style="color:#3B5998;">Facebook</h2>
  			<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2FPassitedu&amp;width=300&amp;height=290&amp;colorscheme=light&amp;show_faces=true&amp;header=true&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:300px; height:290px;" allowTransparency="true">
  			</iframe>
  		</div>
  		<p>Por último, si necesitas decirnos algo de una manera mas personal, o bien tienes alguna duda, queja, o sugerencia, puedes contactar con nosotros por estas dos vías:<br>
        contacto[at]passitedu.es o dudas[at]passitedu.es<br><br><br></p>
		</div>
</div>
</div>
@endsection
