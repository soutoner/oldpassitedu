<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="/" class="navbar-brand">PassItEDU<small style="font-size:8px;">BETA</small></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">



      <!-- Buscador -->
    <script type="text/javascript">
        $(document).ready(function() {
          var cond = 1;
          /**           *
           *  jQuery.browser.mobile will be true if the browser is a mobile device
           *  Si se detecta que se han conectado desde un movil/tablet/etc
           *  se desactivará la busqueda retroalimentada
           **/
          (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);
          if(jQuery.browser.mobile){
            $('.busquedaPanel').remove();
          }

          /**
          *  Ocultar panel de busqueda
          *  Clicando fuera o pulsando esc
          **/
          $(document).click(function(e) {
            if (!$(e.target).closest('.busqueda').length) {
                $('.busquedaPanel').addClass("hide");
            }
          });
          $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
               $('.busquedaPanel').addClass("hide");
            }
          });

          /**
          *  Busqueda sucesiva por pulsaciones de teclado (keyup/down)
          *  cond == 1 busqueda con ajax solo para usuarios (cond == 1)
          **/
          $('.buscador').on('keypress, keyup',function() {
            var valorInp = $(this).val();
            
            if(cond == 1){
              (valorInp.length > 0)?  buscando(valorInp, 'Usuario', cond):cerrarPanel();
              
            }else{
              (valorInp.length > 0)?  buscando(valorInp, 'Apuntes', cond):cerrarPanel();
            }
          });

          function buscando(valor, busqueda, cond){
            $.ajax({
              url: '/utils/busqueda'+busqueda+'/',
              type: 'post',
              data: {
                'buscador': valor,
                'cond': cond
              },
              success: function(response){
                  $('.busquedaPanel').removeClass('hide');
                  $('.busqueda').html(response);
              }
            });
          }
          function cerrarPanel(){
            $('.busquedaPanel').addClass('hide');
            $('.busqueda').html("");
          }
          /**
          *  toggle busqueda entre usuario/apuntes
          **/
          $('.usuario').click(function() {
            $('.icobusqueda').removeClass('glyphicon-book').addClass('glyphicon-user');
            $('.buscador').attr("placeholder", "Buscar por usuario").focus();
            cond = 1;
            var valor = $('.buscador').val();
            (valor != '') ? buscando(valor, 'Usuario', cond):cerrarPanel();
          });
          $('.apunte').click(function() {
            $('.icobusqueda').removeClass('glyphicon-user').addClass('glyphicon-book');
            $('.buscador').attr("placeholder", "Buscar por apuntes").focus();
            cond = 2;
            var valor = $('.buscador').val();
            (valor != '') ? buscando(valor, 'Apuntes', cond):cerrarPanel();
          });

        });
    </script>

      <form class="navbar-form navbar-left" role="search" method="post" action="/utils/search/" name="search" style="margin:0px;">
        <div class="navbar-form navbar-left">
          <div class="input-group">
            <input type="text" class="buscador form-control" name="buscador" placeholder="Buscar por usuario"  autocomplete="off">
              <span class="input-group-btn">
                <button class="btn btn-default disabled">
                  <span class="icobusqueda glyphicon glyphicon-user" aria-hidden="true"></span>
                </button>
              </span>
          </div>
          <div class="btn-group" data-toggle="buttons">
            <label class="usuario btn btn-default active">
              <input type="radio" name="cond" value="1" autocomplete="off" checked>
              <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
            </label>
            <label class="apunte btn btn-default">
              <input type="radio" name="cond" value="2" autocomplete="off">
              <span class="glyphicon glyphicon-book" aria-hidden="true"></span>
            </label>
          </div>
        </div>
      </form>

      <!-- Buscador -->

      @if(Auth::check())
      <ul class="nav navbar-nav navbar-right" style="z-index:10;">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <img src="/images/plus.png" style="margin:5px;"><span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/usuarios/{{Auth::user()->username}}/new">Recurso</a></li> 
            <li><a href="/usuarios/{{Auth::user()->username}}/categorias">Categoria</a></li> 
          </ul>
        </li>
        <!-- Notificaciones -->
        <script type="text/javascript">
         $(document).ready(function() {
          
            var numNotiAntes = $('.putEachNotificacion').text(), numNotiDespues;
            var isReadNoti = false;

            /*
            *  Actividad de las notificaciones visualizadas en vivo (~15")
            */
            setInterval(function(){
              $.ajax({
                  url: '/countnotifications',
                  type: 'post',
                  success: function(countRead){
                      numNotiDespues = countRead;
                      $('.putEachNotificacion').html(countRead);
                  }
                });

              $.ajax({
                  url: '/popnotifications',
                  type: 'post',
                  data: {
                    'userid': {{ Auth::user()->id }}
                  },
                  success: function(data){
                      if(numNotiAntes < numNotiDespues){
                        numNotiAntes = numNotiDespues;
                        popNotificacion($.parseJSON(data));
                      }
                  }
                });
                
            }, 15000);
          
            /*
            *  dropdown abriendo con ajax
            */
            $('.notificaciones').click(function() {  
              $('.notificationesLoading').show();
              $.ajax({
                url: '/notifications',
                type: 'post',
                beforeSend: function() {
                  $('.putNotificaciones').html('<center><img class="notificationesLoading" width="25px" src="/files/loading.gif"/></center>');
                },
                success: function(response){
                    $('.putNotificaciones').html(response);
                    $('.putEachNotificacion').html('0');
                    numNotiAntes = 0;
                    numNotiDespues = 0;
                }
              });
            });

            function popNotificacion(notifications){
              if (Notification) {
                if (Notification.permission !== "granted") {
                  Notification.requestPermission()
                }else {
                  /*
                  *  Solo sonará si se acptan las notificaciones
                  */
                    $('.notificacionBub').trigger('play');
                  }

                if(typeof noti != 'undefined'){
                  noti.onclose=null;
                  noti.close()
                }

                var title = notifications['username'];
                var extra = {
                  icon: notifications['gravatar'],
                  body: notifications['subject']
                }

                // Generamos la notificación
                noti = new Notification( title, extra);
                noti.onclick=function(){
                  window.focus();
                }
                setTimeout( function() { noti.close() }, 3000)
              }
            }

         });
        </script>
        <li class="notificaciones dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <img src="/images/notifications.png" style="margin:5px;">
              <span class="putEachNotificacion">{{Auth::user()->notifications()->unread()->count();}}</span>
              <span class="caret"></span>
          </a>
          <ul class="putNotificaciones dropdown-menu" role="menu" style="width:400px;"> 
            <center><img class="notificationesLoading" width="25px" src="/files/loading.gif"/></center>
          </ul>
        </li>
        <audio class="notificacionBub" src="/files/bub.mp3"></audio>
        <!-- end Notificaciones -->
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <img src="{{Auth::user()->gravatar(25)}}"> {{ Auth::user()->username }} <span class="caret"></span>
          </a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/">Mi perfil</a></li> 
            <li><a href="/faq">FAQ</a></li> 
            <li><a href="/perfil">Editar</a></li>
            <li class="divider"></li>
            <li><a href="/salir">Salir</a></li>
          </ul>
        </li>
      </ul>
      @else
      <form class="navbar-form navbar-right" method="post" action="/login" style="margin-right:-50px;">
        <input type="text" name="login" placeholder='Nombre de usuario/email' 
          class='form-control' value="{{ Input::old('login') }}" style="width:32%;padding:5px;margin:0px;">
        <input type="password" name="pass" placeholder='Contraseña' 
          class='form-control' value="{{ Input::old('pass') }}" style="width:25%;padding:5px;margin:0px;">
        <button class='btn btn-info'>Iniciar Sesión</button>
        <a href="/login/fb"><img style="height:35px;width:35px;" src="/images/facebook.gif" alt="Icono de facebook"></a>
        <a href="/password/remind">Ayuda</a>
      </form>
      @endif
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>


<div class="busquedaPanel row navbar-fixed-top hide" style="top:35px;left:150px;">
    <div class="col-md-4" style="width:370px;">
    <div class="busqueda panel panel-default" style="margin-top:20px;padding:10px;background-color:rgba(255,255,255,0.5);">
    </div>
  </div>   
</div>