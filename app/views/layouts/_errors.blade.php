<!-- Script para que las alertas se desvanezcan pasados 5 y 10 segundos segun prioridad !-->
<script>
window.setTimeout(function() {
    $(".alert-warning").fadeTo(500, 0).slideUp(100, function(){
        $(this).remove(); 
    });
}, 10000);
window.setTimeout(function() {
    $(".alert-success").fadeTo(500, 0).slideUp(100, function(){
        $(this).remove(); 
    });
}, 5000);
</script>

{{-- Flash or errors in the form --}}
@if(Session::has('flash_message'))
<div class="alert alert-warning alert-dismissible" role="alert" style="margin-top:10px;">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    {{ Session::get('flash_message') }}
</div>
@endif

@if($errors->has())
<div class="alert alert-warning alert-dismissible" role="alert" style="margin-top:10px;">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
      El formulario contiene los siguientes errores:
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
</div>
@endif

{{-- Success messages --}}

@if(Session::has('success_message'))
<div class="alert alert-success alert-dismissible" role="alert" style="margin-top:10px;">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    {{ Session::get('success_message') }}
</div>
@endif

