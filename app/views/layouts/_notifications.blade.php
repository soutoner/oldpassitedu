@forelse(Auth::user()->notifications()->unread()->get() as $notification)
    <div><li>
        <div class="eachNotificacion row"><a @if($notification->hasValidObject()) href="{{$notification->uri()}}" @endif >   
        
            <div class="col-md-2">
                <img src="{{$notification->from->gravatar(50)}}" style="margin-left:10px;">
            </div>
            <div class="col-md-10">
                <p style="word-wrap: break-word;">{{ $notification->subject }}</p>
            </div>
            
            {{$notification->readed()}}
        
        </a></div>
    </li></div>
    <hr style="margin:5px 0 5px 0px;">
    @empty
    <div>
        <li>
            <p style="padding-left:10px;">No hay novedades...</p>
        </li>
    </div>
    <hr style="margin:5px 0 5px 0px;">
@endforelse
<center><li><a href="/notificaciones">Ver todas las notificaciones</a></li></center>
