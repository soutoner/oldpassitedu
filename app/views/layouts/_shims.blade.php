<!-- CSS are placed here -->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/bocadillo.css">
<link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
<!-- Scripts are placed here -->
<script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.js"></script>
<script type="text/javascript" src="/ckeditor/ckeditor.js"></script>

<!-- tooltip bootstrap !-->
<script type='text/javascript'>
    $(document).ready(function() {
        $("[data-toggle=tooltip]").tooltip({html:true});
        $("[data-toggle=popover]").popover({html:true});
    }); 
</script>

<link href='https://fonts.googleapis.com/css?family=Roboto:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
