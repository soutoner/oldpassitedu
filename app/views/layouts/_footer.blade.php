<div class="dark center-block" id="footer"> <!-- footer !-->
  <ul class="nav navbar-nav"> <!-- menu derecha !-->
      <li><a href="/about">Sobre Nosotros</a></li>
      <li><a href="/contacto">Contacto</a></li>
      <li><a href="/faq">FAQ</a></li>
      <li><a href="/politicas">Políticas de Privacidad</a></li>
      <li><a href="/condiciones">Condiciones de Uso</a></li>
      <li><a href="/rank">Ranking de Usuarios</a></li>
  </ul><br><br><br>
  <center>Web realizada por Fran y  Adrian, estudiantes de Ingeniería Informática en la Universidad de Málaga.</center>
	<br>
</div>
