<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('layouts._shims')
    <title>
    	@yield('title', 'PassItEDU') - #YoSoyEDU {{-- PassItEDU es el valor por defecto --}}
    </title>

    @section('head')
    @show

	 </head>
  <body>
    @include('layouts._facebook')
  	<!-- Header -->
  	<header>
	  	@section('header')
	  		@include('layouts._header')
	    @show
  	</header>
		
		<!-- Container -->
    <div class="container-fluid">
      @include('layouts._errors')
    		<!-- Content -->
        @yield('content')
    </div>

		<!-- Footer -->
    <footer style="margin-top:20px;">
      @if(Request::segment(1)!=null) @include('publi/footer') @endif
			@section('footer')
				@include('layouts._footer')
	    @show
    </footer>
  @include('layouts.analyticstracking')
  </body>
</html>