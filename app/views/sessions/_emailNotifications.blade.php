<h4>Notificaciones:</h4>
<a href="/notifications/{{$user->username}}/enableAll"><button type="button" class="btn btn-primary btn-sm">Activar todas las notificaciones</button></a>
  <a href="/notifications/{{$user->username}}/disableAll"><button type="button" class="btn btn-danger btn-sm">Desactivar todas las notificaciones</button></a>

<hr>

<p>Mándame un email cuando mis seguidores suban recursos:</p>
<!--TODO-->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->notesNotification,"Y") == 0)
  <button type="submit" name="notification" value="notesNotification" class="btn btn-danger btn-sm">No, no quiero enterarme de esto</button>
@else
  <button type="submit" name="notification" value="notesNotification" class="btn btn-primary btn-sm">Si, mándame un correo</button>
@endif
</form>
<hr>
<p>Mándame un email cuando un usuario me siga</p>
<!-- TODO -->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->followersNotification,"Y") == 0)
  <button class="btn btn-danger btn-sm" type="submit" name="notification" value="followersNotification">No, no quiero enterarme de esto</button>

@else
  <button type="submit" class="btn btn-primary btn-sm" name="notification" value="followersNotification">Si,mándame un correo</button>
@endif
</form>
<hr>
<p>Mándame un email cuando haya nuevas funciones en la web</p>
<!-- TODO -->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->newsNotification,"Y") == 0)
  <button class="btn btn-danger btn-sm" type="submit" name="notification" value="newsNotification">No, no quiero enterarme de esto</button>

@else
  <button type="submit" class="btn btn-primary btn-sm" name="notification" value="newsNotification">Si,mándame un correo</button>
@endif
</form>
<hr>
<p>Mándame un email cuando comenten mis recursos</p>
<!-- TODO -->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->commentsNotification,"Y") == 0)
  <button class="btn btn-danger btn-sm" type="submit" name="notification" value="commentsNotification">No, no quiero enterarme de esto</button>

@else
  <button type="submit" class="btn btn-primary btn-sm" name="notification" value="commentsNotification">Si,mándame un correo</button>
@endif
</form>
<hr>
<p>Mándame un email sugeriendome apuntes</p>
<!-- TODO -->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->suggestionNotesNotifications,"Y") == 0)
  <button class="btn btn-danger btn-sm" type="submit" name="notification" value="suggestionNotesNotifications">No, no quiero enterarme de esto</button>

@else
  <button type="submit" class="btn btn-primary btn-sm" name="notification" value="suggestionNotesNotifications">Si,mándame un correo</button>
@endif
</form>
<hr>
<p>Mándame un email sugeriendome usuarios</p>
<!-- TODO -->

<form action="/notifications/{{$user->username}}/enableOrDisableNotification" method="post">
@if(strcmp($user->suggestionUsersNotifications,"Y") == 0)
  <button class="btn btn-danger btn-sm" type="submit" name="notification" value="suggestionUsersNotifications">No, no quiero enterarme de esto</button>

@else
  <button type="submit" class="btn btn-primary btn-sm" name="notification" value="suggestionUsersNotifications">Si,mándame un correo</button>
@endif
</form>
<hr>
<hr>
