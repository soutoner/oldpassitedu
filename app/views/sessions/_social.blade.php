<h4>Social:</h4>
<b>Facebook:</b>
<p>Al conectar con Facebook podrás iniciar sesion sin necesidad de introducir tus datos, siempre que estes conectado a Facebook.</p>
@if(in_array(Profile::Facebook, $user->profiles->lists('type')))
	Compartir cada vez que:<br> 
<form action="/login/fb/{{$user->username}}/update" method="post">
	<!-- <ul style="list-style:none;">
		<li>
		<input type="checkbox" name="share" value="1" @if($fbProfile->share) checked @endif> Subo un recurso 
		<a data-toggle="popover" data-placement="right" data-content="Si marcas esta opcion, se compartira en tu muro cada vez que subas un recurso. Si la desmarcas, PassItEDU nunca publicara en tu nombre.">(?)</a>
		</li>
	</ul>
	<button class="btn btn-default btn-sm" type="submit">Actualizar</button></a> -->
	<a href="/login/fb/{{$user->username}}/delete"><button type="button" class="btn btn-danger btn-sm">Desconectarme de Facebook</button></a>
</form>
@else
	<a href="/login/fb/"><img style="height:35px;width:200px;" src="/images/facebook-connect-button.png"></a>
@endif
<hr>
<b>Twitter:</b>
<p>Al conectar con Twitter podrás iniciar sesión sin necesidad de introducir tus datos, siempre que estes conectado a Twitter.<br>
También podrás compartir con tus seguidores toda la actividad que tengas en nuestra PassItEDU.</p>
@if(in_array(Profile::Twitter, $user->profiles->lists('type')))
	Twittear cada vez que:<br> 
<form action="/twitter/login/{{$user->username}}/update" method="post">
	<ul style="list-style:none;">
		<li>
			<input type="checkbox" name="share" value="1" @if($twProfile->share) checked @endif> Subo un recurso
			<a data-toggle="popover" data-placement="right" data-content="Si marcas esta opcion, se publicara un tweet cada vez que subas un recurso. Si la desmarcas, PassItEDU nunca publicara en tu nombre.">(?)</a>
		</li>
		<!--<li>
			<input type="checkbox" checked> Empiezo a seguir a un usuario
			<a data-toggle="popover" data-placement="right" data-content="Si marcas esta opcion, se compartira en tu muro cada vez que subas un recurso">(?)</a>
		</li>
		<li>
			<input type="checkbox" checked> Puntuo a otro usuario (+1 o -1)
			<a data-toggle="popover" data-placement="right" data-content="Si marcas esta opcion, se compartira en tu muro cada vez que subas un recurso">(?)</a>
		</li>
		<li>
			<input type="checkbox" checked> Marco como favorito un recurso
			<a data-toggle="popover" data-placement="right" data-content="Si marcas esta opcion, se compartira en tu muro cada vez que subas un recurso">(?)</a>
		</li>!-->
	</ul>
	<button class="btn btn-default btn-sm" type="submit">Actualizar</button></a>
	<a href="/twitter/login/{{$user->username}}/delete"><button type="button" class="btn btn-danger btn-sm">Desconectarme de Twitter</button></a>
</form>
@else
	<a href="/twitter/login/"><button type="button" class="btn btn-primary">Conectarme a Twitter</button></a>
@endif
<hr>
<h4>Usuario:</h4>
<p>Si deseas borrar tu cuenta pulsa en el siguiente botón. Ten en cuenta que se eliminará toda tu información, tanto tus datos como todos los recursos que hayas subido y no se podra recuperar.</p>
<a href="/deleteUser"><button type="button" class="btn btn-danger" onClick="if(confirm('¿Estas seguro que quieres darte de baja? Todos los recursos y tu informacion se perderan para siempre.'))
            ;
            else return false;">Eliminar Cuenta</button></a>
