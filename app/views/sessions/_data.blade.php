<form method="post" action="updateData">

  <div class="form-group">
  	<label>Nombre:</label>
		<input type="text" name="name" class='form-control' style='width:50%;' 
		value="{{ $user->name }}">
  </div>
  <div class="form-group">
  	<label>Apellidos:</label>
		<input type="text" name="surname" class='form-control' style='width:50%;' 
		value="{{ $user->surname }}">
  </div>

  <div class="form-group">
  	<label>Estudios:</label>
		<input type="text" name="studies" placeholder="Dinos que estudias o has estudiado (Opcional)" 
		class='form-control' style='width:50%;' value="{{ $user->studies }}">
  </div>
  <div class="form-group">
    <a href="/usuarios/soutoner/recursos/como-cambiar-tu-avatar">¿Cómo cambiar tu avatar?</a>
  </div>
		<button class="btn btn-large btn-primary" >Guardar</button>
</form>
