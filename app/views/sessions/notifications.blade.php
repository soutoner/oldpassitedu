@section('title', 'Notificaciones')

@section('content')
<!-- CABECERA !-->
@include('users._cabecera')

<!-- FIN CABECERA !-->

<div id="avatar" class="col-md-3">
  <div class="panel panel-default" style="width:250px;">
    <center><img src='{{ $user->gravatar(200) }}' style='margin-top:10px;border-radius:3px;'/></center>
    <div class="panel-body">
      <p>{{$user->short_desc}}</p>
      <small>{{$user->studies}}</small>
      <br><br>
      @include('users._followbuttons')
    </div>
   
    <!-- ESTADISTICAS USUARIO !-->
    <ul class="list-group">
      <li class="list-group-item">
        <span class="badge">{{ $user->followers->count() }}</span>
        <a href="followers">Seguidores</a>
      </li>
      <li class="list-group-item">
        <span class="badge">{{ $user->follow->count() }}</span>
        <a href="following">Siguiendo</a>
      </li>
      <li class="list-group-item">
        <span class="badge" style="background:#D8D8D8;{{{$user->favs < 0 ? 'color:#FF0040;' : 'color:#088A08;'}}}">{{$user->favs}}</span>
        Favs
      </li>
    </ul>
  </div>
</div>

<div id="main" class="col-md-9">
	<!-- SEGUIDORES !-->
  	<div class="panel panel-default" style="padding:10px;"> 
      @forelse($notifications as $notification)
      <div><div class="row"><a @if($notification->hasValidObject()) href="{{$notification->uri()}}" @endif >   
          
              <div class="col-md-2">
                  <img src="{{$notification->from->gravatar(60)}}" style="margin-left:10px;">
              </div>
              <div class="col-md-10">
                  <p style="word-wrap: break-word;">{{ $notification->subject }}</p>
              </div>
              
              {{--$notification->readed()--}}
          
          </a></div></div>
      <hr style="margin:5px 0 5px 0px;">
      @empty
      <div>
          No hay novedades...
      </div>
      @endforelse
    </div>
	  <center>{{$notifications->links()}}</center>
</div>
@endsection