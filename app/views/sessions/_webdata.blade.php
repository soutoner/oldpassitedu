<form method="post" action="updatePassword">

  <div class="form-group">
  	<label>Username:</label>
		<input type="text" name="username" class='form-control' style='width:50%;' 
		value="{{ $user->username }}" disabled>
  </div>
  <div class="form-group">
  	<label>Email:</label>
		<input type="email" name="email" class='form-control' style='width:50%;' 
		value="{{ $user->email }}" disabled>
  </div>

  <hr>

  <h4>Cambiar contraseña:</h4>

  <div class="form-group">
  	<label>Contraseña antigua:</label>
		<input type="password" name="old_password" placeholder="Escribe tu contraseña antigua" 
		class='form-control' style='width:50%;'>
  </div>

  <div class="form-group">
  	<label>Contraseña nueva:</label>
		<input type="password" name="password" placeholder="Escribe tu nueva contraseña" 
		class='form-control' style='width:50%;' value="{{ Input::old('password') }}">
  	<p class="help-block">Mínimo 6 caractéres, al menos una mayúscula y un número.</p>
  </div>

  <div class="form-group">
  	<label>Repite la contraseña:</label>
		<input type="password" name="password_confirmation" placeholder="Repite su nueva contraseña" 
		class='form-control' style='width:50%;'>
  </div>

		<button class="btn btn-large btn-primary" >Guardar</button>
</form>