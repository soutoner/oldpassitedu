@section('title', 'Perfil de '.$user->username)

{{-- Esto es lo que va al yield  --}}
@section('content')
@include('users._cabecera')

  <div id="avatar" class="col-md-3">
    <div class="panel panel-default" style="width:250px;"> <!-- sobre el usuario !-->
      <center><img src='{{ $user->gravatar(200) }}' style='margin-top:10px;border-radius:3px;' data-toggle="popover" data-placement="top" 
   data-content="<center>Para cambiar la imagen de tu avatar pulsa <a href='#'>aquí</a></center>" /></center>
      <div class="panel-body">        
	  		<div id="content"><textarea rows="8" name="short_desc" class='form-control input-lg' form="description">{{ $user->short_desc }}</textarea>
	    	<form method="post" action="updateDesc" id="description">
	    		<button class="btn btn-large btn-primary" style="margin-top:10px;" >Guardar</button>
	    	</form></div>
      </div>  
    </div>
  </div>

  <div id="main" class="col-md-9">
    <div class="row"> <!-- aqui dentro van los bloques de configuraciones !-->
			<!-- Nav tabs -->
			<ul class="nav nav-tabs dark" role="tablist">
			  <li class="active"><a href="#home" role="tab" data-toggle="tab">Datos</a></li>
			  <li><a href="#profile" role="tab" data-toggle="tab">Web</a></li>
			  <li><a href="#messages" role="tab" data-toggle="tab">Preferencias</a></li>
			  <li><a href="#notifications" role="tab" data-toggle="tab">Notificaciones</a></li>
			</ul>

			<!-- Tab panes contenido de cada seccion -->
			<div class="tab-content" style="margin-bottom:20px;">
			  <div class="tab-pane active" id="home" >
			  	@include('sessions/_data')
			  </div>
			  <div class="tab-pane" id="profile">
			  	@include('sessions/_webdata')
			  </div>
			  <div class="tab-pane" id="messages">
			  	@include('sessions/_social')
			  </div>
			  <div class="tab-pane" id="notifications">
			  	@include('sessions/_emailNotifications')
			  </div>
			</div>

    </div>
  </div>
@endsection