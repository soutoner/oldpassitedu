<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Contraseña olvidada</h2>

		<div>
			Para recuperar tu contraseña, completa el siguiente formulario: {{ URL::to('password/reset', array($token)) }}.<br/>
			El enlace expirara en {{ Config::get('auth.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>
