<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Bienvenidos a PassItEDU</h2>

		<div>
			Para poder registrarte completa el siguiente formulario: {{ URL::to('access/unete', array($token)) }}.<br/>
			Este enlace expirara en {{ Config::get('access.reminder.expire', 60) }} minutos.
		</div>
	</body>
</html>