<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Recurso reportado</h2>

		<div>
			Datos del recurso reportado:<br>
			Usuario:{{$username}}<br>
			id del recurso:{{$id}}<br>
			{{$texto}}<br>
			{{$pdf}}<br>
			{{$link}}<br>
			{{$inapropiado}}
		</div>
	</body>
</html>
