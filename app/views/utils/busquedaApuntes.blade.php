<style>
.unabusqueda:hover {
    border-color: #666;
}
</style>
<?php
	/**
	*	recortarTexto(String, Tamaño maximo del Str)
	*	en el <h5> son 31char - la suma del name y del username
	*	el resultado será el tamaño max del apellido
	*
	*	en short_desc se ha limitado los char que se muestran a 50
	*	(porque el tamaño es más pequeño que en el <h5> por tanto se 
	*	pueden mostrar más número de char)
	**/
	function recortarTexto($texto, $limite){
	    if(strlen($texto) <= $limite){
	        return $texto;
	    }else{
	        return substr($texto, 0, $limite).'..';
	    }   
	    
	}

	/**
	*	Busqueda de usuario por id
	*	return name
	**/
	function searchUser($idUsr){
		return User::find($idUsr)->name;
	}

	/**
	*	Busqueda de usuario por id
	*	return username
	**/
	function searchUserName($idUsr){
		return User::find($idUsr)->username;
	}

?>
@foreach ($found as $apuntes)
	<div class="unabusqueda panel panel-heading row" style="margin:0 0 5px 0;">
		<div class="col-md-3" style="padding-left:5px;width:65px;">
			<img height="50" width="50" src="https://placehold.it/50x50&text={{str_replace(' ','%20',$apuntes->title)}}" align="middle"/>
		</div>
		<div class="row">
			<h5 style="margin-top:0px;">
				<a href="/usuarios/{{searchUserName($apuntes->user_id).'/recursos/'.$apuntes->slug}}">{{recortarTexto($apuntes->title, 31-strlen(searchUser($apuntes->user_id))) }}</a>
				<small><a href="/usuarios/{{searchUserName($apuntes->user_id)}}">{{'@'.searchUser($apuntes->user_id)}}</a></small>			
			</h5>
			<small>{{ recortarTexto($apuntes->short_desc,50) }}</small>
		</div>
	</div>
@endforeach
@if (count($found) === 0)
	<div class="row panel panel-heading" style="margin:0 0 10px 0;">
			
		<p style="color:black; text-align:center;">No hay apuntes con el nombre de <span style="color:#666;font-style: italic;">{{ $buscador }}</span>.</p>
		
	</div>
@else
	<div class="unabusqueda row" style="margin:0 0 10px 0;">
		<form action="/utils/search/" method="POST">
			<input type="hidden" class="busquedamas" name="buscador" value="{{ $buscador }}">
			<input type="hidden" name="cond" value="2">
			<input class="btn btn-default" style="width:100%;" type="submit" value="más..">
		</form>
	</div>
@endif