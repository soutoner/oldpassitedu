@section('title','Buscador')
@section('content')

<center>
	<div class="input-group" style="margin-top:30px;margin-bottom:20px;">
		<form class="input-group" action="/utils/advancedSearch" method="post">
			<div class="input-group">
	      		<input type="text" class="form-control" value="{{ Input::get('buscador')}}" name="buscador">
	      		<input type="text" style="display:none;" value="1"name="bool">
	      		<span class="input-group-btn">
	        	<button class="btn btn-default" type="submit" style="border-radius:0px;">Busqueda avanzada</button>
	      		</span>
	    	</div>
		  	<span class="input-group-addon">Curso</span>
		 	<select class="form-control" name="curso[]">
		  	<option value="todos" >Todos</option>
			  <option value="secundaria">Secundaria</option>
			  <option value="bachillerato">Bachillerato</option>
			  <option value="universidad">Universidad</option>
			  <option value="otro">Otro</option>
			</select>
			<span class="input-group-addon">Usuarios</span>
			<select class="form-control" name="usuarios[]">
			<option value="todos">Todos</option>
				<option value="mios">Mis seguidores</option>
				<option value="otros">Otros usuarios</option>
			</select>
		</form>
	</div>
</center>	
<div class="row">
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600left') </div>
	<div class="col-md-8">
	<div class="panel panel-default" >

		@for ($i = 0; $i < count($found); $i++)
<?php 
		$username = User::find($found[$i]->user_id);
		$username = $username->username;
	

?>			
			<div class="panel panel-heading row">
				<div class="col-md-2"><img height="100" width="100" src="https://placehold.it/192x200&text={{$found[$i]->title}}"/></div>
				<div class="col-md-5">
					<h3><a <?php  echo "href=/usuarios/"?>{{$username}}<?php echo "/recursos/";?>{{$found[$i]->slug}}>{{$found[$i]->title}}</a> <small> @<?php echo $username;?></small></h3>
					<p class="lead">{{$found[$i]->short_desc}}</p>
				</div>
					<div class="col-md-3 pull-right">
				 		<table>
				 			<tr><td style="padding: 0 0 0 0;">Favs:</td> <td><span class="badge" {{$found[$i]->colorFavs()}} >{{$found[$i]->favs}}</span></td></tr>
				 		</table>
    				
				</div>									
			</div>
		@endfor
		<center><?php if(count($found)!= 0)echo $found->appends(array('n' => '1','buscador' =>Input::get('buscador')))->links(); ?></center>
		</div>
		</div>
		<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600right') </div>
	</div>
</div>
@endsection

