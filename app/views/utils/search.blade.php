@section('title','Buscador')
@section('content')
<div class="row">
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600left') </div>
	<div class="col-md-8">
		<div class="panel panel-default" style="margin-top:20px;padding:20px;">
			<script type="text/javascript">
			    $(document).ready(function() {
			        var seguir ='\u2714 Seguir', seguido = '\u2718 Seguido';

			        /*seguir usuario*/
			        $('.follow').click(function() {
						var texto;
						var nombreUsuario = $(this).attr('name');
						$(this).toggleClass('btn-success btn-primary');  

						if($(this).hasClass('btn-success')){
							texto = seguido;
							ajax('follow', nombreUsuario);
						}else{
							texto = seguir;
							ajax('unfollow', nombreUsuario);
						}
						$(this).val(texto);
					});

			        function ajax(accion, nombreUser){
			          $.ajax({
			              url: '/usuarios/'+nombreUser+'/'+accion,
			              type: 'post'
			            });
			        }
			    });
			</script>
			@foreach ($found as $usr)
				@include('partials._users')
			@endforeach
			<center><?php if(count($found)!= 0)echo $found->links(); ?></center>
		</div>
	</div>
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600right') </div>
</div>
@endsection