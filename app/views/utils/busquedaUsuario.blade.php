<style>
.unabusqueda:hover {
    border-color: #666;
}
</style>
<?php
	/**
	*	recortarTexto(String, Tamaño maximo del Str)
	*	en el <h5> son 31char - la suma del name y del username
	*	el resultado será el tamaño max del apellido
	*
	*	en short_desc se ha limitado los char que se muestran a 50
	*	(porque el tamaño es más pequeño que en el <h5> por tanto se 
	*	pueden mostrar más número de char)
	**/
	function recortarTexto($texto, $limite){
	    if(strlen($texto) <= $limite){
	        return $texto;
	    }else{
	        return substr($texto, 0, $limite).'..';
	    }   
	    
	}
?>
@foreach ($found as $usr)
	<div class="unabusqueda panel panel-heading row" style="margin:0 0 5px 0;">
		<div class="col-md-3" style="padding-left:5px;width:65px;">
			<img src="{{$usr->gravatar(50)}}" align="middle"/>
		</div>
		<div class="row">
			<h5 style="margin-top:0px;">{{ $usr->name.' '.recortarTexto($usr->surname, 31-(strlen($usr->name)+strlen($usr->username))).' - ' }} 
				<small><a href="/usuarios/{{$usr->username}}">{{'@'.$usr->username}}</a></small>
			</h5>
			<small>{{ recortarTexto($usr->short_desc,50) }}</small>
		</div>
	</div>
@endforeach
@if (count($found) === 0)
	<div class="row panel panel-heading" style="margin:0 0 10px 0;">
			
		<p style="color:black; text-align:center;">No hay usuarios con el nombre de <span style="color:#666;font-style: italic;">{{ $buscador }}</span>.</p>
		
	</div>
@else
	<div class="unabusqueda row" style="margin:0 0 10px 0;">
		<form action="/utils/search/" method="POST">
			<input type="hidden" class="busquedamas" name="buscador" value="{{ $buscador }}">
			<input type="hidden" name="cond" value="1">
			<input class="btn btn-default" style="width:100%;" type="submit" value="más..">
		</form>
	</div>
@endif