@section('title', 'Ranking de usuarios')

@section('content')
	<center>
	<div class="input-group" style="margin-top:30px;margin-bottom:20px;">
		<form class="input-group" action="/showResults" method="post">
			<span class="input-group-addon">Ranking</span>
			<select class="form-control" name="usuarios[]">
			<option value="0">Todos</option>
				<option value="1">Mejores usuarios</option>
				<option value="2">Mas Favs</option>
				<option value="3">Mas Seguidores</option>
				<option value="4">Mayor Seguidor</option>
			</select>
	      		<span class="input-group-btn">
	        	<button class="btn btn-default" type="submit" style="border-radius:0px;">Mostrar resultados</button>
	      		</span>
		</form>
	</div>
</center>
<center>
		<?php 
				
				switch ( Input::get('usuarios')[0]) {
				    case "0":
							echo "<p style='color:white;font-size:1.5em;'>Usuarios de la web</p>";
						break;
				    case "1":
				      		echo "<p style='color:white;font-size:1.5em;'>Top 100 mejores usuarios que suben recursos</p>";
				        break;
				    case "2":
				       		echo "<p style='color:white;font-size:1.5em;'>Top 100 +Favs</p>";
				        break;
				    case "3":
				  			echo "<p style='color:white;font-size:1.5em;'>Top 100 +Followers</p>";
				  		break;
				    case "4":
				    		echo "<p style='color:white;font-size:1.5em;'>Top 100 +Following</p>";
				    	break;
				}
				 
		?>
</center>
<div class="row">
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600left') </div>
	<div class="col-md-8">
		<div class="panel panel-default" style="margin-top:20px;padding:20px;">
		
		<?php if(isset($users)){$cont = 0;?>

			@foreach ($users as $usr)
					<?php $cont++; ?>
					@include('partials._users')		
		  @endforeach
		  <center>@if(!isset($link)){{$users->links() }} @endif</center>
		<?php }?>
		</div>
	</div>
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600right') </div>
</div>
@endsection