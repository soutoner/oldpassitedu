<!-- ACTIVIDAD DEL USUARIO !-->
<div class="tab-pane @if(!ViewHelpers::loged_in($user->username)) active @endif" id="profile">
  <div class="panel-default"> <!-- ultimos apuntes y buascar !-->
    @forelse($activity as $feed)
      @include('users._feed')
    @empty
    <div class="panel panel-heading row" >
       No hay actividad reciente ...
    </div>
    @endforelse
    <center>{{$activity->links()}}</center>
  </div> 
</div>
<!-- FIN ACTIVIDAD DEL USUARIO !-->