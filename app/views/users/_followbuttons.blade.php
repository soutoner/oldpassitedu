<script type="text/javascript">
    $(document).ready(function() {
          var numFavs = parseInt($('.numFavs').text()), numSeguidores = parseInt($('.numSeguidores').text()); 
          var seguir ='\u2714 Seguir', seguido = '\u2718 Seguido';

          @if(Auth::check())

            @if(ViewHelpers::loged_in($user->username))
              @elseif($user->faved_by(Auth::user()->id))
                $('.fav').removeClass('hide btn-default').addClass('btn-primary');
                $('.defav').removeClass('hide').addClass('btn-default disabled');
              @elseif($user->defaved_by(Auth::user()->id))
                $('.fav').removeClass('hide').addClass('btn-default disabled');
                $('.defav').removeClass('hide btn-default').addClass('btn-danger');
              @elseif(!$user->defaved_by(Auth::user()->id))
                $('.fav, .defav').removeClass('hide');
            @endif

            @if(ViewHelpers::loged_in($user->username))
              @elseif($user->followed_by(Auth::user()->id))
                $('.follow').removeClass('hide').addClass('btn-success').val(seguido);
              @else
                $('.follow').removeClass('hide').addClass('btn-primary').val(seguir);
            @endif

          @endif


        $('.fav').click(function() { 
          $(this).toggleClass('btn-primary btn-default');  
          $('.defav').toggleClass('disabled');      

          if($('.defav').hasClass('disabled')){
            ajax('fav');
            numFavs++;
            numeroFavs();
          }else{
            ajax('unfav');
            numFavs--;
            numeroFavs();
          }

        });

        $('.defav').click(function() { 
          $(this).toggleClass('btn-danger btn-default');  
          $('.fav').toggleClass('disabled');

          if($('.fav').hasClass('disabled')){
            ajax('defav');
            numFavs--;
            numeroFavs();
          }else{
            ajax('undefav');
            numFavs++;
            numeroFavs();
          }

        });

        /*seguir usuario*/
        $('.follow').click(function() { 
          var texto;
          $(this).toggleClass('btn-success btn-primary');  
          if($(this).hasClass('btn-success')){
            texto = seguido;
            ajax('follow');
            numSeguidores++;
            $('.numSeguidores').text(numSeguidores);
          }else{
           texto = seguir;
           ajax('unfollow');
           numSeguidores--;
          $('.numSeguidores').text(numSeguidores);
          }
          $(this).val(texto);
        });

        function ajax(accion){
          $.ajax({
              url: '{{$user->username}}/'+accion,
              type: 'post'
            });
        }

        function numeroFavs(){
          if(numFavs>=0){
            $('.numFavs').html(numFavs).css('color', '#088A08');
          }else{
            $('.numFavs').html(numFavs).css('color', '#FF0040');
          }
        }
    });
</script>

<center>
  <!-- SEGUIR USUARIO !-->
  @if(Auth::check())
    <div class="col-md-6">
      <input class="hide follow  btn" type="button" name="follow" value="" />
    </div>
    <!-- +1 BOTON !-->
    <div class="col-md-3">
      <input class="hide btn btn-default fav" type="button" name="fav" value="+1" />
    </div>
    <!-- -1 BOTON !-->
    <div class="col-md-3">
      <input class="hide btn btn-default defav" type="button" name="defav" value="-1" />
    </div>
  @endif
</center>