@section('title', 'Passiteros')

@section('content')
<div class="row">
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600left') </div>
	<div class="col-md-8">
		<div class="panel panel-default" style="margin-top:20px;padding:20px;">
			@foreach ($users as $usr)
					@include('partials._users')		
		  @endforeach
		  <center>{{ $users->links() }} </center>
		</div>
	</div>
	<div class="col-md-2" style="margin-top:20px;">	@include('publi/160x600right') </div>
</div>
@endsection