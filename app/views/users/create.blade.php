@section('title', 'Únete')
@section('content')
  <h1 class="dark">Únete</h1>
    <div class="col-md-6 dark" id="form">
    <form method="post" action="/users">
      <div class="form-group">
          <p>Sobre ti</p>
          <input  class="form-control" style="width:30%" type="text" name="name" 
          placeholder="Nombre" value="{{ (!empty($fbUser)) ? $fbUser['first_name'] : Input::old('name') }}" required>
      </div>
      <div class="form-group">
        <input  class="form-control" style="width:50%" type="text" name="surname" 
        placeholder="Apellidos" value="{{ (!empty($fbUser)) ? $fbUser['last_name'] : Input::old('surname') }}" required>
      </div>   
      <div class="form-group">
        <input  class="form-control" type="text" name="studies" 
        placeholder="Dinos que estudias o has estudiado (Opcional)" 
        style="width:60%" value="{{ Input::old('studies') }}">
      </div>
      <div class="form-group">
        <textarea  class="form-control" type="text" name="short_desc" 
        placeholder="Cuentanos algo sobre ti (Opcional)" 
        style="width:60%"></textarea>
         <p class="help-block" style="color:#dfdfdf;">Max. 140 caracteres.</p>
      </div>
      <div class="form-group">
        <p>Sexo</p>
        <table class="dark">
          <tr><td>Hombre</td><td></td><td><input type="radio" name="gender" value="H" @if(!empty($fbUser) && $fbUser['gender']=='H') checked @endif required></td></tr>
          <tr><td>Mujer</td><td> <input type="radio" name="gender" value="M" @if(!empty($fbUser) && $fbUser['gender']=='M') checked @endif required></td></tr>
        </table>
      </div>
      <div class="form-group">
        <p>Fecha de nacimiento</p>
        <input type="date" class="form-control" style="width=30%" placeholder="dd-mm-aaaa" name="birth" value="{{ Input::old('birth') }}" min="{{date('Y-m-d',strtotime('-100 years'))}}" max="{{date('Y-m-d',strtotime('-16 years'))}}" required>
        <p class="help-block" style="color:#dfdfdf;">Recuerda: tienes que tener mas de 16 años.</p>
      </div>
      
      <div class="form-group">
        <p>Para la web</p>
        <input type="text" class="form-control" style="width=50%" name="username" 
        placeholder="Elige un nombre de usuario" value="{{ Input::get('username') }}" required>
      </div>
      <div class="form-group">
        <input type="email" class="form-control" style="width=50%" name="email" 
        placeholder="pepito@example.com" value="{{ (!empty($fbUser)) ? $fbUser['email'] : Input::get('email') }}" required>
      </div>
      <div class="form-group">
        <input type="password" class="form-control" style="width=50%" name="password" 
        placeholder="Elige tu contraseña" value="{{ Input::get('password') }}" required>
        <p class="help-block" style="color:#dfdfdf;">Mínimo 6 caracteres, al menos una mayúscula y un número.</p>
      </div>
      <div class="form-group">
      <input type="password" class="form-control" style="width=50%" name="password_confirmation" 
      placeholder="Confirma tu contraseña" required>
      </div>

      <input type="hidden" name="uid" value="{{ (!empty($fbUser)) ? $fbUser['id'] : '0' }}">
      <input type="hidden" name="accessToken" value="{{ (!empty($fbUser)) ? $fbUser['accessToken'] : '0' }}">

      <div class="form-group">
       Al registrarte confirmas que has leído y aceptado las <a href="/politicas">'Política de Privacidad'</a>, y
         <a href="/condiciones">'Términos de Uso'</a>
      </div>
    
    <button class="btn btn-large btn-info">Enviar</button>
    
    </form>
  </div>
  
  <div class="col-md-6" id="intro">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2 class="panel-title">¿Cómo funciona PassItEDU?</h2>
      </div>
      <div class="panel-body" style="width:550px;">
        <div>
        <img src="/images/mascotacolor.png" height="100px"width="55px" class="col-md-2" style="margin:10px;">
        <p>
          Bienvenido! yo soy Edu y te voy a contar como funciona la web.
          <h4>¿Que puedo hacer?</h4>
          Una vez que te registres en la web, podrás <b>subir tus apuntes</b> a la web de la forma que tu prefieres, ya sea en formato de <b>texto</b>, <b>PDF</b> o como video de YouTube.
          <h4>¿Por qué 'era social'?</h4>
          Claro! Ahora en PassitEDU podrás <b>seguir</b> a otros usuarios para enterarte cuando suben apuntes nuevos, incluso <b>puntuarlos</b> y mandar comentarios o mensajes privados para ponerte en contacto con ellos.
          <br></br>
          Quiero que <b>compartir tus apuntes</b> sea ahora más fácil que nunca. Pero siempre podrás elegir si un <b>invitado</b> puede ver tus apuntes, o tiene que unirse a nosotros!
          <h4>¿Pero a qué precio?</h4>
          <b>Totalmente gratis</b>! Veras publicidad que nos ayuda a mantener el sitio activo, pero trataré que sea lo menos molesta para que tu experiencia sea lo mas comoda posible.
        </p>
        </div>
        <hr style="width:100px;">
        <img src="/images/gomalapiz.png" height="100px" width="100px" style="display:block;margin:-10px auto 0 auto;align:center;">
      </div>
    </div>
  </div>
@endsection