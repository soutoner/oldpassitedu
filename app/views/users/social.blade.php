@section('title', 'Estadisticas de '.$user->username)

@section('content')
<!-- CABECERA !-->
@include('users._cabecera')

<!-- FIN CABECERA !-->

<div id="avatar" class="col-md-3">
  <div class="panel panel-default" style="width:250px;">
    <center><img src='{{ $user->gravatar(200) }}' style='margin-top:10px;border-radius:3px;'/></center>
    <div class="panel-body">
      <p>{{$user->short_desc}}</p>
      <small>{{$user->studies}}</small>
      <br><br>
      @include('users._followbuttons')
    </div>
   
    <!-- ESTADISTICAS USUARIO !-->
    <ul class="list-group">
      <li class="list-group-item">
        <span class="badge">{{ $user->followers->count() }}</span>
        <a href="followers">Seguidores</a>
      </li>
      <li class="list-group-item">
        <span class="badge">{{ $user->follow->count() }}</span>
        <a href="following">Siguiendo</a>
      </li>
      <li class="list-group-item">
        <span class="badge" style="background:#D8D8D8;{{{$user->favs < 0 ? 'color:#FF0040;' : 'color:#088A08;'}}}">{{$user->favs}}</span>
        Favs
      </li>
      <li class="list-group-item">
        <a href="sugerencias">Seguir a otros usuarios</a>
      </li>
    </ul>
  </div>
</div>

<div id="main" class="col-md-9">
	<!-- SEGUIDORES !-->
  	<div class="panel panel-default" style="padding:10px;"> 
    @forelse($usuarios as $usr)
		  @include('partials._users')  
		@empty
    <div class="panel panel-heading row" >
		 El usuario todavía no sigue a nadie.
    </div>
	  @endforelse
    </div>
	  <center>{{$usuarios->links()}}</center>
</div>
@endsection
