<!-- ACIVIDAD DE LOS FOLLOWING !-->
@if(Auth::check() && ViewHelpers::loged_in($user->username))
<div class="tab-pane active" id="home">
  <div class="panel-default"> 
    @forelse($pagFeed as $feed)
      @include('users._feed')
    @empty
    <div class="panel panel-heading row" >
        No hay actividad reciente de tus seguidores ...
    </div>
    @endforelse
    <center>{{$pagFeed->links()}}</center>
  </div>
</div>
@endif
<!-- FIN ACIVIDAD DE LOS FOLLOWING !-->