@section('title', $user->username)

@section('content')
<!-- CABECERA !-->
@include('users._cabecera')
<!-- FIN CABECERA !-->

<!-- LATERAL IZQUIERDO !-->
<div id="avatar" class="col-md-3">
  <!-- DESCRIPCION !-->
  <div class="panel panel-default" >
    <center><img src='{{ $user->gravatar(200) }}' style='margin-top:10px;border-radius:3px;'/></center>
    <div class="panel-body">
      <p>{{$user->short_desc}}</p>
      <small>{{$user->studies}}</small>
      <br><br>
      @include('users._followbuttons')
    </div>
   
    <!-- ESTADISTICAS USUARIO !-->
    <ul class="list-group">
      <li class="list-group-item">
        <span class="badge numSeguidores">{{ $user->followers->count() }}</span>
        <a href="{{$user->username}}/followers">Seguidores</a>
      </li>
      <li class="list-group-item">
        <span class="badge">{{ $user->follow->count() }}</span>
        <a href="{{$user->username}}/following">Siguiendo</a>
      </li>
      <li class="list-group-item">
        <span class="badge numFavs" style="background:#D8D8D8;{{{$user->favs < 0 ? 'color:#FF0040;' : 'color:#088A08;'}}}">{{$user->favs}}</span>
        Favs
      </li>
      <li class="list-group-item">
        <a href="{{$user->username}}/sugerencias">Seguir a otros usuarios</a>
      </li>
    </ul>
  </div>
  <!-- FIN DESCRIPCION !-->
</div>
<!-- FIN LATERAL IZQUIERDO !-->

<!-- PANEL CENTRAL !-->
<div class="col-md-6">
  <!-- AÑADIR RECURSO FORM !-->
  @include('partials._addrecursoform')
  <br>
  <!-- FIN AÑADIR RECURSO FORM !-->

  <!-- PESTAÑAS BLOQUE PRINCIPAL -->
  <ul class="nav nav-tabs dark" role="tablist">
  @if(Auth::check())
    @if(ViewHelpers::loged_in($user->username)) <!-- yo estoy logueado !-->
    <li class="active"><a href="#home" role="tab" data-toggle="tab">Novedades</a></li>
    <li><a href="#desktop" role="tab" data-toggle="tab">Escritorio Personal</a></li>
    @endif
    <li @if(!ViewHelpers::loged_in($user->username)) class="active" @endif><a href="#profile" role="tab" data-toggle="tab">
      @if(ViewHelpers::loged_in($user->username)) Mi Actividad @else Actividad de {{$user->username}}  @endif
    </a></li>
  @else
    <li class="active"><a href="#profile" role="tab" data-toggle="tab">
      Actividad de {{$user->username}}
    </a></li>
  @endif
  </ul>
  <!-- FIN PESTAÑAS BLOQUE PRINCIPAL -->
  
  <!-- CONTENIDO PESTAÑAS -->
  <div class="tab-content">
    <!-- ACIVIDAD DE LOS FOLLOWING !-->
    @include('users._following')
    <!-- FIN ACIVIDAD DE LOS FOLLOWING !-->

    <!-- ESCRITORIO PERSONAL !-->
    @include('users._personal')
    <!-- FIN ESCRITORIO PERSONAL !-->

    <!-- ACTIVIDAD DEL USUARIO !-->
    @include('users._useractivity')
    <!-- FIN ACTIVIDAD DEL USUARIO !-->
  </div>
  <!-- FIN CONTENIDO PESTAÑAS -->

</div>
<!-- FIN PANEL CENTRAL !-->

<!-- PANEL DERECHO !-->
<div class="col-md-3">
  <!-- CATEGORIAS PANEL !-->
  <div class="panel panel-default">
    <!-- AÑADIR CATEGORIA FORM !-->
    <div class="panel-body">
      <div class="col-md-10">
        <a href="{{$user->username}}/categorias" style="color:black;text-decoration:underline;"><h5 style="margin-left:-10px;">Últimas Categorias</h5></a>
      </div>
      @if(ViewHelpers::loged_in($user->username))
      <button type="button" class="btn btn-default add" >+</button>
        <script>
          $(document).ready(function() {
            $(".add").click(function() {
             $(".block").toggle('"."showOrHide"."');
            });
          });
        </script>
       <div class="triangle-isosceles block" style="display:none;">
        @include('partials._addcategoryform')
       </div>
      @endif
    </div>
    <!-- FIN AÑADIR CATEGORIA FORM !-->
    <!-- CATEGORIAS USUARIO !-->
    @forelse($user->categories->reverse()->slice(0,10) as $cat)
      <div class="list-group">
        <a href="{{$user->username}}/categorias/{{$cat->slug}}" class="list-group-item disabled">
          {{ Str::limit($cat->title, 40) }}
        </a>
      </div>
    @empty
      <p style="padding:10px;">El usuario no ha añadido categorías todavía...</p>
    @endforelse 
    @if($user->categories->count()>10) <center><a href="{{$user->username}}/categorias">Ver más</a></center> @endif
  </div>
  <!-- FIN CATEGORIAS PANEL !-->
</div>
<!-- FIN PANEL DERECHO !-->
@endsection