
<div class="panel panel-heading row" style="margin:0 0 10px 0;">
  <div class="col-md-3"><img style="margin-left:10px;" src="{{$feed->user->gravatar(80)}}" align="middle"/></div>
  <div class="col-md-9">
	    <h4>{{$feed->user->name.' '.$feed->user->surname}} - <small><a href="/usuarios/{{$feed->user->username}}">{{'@'.$feed->user->username}}</a></small></h4>
	    <p>Ha subido <a href="{{$feed->user->username}}/recursos/{{$feed->slug}}">{{{$feed->title}}}</a>@<a href="{{$feed->user->username}}/categorias/{{$feed->category->slug}}">{{{$feed->category->title}}}</a></p>
	    <small class="pull-right">{{ViewHelpers::time_ago($feed->created_at)}}</small>
  </div>
</div>