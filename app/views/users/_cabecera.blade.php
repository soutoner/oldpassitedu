<div class="page-header dark row" style="margin-top:0px;">
  <div class="col-md-9">
  <h2>{{ $user->name }} {{ $user->surname }} <small>{{ $user->username }}</small></h2>
  </div>
  @if(ViewHelpers::loged_in($user->username))
  <div class="col-md-3">
    <div class="progress" style="margin:20px 0 0 0;">
      <div class="progress-bar" role="progressbar" aria-valuenow="{{ViewHelpers::quota_percentage($user)}}" aria-valuemin="0" aria-valuemax="100" style="width: {{ViewHelpers::quota_percentage($user)}}%;">
        {{ViewHelpers::quota_percentage($user,2).'%'}}
      </div>
    </div>
     Espacio ocupado ({{number_format($user->quota/1024,2).'/'.number_format(User::Quota/1024,2)}} MB)
  </div>
  @endif
</div>