@section('title', 'Recuperar contraseña')

@section('content')
<div class="row" style="padding:20px;">
	<div class="panel panel-default">
		<div class="panel-body">
			<center>
				<h3>¿Has olvidado la contraseña?</h3>
				<p>Introduce tu e-mail y te enviaremos un correo para recuperar tu contraseña</p>
				<form action="{{ action('RemindersController@postRemind') }}" method="POST">
				    <div class="form-group">
					    <input type="email" name="email" placeholder="Introduce tu email" class="form-control" style="width:40%;" value="{{Input::old('email')}}">
					    <br>
					    <input type="submit" value="Enviar correo" class="btn btn-primary">
				  	</div>
				</form>
			</center>
		</div>
	</div>
</div>
@endsection