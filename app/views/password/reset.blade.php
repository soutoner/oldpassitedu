@section('title', 'Recuperar contraseña')

@section('content')
<div class="row" style="padding:20px;">
	<div class="panel panel-default">
		<div class="panel-body">
			<center>
				<h3>¿Has olvidado la contraseña?</h3>
				<br>
				<p>Introduce tu e-mail, tu nueva contraseña y su confirmacion, y la contraseña sera cambiada</p>
			<form action="{{ action('RemindersController@postReset') }}" method="POST">
			    <input type="hidden" name="token" value="{{ $token }}">
			    <div class="form-group" style="width:40%;">
				    <input type="email" name="email" placeholder="Introduce tu email" class="form-control"><br>
				    <input type="password" name="password"  placeholder="Introduce tu nueva contraseña" class="form-control"><br>
				    <input type="password" name="password_confirmation"  placeholder="Confirma tu contraseña" class="form-control"><br>
				    <input type="submit" value="Resetear contraseña" class="btn btn-primary">
				  </div>
			</form>
			</center>
		</div>
	</div>
</div>
@endsection